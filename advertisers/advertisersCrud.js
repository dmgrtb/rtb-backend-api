exports.findAdvertiser = function(req, res) {
	var Advertisers = schemas.advertisers;
	Advertisers.find({status: { $ne: 'deleted' }}, {name: 1}, function (err, result) {
		if (err || result === null) {
			var status = 400;
			sendResponse.sendHttpStatus(res, status);
		} else {
			var status = 200;
			var advObj = {advertisers: {}};
			advObj.advertisers = result;
			sendResponse.sendJson(res, status, advObj);
		}
	});
}


exports.addAdvertiser = function(req, res) {
	var status;
	var Advertisers = schemas.advertisers;

	if ( req.body.name !== undefined ) {
		var newAdvertiser = new Advertisers({name: req.body.name, status: 'active'});
		newAdvertiser.save(function (err, result) {
			if (err) status = 400;
			status = 201;
			sendResponse.sendHttpStatus(res, status);
		});

	} else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}


exports.editAdvertiser = function(req, res) {
	var status;
	var Advertisers = schemas.advertisers;
	var setParams;

	if ( req.body.name !== undefined && req.body.advId !== undefined && req.body.status !== undefined ) {
		 setParams = {_id: req.body.advId, name:req.body.name, status: req.body.status};
	}

	if (setParams === undefined) {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	} else {
		Advertisers.findByIdAndUpdate(req.body.advId, { $set: setParams }, {new: true}, function (err, result) {
			if (err || result.n === 0) {
				status = 400;
			} else {
				status = 200;
			}
			sendResponse.sendHttpStatus(res, status);
		});
	}
}


exports.deleteAdvertiser = function(req, res) {
	var status;
	var Advertisers = schemas.advertisers;

	if (req.body.advId !== undefined) {
		Advertisers.findByIdAndUpdate(req.body.advId, { $set: { status: 'deleted' } },function (err, result) {
//                    console.log(result);
			if (err || result.n === 0) {
				status = 400;
			} else {
                                if (typeof result.deals !== 'undefined' && Array.isArray(result.deals) && result.deals.length > 0){
                                    for (var i = 0; i < result.deals.length; i++){
                                        var setParams = { '$set': {} };
                                        var dealsStatus = 'deals.' + i + '.status';
                                        setParams[dealsStatus] = 'deleted';
                                        Advertisers.findByIdAndUpdate(req.body.advId, setParams, function (err, result) {
                                            if (err){
                                                console.log('Failed deleting demand deal id ' + result.deals[i]._id + ' when deleting advertiser id ' + req.body.advId + '. Error message -');
                                                console.log(err);
                                            }
                                        });
                                    }
                                }
                                if (typeof result.creatives !== 'undefined' && Array.isArray(result.creatives) && result.creatives.length > 0){
                                    for (var i = 0; i < result.deals.length; i++){
                                        var setParams = { '$set': {} };
                                        var creativeStatus = 'creatives.' + i + '.status';
                                        setParams[creativeStatus] = 'deleted';
                                        Advertisers.findByIdAndUpdate(req.body.advId, setParams, function (err, result) {
                                            if (err){
                                                console.log('Failed deleting creative id ' + result.creatives[i]._id + ' when deleting advertiser id ' + req.body.advId + '. Error message -');
                                                console.log(err);
                                            }
                                        });
                                    }
                                }
                                if (typeof result.campaigns !== 'undefined' && Array.isArray(result.campaigns) && result.campaigns.length > 0){
                                    for (var i = 0; i < result.deals.length; i++){
                                        var setParams = { '$set': {} };
                                        var campaignStatus = 'campaigns.' + i + '.status';
                                        setParams[campaignStatus] = 'deleted';
                                        Advertisers.findByIdAndUpdate(req.body.advId, setParams, function (err, result) {
                                            if (err){
                                                console.log('Failed deleting campaign id ' + result.campaigns[i]._id + ' when deleting advertiser id ' + req.body.advId + '. Error message -');
                                                console.log(err);
                                            }
                                        });
                                    }
                                }
				status = 204;
			}
			sendResponse.sendHttpStatus(res, status);
		});
	} else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}
