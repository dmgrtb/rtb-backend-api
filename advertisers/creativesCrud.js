var mongoose = require('mongoose');
var fs = require('fs');
var filePath;
var path = require('path');


exports.findCreatives = function(req, res) {
	var status;
	var Advertisers = schemas.advertisers;
	Advertisers.aggregate({$unwind: "$creatives"}, {$project: {name: 1, status: 1, creatives: 1}},
						{$match: {"creatives.status": {$ne: 'deleted' }}}, function (err, result) {

		if (err || result === null) {
			status = 400;
			sendResponse.sendHttpStatus(res, status);
		} else {
			status = 200;
			var creativesObj = {creatives: {}};
			creativesObj.creatives = result;
			sendResponse.sendJson(res, status, creativesObj);
		}
	});
}


exports.findCreativesByAdvertiserId = function(req, res) {
	var status;
	var Advertisers = schemas.advertisers;
	Advertisers.aggregate([{$match: {_id: mongoose.Types.ObjectId(req.params.id)}}, {$unwind: "$creatives"}, {$project: {creatives: 1}},
									{ $match: {"creatives.status": {$ne: 'deleted' } } }], function (err, result) {
		if (err || result === null) {
			status = 400;
			sendResponse.sendHttpStatus(res, status);
		} else {
			status = 200;
			var creativesObj = {creatives: {}};
			creativesObj.creatives = result;
			sendResponse.sendJson(res, status, creativesObj);
		}
	});
}


exports.findCreativeById = function(req, res) {
	var status;
	var Advertisers = schemas.advertisers;
	Advertisers.aggregate([{$unwind: "$creatives"}, {$match: {"creatives._id": mongoose.Types.ObjectId(req.params.id)}}, {$project: {creatives: 1}},
									{ $match: {"creatives.status": {$ne: 'deleted' } } }], function (err, result) {
		if (err) {
			status = 400;
			sendResponse.sendHttpStatus(res, status);
		} else {
			status = 200;
			var creativesObj = {creatives: {}};
			creativesObj.creatives = result;
			sendResponse.sendJson(res, status, creativesObj);
		}
	});
}


exports.addCreative = function(req, res, files) {
	var status;
	var Advertisers = schemas.advertisers;
	var util  = require('util');
	var _id = mongoose.Types.ObjectId();
	var form = new formidable.IncomingForm();

	form.parse(req, function(err, fields, files) {
		creativeValidate('active', _id, fields, files, function(params) {
			if (params === undefined) {
					status = 400;
					sendResponse.sendHttpStatus(res, status);
			} else {
				Advertisers.findByIdAndUpdate(fields.advId, { $push: {"creatives": params } }, {new: true}, function (err, result) {
					if (err || result === null) {
						status = 400;
					} else {
						status = 201;
					}
					sendResponse.sendHttpStatus(res, status);
				});
			}
		});
	});
}


exports.editCreative = function(req, res) {
	var status;
	var Advertisers = schemas.advertisers;
	var form = new formidable.IncomingForm();

	form.parse(req, function(err, fields, files) {

		creativeValidate(fields.status, fields.creativeId, fields, files, function(params) {

			if (params === undefined) {
				status = 400;
				sendResponse.sendHttpStatus(res, status);
			} else {
				Advertisers.update({_id: fields.advId, "creatives._id": fields.creativeId}, { $set: {"creatives.$": params } }, {new: true}, function (err, result) {
					if (err || result.n === 0) {
						status = 400;
					} else {
						status = 201;
					}
					sendResponse.sendHttpStatus(res, status);
				});
			}
		});
	});
}


exports.deleteCreative = function(req, res) {
	var status;
	var result;
	var Advertisers = schemas.advertisers;
	var form = new formidable.IncomingForm();

	if ( req.body.creativeId !== undefined && req.body.advId !== undefined ) {
		Advertisers.update({_id: req.body.advId, "creatives._id": req.body.creativeId}, { $set: {"creatives.$.status": 'deleted' } }, {new: true}, function (err, result) {
				if (err || result.n === 0) {
					status = 400;
				} else {
					status = 201;
				}
				sendResponse.sendHttpStatus(res, status);
		});
	} else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}

exports.uploadBulk = function(req, res){
    var form = new formidable.IncomingForm();
    var Advertisers = schemas.advertisers;
    form.parse(req, function(err, fields, files) {
        var _id = mongoose.Types.ObjectId();
        var creativeDetails = JSON.parse(fields.creativeDetails);
        var fileObj = {iUrl: files.qqfile};

        var specificBulkFields = creativeDetails.bulk[creativeDetails.idMapping[fields.qquuid]];
        var editedFields = creativeDetails.general;
        editedFields.name = specificBulkFields.creativeName;
        editedFields.w = specificBulkFields.width;
        editedFields.h = specificBulkFields.height;

        creativeValidate('active', _id, editedFields, fileObj, function(params) {
            if (params === undefined) {
                status = 400;
                sendResponse.sendJson(res, status, {"success": false});
            } else {
                Advertisers.findByIdAndUpdate(editedFields.advId, { $push: {"creatives": params } }, {new: true}, function (err, result) {
                    if (err || result === null) {
                            status = 400;
                            sendResponse.sendJson(res, status, {"success": false});
                    } else {
                            status = 201;
                            sendResponse.sendJson(res, status, {"success": true});
                    }

                });
            }
        });
    });

}

exports.verifyCreativeNames = function(req, res){
    var Advertisers = schemas.advertisers;
    var aggObj = [
        {$unwind: "$creatives"},
        {$project: {'creatives.id': 1, 'creatives.name': 1, 'creatives.status': 1, 'creatives._id': 1}},
    ]
    if (typeof req.body.currentId !== 'undefined'){
        aggObj.push({$match: {"creatives.name": {$in: req.body.names}, "creatives.status": {$ne: "deleted"}, "creatives._id": {$ne: mongoose.Types.ObjectId(req.body.currentId)}}});
    } else {
        aggObj.push({$match: {"creatives.name": {$in: req.body.names}, "creatives.status": {$ne: "deleted"}}});
    }
    Advertisers.aggregate(aggObj, function (err, result) {
        if (err){
            sendResponse.sendJson(res, 400, {"success": false});
        } else if (result.length === 0){
            sendResponse.sendJson(res, 200, {"success": true});
        } else {
            var response = {"success": false, data: []};
            var namesFound = [];
            for (var i = 0; i < result.length; i++){
                if (namesFound.indexOf(result[i].creatives.name) === -1){
                    matchIndex = req.body.names.indexOf(result[i].creatives.name);
                    response.data.push({"id": req.body.ids[matchIndex], "name": req.body.names[matchIndex]});
                    namesFound.push(result[i].creatives.name);
                }
            }
            sendResponse.sendJson(res, 200, response);
        }
    });
}

function creativeValidate(status, id, fields, files, callback) {
	var params = {};
	if ( fields.advId !== undefined && id !== undefined &&
	     fields.name !== undefined &&
	     fields.creativeType !== undefined && status !== undefined ) {
                var frequencyCap = -1;
                if (fields.frequencyCap && !isNaN(fields.frequencyCap) && fields.frequencyCap >= -1 && fields.frequencyCap <= 9) {
                    frequencyCap = fields.frequencyCap;
                }

//		 if (fields.creativeType === 'iframe' && fields.iframe !== undefined) {
//				params = { iframe: fields.iframe };
//				createCreativeFields(params, fields, id, status, callback);
//
//		 } else
                     if ( fields.creativeType === 'image' && fields.w !== undefined &&
                            fields.h !== undefined && fields.landingPage !== undefined && fields.secure !== undefined ) {
		 		uploadCreative(id, files, function(filePath) {
		 			if (filePath !== null && typeof filePath.iUrl !== "undefined") {
		 				createAdm('image', filePath.iUrl, function(adm, xAdm) {
		 					params = { frequencyCap: frequencyCap, iUrl: filePath.iUrl, adm: adm, xAdm: xAdm };
		 					createCreativeFields('image', params, fields, id, status, callback);
		 				});

		 			} else if (fields.iUrl !== undefined) {
		 				createAdm('image', fields.iUrl, function(adm, xAdm) {
		 					params = { frequencyCap: frequencyCap, iUrl: fields.iUrl, adm: adm, xAdm: xAdm };
		 					createCreativeFields('image', params, fields, id, status, callback);
	 					});
		 			} else {
		 				callback();
		 			}
		 		});

		 } else if (fields.creativeType === 'script' && fields.jsScript !== undefined) {
			createAdm('script', fields.jsScript, function(adm, xAdm) {
                        	params = { frequencyCap: frequencyCap, adm: adm, xAdm: xAdm, jsScript: fields.jsScript };
                                createCreativeFields('image', params, fields, id, status, callback);
                        });

//		 } else if (fields.creativeType === 'vast' && fields.vast !== undefined) {
//
//		 		if (fields.videoExtraDetails !== undefined) {
//		 			fields.videoExtraDetails = fields.videoExtraDetails.split(",");
//		 		}
//				params = { vast: fields.vast, videoExtraDetails: fields.videoExtraDetails, skippable: fields.skippble };
//				createCreativeFields(params, fields, id, status, callback);

		 } else if (fields.creativeType === 'native-display' && fields.title !== undefined &&
                            fields.description !== undefined && fields.action !== undefined &&
                            fields.starRating !== undefined ) {
		 		uploadCreative(id, files, function(filePath) {
                                        var iconUrl = null;
                                        if (filePath !== null && typeof filePath.iconUrl !== "undefined"){
                                            iconUrl = filePath.iconUrl;
                                        } else if (typeof fields.iconUrl !== "undefined"){
                                            iconUrl = fields.iconUrl;
                                        }
                                        var mainUrl = null;
                                        if (filePath !== null && typeof filePath.mainUrl !== "undefined"){
                                            mainUrl = filePath.mainUrl;
                                        } else if (typeof fields.mainUrl !== "undefined"){
                                            mainUrl = fields.mainUrl;
                                        }
                                        if (iconUrl !== null && mainUrl !== null){

                                            assetBuildObject = {
                                                        title: {
                                                            text: fields.title
                                                        },
                                                        img:{
                                                            1: {
                                                                url: iconUrl,
                                                                w: 80,
                                                                h: 80
                                                            },
                                                            3: {
                                                                url: mainUrl,
                                                                w: 1200,
                                                                h: 627
                                                            },
                                                        },
                                                        data: {
                                                            2: {
                                                                value: fields.description
                                                            },
                                                            3: {
                                                                value: fields.starRating
                                                            },
                                                            12: {
                                                                value: fields.action
                                                            }
                                                        }
                                                    };

                                            createNativeAssetsAndStrings(assetBuildObject, function(assetsObject, numRequired, native10, native11){
                                                        params = {
                                                            frequencyCap: frequencyCap,
                                                            assets: assetsObject,
                                                            requiredAssetsCount: numRequired,
                                                            native10: native10,
                                                            native11: native11
                                                        };
                                                        createCreativeFields('nativeDisplay', params, fields, id, status, callback);
                                                    });
                                        } else {
                                                callback();
                                        }
		 		});
                 } else {
				callback();
				return;
		 }
	} else {
		callback();
		return;
	}
}


function createCreativeFields(type, params, fields, id, status, callback) {

        var creativeMandatory = null;

	if (fields.categories !== undefined  && typeof fields.categories === 'string') {
		fields.categories = fields.categories.split(",");
		params = Object.assign(params, {categories:fields.categories});
	} else if (fields.categories !== undefined && Array.isArray(fields.categories)){
                params = Object.assign(params, {categories: fields.categories});
        }  else {
                params = Object.assign(params, {categories:[]});
        }

        if (fields.aDomain !== undefined && typeof fields.aDomain === 'string') {
                fields.aDomain = fields.aDomain.split(",");
                params = Object.assign(params, {aDomain:fields.aDomain});
        }  else if (fields.aDomain !== undefined && Array.isArray(fields.aDomain)){
                params = Object.assign(params, {type:fields.aDomain});
        }  else {
                params = Object.assign(params, {aDomain:[]});
        }

        if (fields.attr !== undefined && typeof fields.attr === 'string') {
                fields.attr = fields.attr.split(",");
                params = Object.assign(params, {attr:fields.attr});
        }  else if (fields.attr !== undefined && Array.isArray(fields.attr)){
                params = Object.assign(params, {type:fields.attr});
        }  else {
                params = Object.assign(params, {attr:[]});
        }

        if (type === 'image'){
            if (fields.type !== undefined && typeof fields.type === 'string') {
                    fields.type = fields.type.split(",");
                    params = Object.assign(params, {type:fields.type});
            }  else if (fields.type !== undefined && Array.isArray(fields.type)){
                    params = Object.assign(params, {type:fields.type});
            }  else {
                    params = Object.assign(params, {type: []});
            }

            if (params !== undefined) {
                    var creativeMandatory = {_id: id, name:fields.name, w: fields.w, h: fields.h, landingPage: fields.landingPage, creativeType: fields.creativeType,
                                             secure: fields.secure, pixelGenerator: fields.pixelGenerator, status: status};
                    creativeMandatory = Object.assign(creativeMandatory, params);
            }
        } else if (type === 'nativeDisplay') {
            if (fields.starRating !== undefined) {
                    params = Object.assign(params, {starRating: fields.starRating});
            }

            if (fields.layout !== undefined) {
                    fields.layout = fields.layout.split(",");
                    var fieldsLayout = fields.layout.map(function(lay){
                        return parseInt(lay);
                    });
                    params = Object.assign(params, {layout:fieldsLayout});
            } else {
                    params = Object.assign(params, {layout:[]});
            }

            if (fields.adUnit !== undefined) {
                    fields.adUnit = fields.adUnit.split(",");
                    var fieldsAdunit = fields.adUnit.map(function(adunit){
                        return parseInt(adunit);
                    });
                    params = Object.assign(params, {adUnit:fieldsAdunit});
            } else {
                    params = Object.assign(params, {adUnit:[]});
            }

            if (fields.context !== undefined) {
                    fields.context = fields.context.split(",");
                    var fieldsContext = fields.context.map(function(context){
                        return parseInt(context);
                    });
                    params = Object.assign(params, {context:fieldsContext});
            } else {
                    params = Object.assign(params, {context:[]});
            }

            if (fields.contextSubType !== undefined) {
                    fields.contextSubType = fields.contextSubType.split(",");
                    var fieldsContextSubType = fields.contextSubType.map(function(contextSubtype){
                        return parseInt(contextSubtype);
                    });
                    params = Object.assign(params, {contextSubType: fieldsContextSubType});
            } else {
                    params = Object.assign(params, {contextSubType: []});
            }

            if (params !== undefined) {
                    var creativeMandatory = {_id: id, name:fields.name, creativeType: fields.creativeType,
                                             status: status};
                    creativeMandatory = Object.assign(creativeMandatory, params);
            }
        }

        callback(creativeMandatory);
}


function uploadCreative(id, files, callback) {
	if (Object.keys(files).length !== 0) {
            var creativeNames = {};
            async.forEachOf(files, function (file, key, callback) {
		fs.readFile(file.path, function(err, data) {
                    var fileExt = path.extname(file.name);
                    file.name = file.name.replace(file.name, id) + '_' + key + fileExt;
                    var newPath = config.creativeUploadUrl + file.name;
                    fs.writeFile(newPath, data, function(err) {
                        if (err === null) {
                            creativeNames[key] = config.creativeExternalUrl + file.name;
                            callback();
                        } else {
                            callback(err);
                        }
                    });
		});
            }, function (err){
                if (err){
                    callback(null);
                } else {
                    callback(creativeNames)
                }
            });
	} else {
		callback(null);
	}
}

function createAdm(type, iUrl, callback) {
	if (type == 'image'){
        	var adm = "<script src='http://cdn.rtb2web.com/js/pixel.js'></script><img src='iUrl' border='0' onclick='go();' /><img style='position:absolute; left: -10000px;' width='1' height='1' src='http://evt.rtb2web.com/imp?auctionid=${AUCTION_ID}&bidid=${AUCTION_BID_ID}&impid=${AUCTION_IMP_ID}&seatid=${AUCTION_SEAT_ID}&adid=${AUCTION_AD_ID}&price=${AUCTION_PRICE}&cur=${AUCTION_CURRENCY}'/><script>function go(){fireClick('http://evt.rtb2web.com/click?auctionid=${AUCTION_ID}&bidid=${AUCTION_BID_ID}&impid=${AUCTION_IMP_ID}&seatid=${AUCTION_SEAT_ID}&adid=${AUCTION_AD_ID}', '[CLICKURL]');}</script>";
        	adm = adm.replace('iUrl', iUrl);

        	var xAdm = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'><head><title></title><script type='text/javascript' src='http://cdn.rtb2web.com/js/pixel.js'></script><script type='text/javascript'>function go(){fireClick('http://evt.rtb2web.com/click?auctionid=${AUCTION_ID}&bidid=${AUCTION_BID_ID}&impid=${AUCTION_IMP_ID}&seatid=${AUCTION_SEAT_ID}&adid=${AUCTION_AD_ID}', '[CLICKURL]'); try{adScore('send', 'adclick',  {}, { l6 : 'clicked' });}catch(e){}}</script></head><body><img src='iUrl' onclick='go();' alt='' /><img alt='' style='position:absolute; left: -10000px;' width='1' height='1' src='http://evt.rtb2web.com/imp?auctionid=${AUCTION_ID}&bidid=${AUCTION_BID_ID}&impid=${AUCTION_IMP_ID}&seatid=${AUCTION_SEAT_ID}&adid=${AUCTION_AD_ID}&price=${AUCTION_PRICE}&cur=${AUCTION_CURRENCY}' /><script type='text/javascript' async='async' src='https://pixel.yabidos.com/fltiu.js?qid=039393f5539393f5639363&cid=696&p=DMG&s=[APPNAME][DOMAIN]&x=[PUBNAME]&nci=[CNAME]&adtg=[COUNTRY]&nai=[ADID]&ci=[CRID]&ip=[IP]&ai=[APPID][SITEID]&di=[DEVICEID]&mm=[MAKE_MODEL]&os=[OS]' language='javascript'></script></body></html>";
		xAdm = xAdm.replace('iUrl', iUrl);
	}
	else if (type == 'script'){
		var adm = "<script src='http://cdn.rtb2web.com/js/pixel.js'></script>iUrl<script>function go(){fireClick('http://evt.rtb2web.com/click?auctionid=${AUCTION_ID}&bidid=${AUCTION_BID_ID}&impid=${AUCTION_IMP_ID}&seatid=${AUCTION_SEAT_ID}&adid=${AUCTION_AD_ID}', '[CLICKURL]');}</script>";

		adm = adm.replace('iUrl', iUrl);

		var xAdm = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'><head><title></title><script type='text/javascript' src='http://cdn.rtb2web.com/js/pixel.js'></script><script type='text/javascript'>function go(){fireClick('http://evt.rtb2web.com/click?auctionid=${AUCTION_ID}&bidid=${AUCTION_BID_ID}&impid=${AUCTION_IMP_ID}&seatid=${AUCTION_SEAT_ID}&adid=${AUCTION_AD_ID}', '[CLICKURL]'); try{adScore('send', 'adclick',  {}, { l6 : 'clicked' });}catch(e){}}</script></head><body>iUrl<img alt='' style='position:absolute; left: -10000px;' width='1' height='1' src='http://evt.rtb2web.com/imp?auctionid=${AUCTION_ID}&bidid=${AUCTION_BID_ID}&impid=${AUCTION_IMP_ID}&seatid=${AUCTION_SEAT_ID}&adid=${AUCTION_AD_ID}&price=${AUCTION_PRICE}&cur=${AUCTION_CURRENCY}' /><script type='text/javascript' src='https://js.ad-score.com/score.min.js?pid=1000093#tid=DSP&l1=[PUBNAME]&l2=[COUNTRY]&l3=[ISP]&l4=[APPNAME]&l5=[MAKE_MODEL]&utid=[REQUESTID]&uip=[IP]&ref=[REFURL]&pub_app=[APPNAME]&pub_domain=[DOMAIN]&cb=[CB]' async=1 ></script><noscript><img src='https://data.ad-score.com/img?s=ns&pid=1000093&tid=DSP&l1=[PUBNAME]&l2=[COUNTRY]&l3=[ISP]&l4=[APPNAME]&l5=[MAKE_MODEL]&utid=[REQUESTID]&uip=[IP]&ref=[REFURL]&pub_app=[APPNAME]&pub_domain=[DOMAIN]&cb=[CB]' width='1' height='1'></img></noscript><script type='text/javascript' async='async' src='https://pixel.yabidos.com/fltiu.js?qid=039393f5539393f5639363&cid=696&p=DMG&s=[APPNAME][DOMAIN]&x=[PUBNAME]&nci=[CNAME]&adtg=[COUNTRY]&nai=[ADID]&ci=[CRID]&ip=[IP]&ai=[APPID][SITEID]&di=[DEVICEID]&mm=[MAKE_MODEL]&os=[OS]' language='javascript'></script></body></html>";
		xAdm = xAdm.replace('iUrl', iUrl);
	}

	callback(adm, xAdm);
}


function createNativeAssetsAndStrings(rawAssets, callback) {

        //create the assets objects including the strings used in the response
        var finalAssets= {};
        var numRequired = 0;
        for (var i = 0; i < config.native.assets.length; i++){
            if (config.native.assets[i].type1 in rawAssets){
                var type1 = config.native.assets[i].type1;
                var type2 = typeof config.native.assets[i].type2 !== 'undefined' ? config.native.assets[i].type2 : null;
                if (typeof finalAssets[type1] === 'undefined'){
                    finalAssets[type1] = {};
                }
                var contentObj;
                if (type2 !== null){
                    contentObj = rawAssets[type1][type2];
                } else {
                    contentObj = rawAssets[type1];
                }

                contentObj.stringified =  createAssetString(type1, type2, contentObj);
                switch (type1){
                    case 'title':
                        contentObj.len = contentObj.text.length;
                        break;
                    case 'data':
                        contentObj.len = contentObj.value.length;
                        break;
                }
                contentObj.required = config.native.assets[i].required;
                numRequired += config.native.assets[i].required;
                if (type2 !== null){
                    finalAssets[type1][type2] = contentObj;
                } else {
                    finalAssets[type1] = contentObj;
                }
            }
        }

        //create the strings for the native responses not including the assets
        var obj11 = {
//                    "ver": "1.1",
                    "link": {
                        "url": "[CLICKURL]",
                        "clicktrackers": config.native.clickTrackers
                    },
                    "assets": ["[ASSETS_STRINGS]"],
                    "imptrackers": config.native.impTrackers
                };

        var objString11 = JSON.stringify({"native": obj11});

        var obj10 = {"native": obj11};
        obj10.native.ver = "1.0";

        var objString10 = JSON.stringify(obj10);
	callback(finalAssets, numRequired, objString10, objString11);
}

function createAssetString(type1, type2 = null, content){
    var obj = {};
    var replacementString = '[' + type1.toUpperCase() + '_';
    replacementString += type2 !== null ? type2 + '_' : '';
    obj.id = replacementString + 'ID]';
    obj.required = replacementString + 'REQUIRED]';
    obj[type1] = content;
    return JSON.stringify(obj);
}
