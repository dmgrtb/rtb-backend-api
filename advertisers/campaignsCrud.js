var mongoose = require('mongoose');
var rotConfig = require('../config/rotationsConfig');

exports.findCampaigns = function (req, res) {
    var status;
    var Advertisers = schemas.advertisers;
    var statusQuery = {$match: {"campaigns.status": {$ne: "deleted"}}};
    if (typeof req.query.status !== "undefined") {
        switch (req.query.status) {
            case 'inactive':
            case 'active':
                statusQuery = {$match: {"campaigns.status": req.query.status}};
                break;
        }
    }

    if (typeof req.query.dealId !== 'undefined') {
        var dealId = mongoose.Types.ObjectId(req.query.dealId);
        Advertisers.aggregate({$unwind: "$deals"}, {$match: {'deals._id': dealId}}, function (err, result) {
            if (err || result === null || typeof result[0].deals.campaigns === 'undefined') {
                status = 400;
                sendResponse.sendHttpStatus(res, status);
            } else {
                findCampaignsIds(result[0].deals.campaigns, statusQuery, res);
            }
        });
    } else {
        findCampaignsIds(false, statusQuery, res);
    }
};

function findCampaignsIds(ids, statusQuery, res) {
    var Advertisers = schemas.advertisers;
    var matchIds = {"$match": {"_id": {"$ne": null}}};
    if (ids !== false) {
        matchIds = {"$match": {"campaigns._id": {$in: ids}}};
    }
    Advertisers.aggregate({$unwind: "$campaigns"}, matchIds, {$project: {name: 1, status: 1, "deals._id": 1, "deals.name": 1, "deals.status": 1, "deals.campaigns": 1, campaigns: 1,
            cap: {$divide: ["$campaigns.cap", 1000]}}},
            statusQuery, function (err, result) {
                if (err || result === null) {
                    status = 400;
                    sendResponse.sendHttpStatus(res, status);
                } else {
                    status = 200;
                    var campaignsObj = {campaigns: {}};
                    campaignsObj.campaigns = result;
                    sendResponse.sendJson(res, status, campaignsObj);
                }
            });
}

exports.findCountries = function (req, res) {
    var status;
    var Countries = schemas.countries;
    var params;

    if (req.query.value !== undefined) {
        params = {name: new RegExp('.*' + req.query.value + '.*', "i")};
    } else {
        params = {};
    }

    Countries.find(params, function (err, result) {
        if (err || result === null) {
            status = 400;
            sendResponse.sendHttpStatus(res, status);
        } else {
            status = 200;
            var countriesObj = {countries: {}};
            countriesObj.countries = result;
            sendResponse.sendJson(res, status, countriesObj);
        }
    });
};

exports.findCategories = function (req, res) {
    var status;
    var Categories = schemas.categories;
    var params;

    if (req.query.value !== undefined) {
        params = {description: new RegExp('.*' + req.query.value + '.*', "i")};
    } else {
        params = {};
    }
    Categories.find(params, function (err, result) {
        if (err || result === null) {
            status = 400;
            sendResponse.sendHttpStatus(res, status);
        } else {
            status = 200;
            var categoriesObj = {categories: {}};
            categoriesObj.categories = result;
            sendResponse.sendJson(res, status, categoriesObj);
        }
    });
};

exports.findCampaignById = function (req, res) {
    var Advertisers = schemas.advertisers;
    if (typeof req.params.id === 'undefined') {
        sendResponse.sendHttpStatus(res, 400);
    } else {
        campaignId = mongoose.Types.ObjectId(req.params.id);
        Advertisers.aggregate({$project: {deals: 1}}, {$unwind: "$deals"}, {$unwind: "$deals.campaigns"}, {$match: {'deals.campaigns': campaignId}}, function (err, resultDeal) {
            if (err || resultDeal === null ||
                    resultDeal.length === 0 ||
                    typeof resultDeal[0].deals === 'undefined' ||
                    typeof resultDeal[0].deals._id === 'undefined') {
                sendResponse.sendHttpStatus(res, 400);
            } else {
                var dealId = resultDeal[0].deals._id;
                var dealType = resultDeal[0].deals.dealType;
                Advertisers.aggregate([{$unwind: "$campaigns"}, {$match: {"campaigns._id": campaignId, "campaigns.status": {$ne: 'deleted'}}}, {$project: {campaigns: 1, cap: {$divide: ["$campaigns.cap", 1000]}}}], function (err, result) {
                    if (err || result.length === 0) {
                        sendResponse.sendHttpStatus(res, 400);
                    } else {
                        var campaignsObj = result[0];
                        campaignsObj.dealId = dealId;
                        campaignsObj.dealType = dealType;
                        sendResponse.sendJson(res, 200, campaignsObj);
                    }
                });
            }
        });
    }
};

exports.findRotations = function (req, res) {
    var status;
    var Rotations = schemas.rotations;

    Rotations.find({"status": {$ne: "deleted"}}, function (err, result) {
        if (err || result === null) {
            status = 400;
            sendResponse.sendHttpStatus(res, status);
        } else {
            status = 200;
            var rotationsObj = {rotations: {}};
            rotationsObj.rotations = result;
            sendResponse.sendJson(res, status, rotationsObj);
        }
    });

};

exports.findRotationById = function (req, res) {
    var Rotations = schemas.rotations;
    if (typeof req.params.id === 'undefined') {
        sendResponse.sendHttpStatus(res, 400);
    } else {
        Rotations.findById(req.params.id, function (err, result) {
            if (err || result === null) {
                var status = 400;
                sendResponse.sendHttpStatus(res, status);
            } else {
                var status = 200;
                var rotObj = {rotations: {}};
                rotObj.rotations = result;
                sendResponse.sendJson(res, status, rotObj);
            }
        });
    }
};

exports.addRotation = function (req, res) {
    var Rotations = schemas.rotations;
    var _id = mongoose.Types.ObjectId();

    if (typeof req.body !== "undefined" && typeof req.body.createCopy !== 'undefined' &&
            req.body.createCopy === 'true' && typeof req.body.rotationName !== "undefined" &&
            typeof req.body.rotationId !== 'undefined') {
        createCopiedRotation(req.body.rotationId, _id, req.body.rotationName, function (oldId, params) {
            if (oldId === undefined || params === undefined) {
                status = 400;
                sendResponse.sendHttpStatus(res, status);
            } else {
                var copyRotation = new Rotations(
                        {_id: params._id, 
                        endpoint: params.endpoint,
                        name: params.name,
                        publisher: params.publisher,
                        campaigns: params.campaigns,
                        status: params.status,
                        sum: params.sum});
                copyRotation.save(function (err) {
                    if (err)
                        status = 400;
                    status = 201;
                    sendResponse.sendHttpStatus(res, status);
                });
            }
        });
    } else {

        if (req.body.status !== undefined && req.body.name !== undefined && req.body.campaigns !== undefined && req.body.sum !== undefined && req.body.publisher !== undefined) {
            var endpoint = 'https://dsp.rtb2web.com/ro/' + _id + rotConfig[req.body.publisher];

            var newRotation = new Rotations(
                    {_id: _id, 
                        endpoint: endpoint,
                        name: req.body.name,
                        publisher: req.body.publisher,
                        campaigns: JSON.parse(req.body.campaigns),
                        status: req.body.status,
                        sum: parseInt(req.body.sum)});
            newRotation.save(function (err) {
                if (err)
                    status = 400;
                status = 201;
                sendResponse.sendHttpStatus(res, status);
            });

        } else {
            status = 400;
            sendResponse.sendHttpStatus(res, status);
        }
    }
};

exports.editRotation = function (req, res) {
    var Rotations = schemas.rotations;

    if (req.body.status !== undefined && req.body.name !== undefined && req.body.campaigns !== undefined && req.body.sum !== undefined) {
        var endpoint = 'https://dsp.rtb2web.com/ro/' + req.body._id + rotConfig[req.body.publisher];
        var set = {$set:
                    {_id: req.body._id,
                        endpoint: endpoint,
                        name: req.body.name,
                        publisher: req.body.publisher,
                        campaigns: JSON.parse(req.body.campaigns),
                        status: req.body.status,
                        sum: parseInt(req.body.sum)}};

        Rotations.update({"_id": req.body._id}, set, {new : true}, function (err) {
            if (err) {
                console.log(err);
                status = 400;
            }
            status = 201;
            sendResponse.sendHttpStatus(res, status);
        });

    } else {
        status = 400;
        sendResponse.sendHttpStatus(res, status);
    }
};

exports.deleteRotation = function (req, res) {
    var status;
    var Rotations = schemas.rotations;

    if (req.body.rotationId !== undefined) {
        Rotations.update({_id: req.body.rotationId}, {$set: {"status": 'deleted'}}, {new : true}, function (err, result) {
            if (err || result.n === 0) {
                status = 400;
            } else {
                status = 201;
            }
            sendResponse.sendHttpStatus(res, status);
        });
    } else {
        status = 400;
        sendResponse.sendHttpStatus(res, status);
    }
}


exports.addCampaign = function (req, res) {
    var Publishers = schemas.publishers;
    var Advertisers = schemas.advertisers;
    var _id = mongoose.Types.ObjectId();

    //First we'll check if we're trying to create a copy of an existing campaign
    if (typeof req.body !== "undefined" && typeof req.body.createCopy !== 'undefined' &&
            req.body.createCopy === 'true' && typeof req.body.campaignName !== "undefined" &&
            typeof req.body.cid !== 'undefined') {
        createCopiedCampaign(req.body.cid, _id, req.body.campaignName, function (advId, dealId, params) {
            if (advId === undefined || dealId === undefined || params === undefined) {
                status = 400;
                sendResponse.sendHttpStatus(res, status);
            } else {
                Advertisers.findByIdAndUpdate(advId, {$push: {"campaigns": params}}, {new : true}, function (err, result) {
                    if (err || result === null) {
                        console.log(err);
                        status = 400;
                        sendResponse.sendHttpStatus(res, status);
                    } else {
                        Advertisers.update({"deals._id": dealId}, {$push: {"deals.$.campaigns": _id}}, {new : true}, function (err, result) {
                            if (err || result === null) {
                                console.log(err);
                                status = 400;
                                sendResponse.sendHttpStatus(res, status);
                            } else {
                                status = 201;
                            }
                            sendResponse.sendHttpStatus(res, status);
                        });
                    }
                });
            }
        });
    } else if (req.body.name !== undefined && (req.body.dealId !== undefined || req.body.campaignId !== undefined) && req.body.advId !== undefined &&
            req.body.pricingModel !== undefined && !isNaN(req.body.cap)) {
        var status;
        var set;

        campaignValidate(req, _id, req.body.status, function (params) {

            if (params === undefined) {
                status = 400;
                sendResponse.sendHttpStatus(res, status);
            } else {
                Advertisers.findByIdAndUpdate(req.body.advId, {$push: {"campaigns": params}}, {new : true}, function (err, result) {
                    if (err || result === null) {
                        console.log(err);
                        status = 400;
                        sendResponse.sendHttpStatus(res, status);
                    } else {
                        Advertisers.update({"deals._id": req.body.dealId}, {$push: {"deals.$.campaigns": _id}}, {new : true}, function (err, result) {
                            if (err || result === null) {
                                status = 400;
                                sendResponse.sendHttpStatus(res, status);
                            } else {
                                status = 201;
                                if (req.body.creativeId !== undefined) {
                                    if (Array.isArray(req.body.creativeId)) {
                                        set = {$set: {"campaigns.$.creatives": req.body.creativeId}};
                                    } else {
                                        set = {$push: {"campaigns.$.creatives": req.body.creativeId}};
                                    }

                                    Advertisers.update({"campaigns._id": _id}, set, {new : true}, function (err, result) {
                                        if (err || result === null) {
                                            status = 400;
                                        } else {
                                            status = 201;
                                        }
                                        sendResponse.sendHttpStatus(res, status);
                                    });
                                } else {
                                    updateSupplies(Publishers, req.body.supplySourceList, req.body.supplySource, req.body.name, res);
                                }
                            }
                        });
                    }
                });
            }
        });
    } else {
        status = 400;
        sendResponse.sendHttpStatus(res, status);
    }
}


exports.editCampaign = function (req, res) {
    var status;
    var Advertisers = schemas.advertisers;
    var Publishers = schemas.publishers;

    if (req.body.name !== undefined && (req.body.dealId !== undefined || req.body.campaignId !== undefined) && req.body.advId !== undefined &&
            req.body.clickUrl !== undefined && !isNaN(req.body.maxBid) && req.body.pricingModel !== undefined && req.body.payoutModel !== undefined && !isNaN(req.body.payout) && !isNaN(req.body.cap) && typeof req.body.department !== 'undefined') {
        campaignValidate(req, req.body.campaignId, req.body.status, function (params) {
            if (params === undefined) {
                status = 400;
                sendResponse.sendHttpStatus(res, status);
            } else {
                //We'll check if we've changed the advertiser when we edited the campaign
                if (typeof req.body.curAdvId !== 'undefined' && typeof req.body.curDealId !== 'undefined' && req.body.curAdvId !== req.body.advId) {
                    Advertisers.findByIdAndUpdate(req.body.advId, {$push: {"campaigns": params}}, {new : true}, function (err, result) {
                        if (err || result === null) {
                            console.log(err);
                            status = 400;
                            sendResponse.sendHttpStatus(res, status);
                        } else {
                            Advertisers.update({"deals._id": req.body.dealId}, {$push: {"deals.$.campaigns": req.body.campaignId}}, {new : true}, function (err, result) {
                                if (err || result === null) {
                                    console.log(err);
                                    status = 400;
                                    sendResponse.sendHttpStatus(res, status);
                                } else {
                                    status = 201;
                                    if (req.body.creativeId !== undefined) {
                                        if (Array.isArray(req.body.creativeId)) {
                                            set = {$set: {"campaigns.$.creatives": req.body.creativeId}};
                                        } else {
                                            set = {$push: {"campaigns.$.creatives": req.body.creativeId}};
                                        }
                                        Advertisers.update({"campaigns._id": req.body.campaignId}, set, {new : true}, function (err, result) {
                                            if (err || result === null) {
                                                console.log(err);
                                                status = 400;
                                                sendResponse.sendHttpStatus(res, status);
                                            } else {
                                                status = 201;
                                                deleteCampaignFromAdvertiser(res, status, Advertisers, req.body.curAdvId, req.body.curDealId, req.body.campaignId);
                                                updateSupplies(Publishers, req.body.supplySourceList, req.body.supplySource, req.body.name, res);
                                            }
                                        });
                                    } else {
                                        deleteCampaignFromAdvertiser(res, status, Advertisers, req.body.curAdvId, req.body.curDealId, req.body.campaignId);
                                    }
                                }
                            });
                        }
                    });
                } else {
                    if (params.factor === undefined) {
                        params.factor = 0;
                    }

                    var setParams = {'$set': {'campaigns.$._id': params._id, 'campaigns.$.name': params.name, 'campaigns.$.clickUrl': params.clickUrl,
                            'campaigns.$.pricingModel': params.pricingModel, 'campaigns.$.maxBid': params.maxBid,
                            'campaigns.$.payoutModel': params.payoutModel, 'campaigns.$.payout': params.payout,
                            'campaigns.$.cap': params.cap, 'campaigns.$.department': params.department,
                            'campaigns.$.factor': params.factor,
                            'campaigns.$.frequencyCap': params.frequencyCap, 'campaigns.$.status': params.status,
                            'campaigns.$.creatives': params.creatives, 'campaigns.$.target': params.target}};
                    Advertisers.update({_id: req.body.advId, "campaigns._id": req.body.campaignId}, setParams, {new : true}, function (err, result) {
                        if (err || result.n === 0) {
                            console.log(err);
                            status = 400;
                            sendResponse.sendHttpStatus(res, status);
                        } else {
                            status = 201;
                            updateSupplies(Publishers, req.body.supplySourceList, req.body.supplySource, req.body.name, res);

                            if ((typeof req.body.curDealId !== 'undefined' && req.body.curDealId !== req.body.dealId)) {
                                Advertisers.update({"deals._id": req.body.dealId}, {$push: {"deals.$.campaigns": req.body.campaignId}}, {new : true}, function (err, result) {
                                    if (err || result === null) {
                                        console.log(err);
                                        status = 400;
                                        sendResponse.sendHttpStatus(res, status);
                                    } else {
                                        deleteCampaignFromDemandDeal(res, status, Advertisers, req.body.advId, req.body.curDealId, req.body.campaignId);
                                    }
                                });
                            } else {
                                sendResponse.sendHttpStatus(res, status);
                            }
                        }
                    });
                }
            }
        });
    } else {
        status = 400;
        sendResponse.sendHttpStatus(res, status);
    }
}

function deleteCampaignFromDemandDeal(res, status, Advertisers, adId, dealId, cid) {
    Advertisers.update({_id: adId, "deals._id": dealId}, {$pull: {"deals.$.campaigns": cid}}, function (err, result) {
        if (err || result.n === 0) {
            status = 400;
            sendResponse.sendHttpStatus(res, status);
        } else {
            status = 201;
            sendResponse.sendHttpStatus(res, status);
        }
    });
}

function deleteCampaignFromAdvertiser(res, status, Advertisers, adId, dealId, cid) {
    Advertisers.update({_id: adId}, {$pull: {campaigns: {_id: cid}}}, function (err, result) {
        if (err || result.n === 0) {
            status = 400;
            sendResponse.sendHttpStatus(res, status);
        } else {
            deleteCampaignFromDemandDeal(res, status, Advertisers, adId, dealId, cid);
        }
    });
}


exports.deleteCampaign = function (req, res) {
    var status;
    var result;
    var Advertisers = schemas.advertisers;

    if (req.body.campaignId !== undefined && req.body.advId !== undefined) {
        Advertisers.update({_id: req.body.advId, "campaigns._id": req.body.campaignId}, {$set: {"campaigns.$.status": 'deleted'}}, {new : true}, function (err, result) {
            if (err || result.n === 0) {
                status = 400;
            } else {
                status = 201;
            }
            sendResponse.sendHttpStatus(res, status);
        });
    } else {
        status = 400;
        sendResponse.sendHttpStatus(res, status);
    }
}


function campaignValidate(req, id, status, callback) {

    if (typeof req.body.maxBid === 'undefined' || isNaN(req.body.maxBid) || req.body.maxBid <= 0) {
        req.body.maxBid = -1;
    }

    var params = {_id: id, name: req.body.name, pricingModel: req.body.pricingModel, maxBid: req.body.maxBid, payoutModel: req.body.payoutModel, payout: req.body.payout, cap: req.body.cap * 1000, status: status, department: req.body.department, clickUrl: req.body.clickUrl, target: {}};

    if (req.body.frequencyCap && !isNaN(req.body.frequencyCap) && req.body.frequencyCap >= -1 && req.body.frequencyCap <= 9) {
        params.frequencyCap = req.body.frequencyCap;
    } else {
        params.frequencyCap = -1;
    }

    if (req.body.factor !== undefined) {
        params.factor = req.body.factor;
    }

    if (req.body.creativeId !== undefined) {
        if (Array.isArray(req.body.creativeId)) {
            params.creatives = req.body.creativeId;
        } else {
            params.creatives = [req.body.creativeId];
        }
    }

    if (req.body.countryList !== undefined && req.body.country !== undefined) {
        var countries = [];
        if (Array.isArray(req.body.country)) {
            countries = req.body.country;
        } else {
            countries.push(req.body.country);
        }
        if (req.body.countryList === 'black') {
            params.target = Object.assign(params.target, {country_black: countries});
        } else {
            params.target = Object.assign(params.target, {country: countries});
        }
    }

    if (req.body.connectionTypeList !== undefined && req.body.connectionType !== undefined) {
        var connectionTypes = [];
        if (Array.isArray(req.body.connectionType)) {
            connectionTypes = req.body.connectionType.map(function (cType) {
                return parseInt(cType, 10);
            });
        } else {
            var connectionType = parseInt(req.body.connectionType, 10);
            connectionTypes.push(connectionType);
        }
        if (req.body.connectionTypeList === 'black') {
            params.target = Object.assign(params.target, {connectionType_black: connectionTypes});
        } else {
            params.target = Object.assign(params.target, {connectionType: connectionTypes});
        }
    }

    if (req.body.mobileOsList !== undefined && req.body.mobileOs !== undefined) {
        var mobileOses = [];
        if (Array.isArray(req.body.mobileOs)) {
            mobileOses = req.body.mobileOs;
        } else {
            mobileOses.push(req.body.mobileOs);
        }
        if (req.body.mobileOsList === 'black') {
            params.target = Object.assign(params.target, {mobileOs_black: mobileOses});
        } else {
            params.target = Object.assign(params.target, {mobileOs: mobileOses});
        }
    }

    if (req.body.mobileDeviceList !== undefined && req.body.deviceMake !== undefined && req.body.deviceModel !== undefined) {
        var mobileMakes = [];
        var mobileModels = [];
        if (Array.isArray(req.body.deviceMake)) {
            mobileMakes = req.body.deviceMake;
        } else {
            mobileMakes.push(req.body.deviceMake);
        }
        if (Array.isArray(req.body.deviceModel)) {
            mobileModels = req.body.deviceModel;
        } else {
            mobileModels.push(req.body.deviceModel);
        }

        var mobileDevices = {};
        for (var i = 0; i < mobileMakes.length; i++) {
            if (!(mobileMakes[i].toLowerCase() in mobileDevices)) {
                mobileDevices[mobileMakes[i].toLowerCase()] = [];
            }
            mobileDevices[mobileMakes[i].toLowerCase()].push(mobileModels[i].toLowerCase());
        }

        if (req.body.mobileDeviceList === 'black') {
            params.target = Object.assign(params.target, {mobileDevice_black: mobileDevices});
        } else {
            params.target = Object.assign(params.target, {mobileDevice: mobileDevices});
        }
    }


    if (req.body.campaignTiming !== undefined) {
        params.target = Object.assign(params.target, {campaignTiming: req.body.campaignTiming});
    }
    if (req.body.osVersionList !== undefined && (req.body.osVersionMin !== undefined || req.body.osVersionMax !== undefined)) {
        var min = req.body.osVersionMin !== undefined ? parseFloat(req.body.osVersionMin) : false;
        var max = req.body.osVersionMax !== undefined ? parseFloat(req.body.osVersionMax) : false;
        if (isNaN(min))
            min = false;
        if (isNaN(max))
            max = false;
        var osVersion = {
            min: min,
            max: max
        };

        if (req.body.osVersionList === 'black') {
            params.target = Object.assign(params.target, {osVersion_black: osVersion});
        } else {
            params.target = Object.assign(params.target, {osVersion: osVersion});
        }
    }

    if (req.body.desktopOsList !== undefined && req.body.desktopOs !== undefined) {
        var desktopOses = [];
        if (Array.isArray(req.body.desktopOs)) {
            desktopOses = req.body.desktopOs;
        } else {
            desktopOses.push(req.body.desktopOs);
        }
        if (req.body.desktopOsList === 'black') {
            params.target = Object.assign(params.target, {desktopOs_black: desktopOses});
        } else {
            params.target = Object.assign(params.target, {desktopOs: desktopOses});
        }
    }

    if (req.body.posList !== undefined && req.body.pos !== undefined) {
        var poses = [];
        if (Array.isArray(req.body.pos)) {
            poses = req.body.pos;
        } else {
            poses.push(req.body.pos);
        }
        if (req.body.posList === 'black') {
            params.target = Object.assign(params.target, {pos_black: poses});
        } else {
            params.target = Object.assign(params.target, {pos: poses});
        }
    }

    if (req.body.categoriesList !== undefined && req.body.categories !== undefined) {
        var categories = [];
        if (Array.isArray(req.body.categories)) {
            categories = req.body.categories;
        } else {
            categories.push(req.body.categories);
        }
        if (req.body.categoriesList === 'black') {
            params.target = Object.assign(params.target, {categories_black: categories});
        } else {
            params.target = Object.assign(params.target, {categories: categories});
        }
    }

    if (req.body.domainList !== undefined && req.body.domain !== undefined) {


        if (req.body.department == 'Video') {
            var domains = [];

            if (Array.isArray(req.body.domain)) {
                domains = req.body.domain;
            } else {
                domains.push(req.body.domain);
            }
            if (req.body.domainList === 'black') {
                params.target = Object.assign(params.target, {domain_black: domains});
            } else {
                params.target = Object.assign(params.target, {domain: domains});
            }

        } else {
            var domains;
            if (Array.isArray(req.body.domain)) {
                domains = convertComplexStrings(req.body.domain);
            } else {
                domains = [req.body.domain];
                domains = convertComplexStrings(domains);
            }
            if (req.body.domainList === 'black') {
                params.target = Object.assign(params.target, {domain_black: domains});
            } else {
                params.target = Object.assign(params.target, {domain: domains});
            }
        }
    }

    if (req.body.appList !== undefined && req.body.app !== undefined) {
        var apps = [];

        if (Array.isArray(req.body.app)) {
            apps = req.body.app;
        } else {
            apps.push(req.body.app);
        }

        if (req.body.appList === 'black') {
            params.target = Object.assign(params.target, {app_black: apps});
        } else {
            params.target = Object.assign(params.target, {app: apps});
        }
    }

    if (req.body.supplySourceList !== undefined && req.body.supplySource !== undefined) {
        var supplySources = [];
        if (Array.isArray(req.body.supplySource)) {
            supplySources = req.body.supplySource;
        } else {
            supplySources.push(req.body.supplySource);
        }
        if (req.body.supplySourceList === 'black') {
            params.target = Object.assign(params.target, {supplySource_black: supplySources});
        } else {
            params.target = Object.assign(params.target, {supplySource: supplySources});
        }
    }

    if (req.body.carrierList !== undefined && req.body.carrier !== undefined) {
        var carriers = [];
        if (Array.isArray(req.body.carrier)) {
            carriers = req.body.carrier;
        } else {
            carriers.push(req.body.carrier);
        }
        var carriersTrimmed = carriers.map(function (carrier) {
            return carrier.trim()
        });
        if (req.body.carrierList === 'black') {
            params.target = Object.assign(params.target, {carrier_black: carriersTrimmed});
        } else {
            params.target = Object.assign(params.target, {carrier: carriersTrimmed});
        }
    }

    if (req.body.bundleIdList !== undefined && req.body.bundleId !== undefined) {
        var bundleId = [];
        if (Array.isArray(req.body.bundleId)) {
            bundleId = req.body.bundleId;
        } else {
            bundleId.push(req.body.bundleId);
        }
        if (req.body.bundleIdList === 'black') {
            params.target = Object.assign(params.target, {bundleId_black: bundleId});
        } else {
            params.target = Object.assign(params.target, {bundleId: bundleId});
        }
    }

    if (req.body.keywordsList !== undefined && req.body.keywords !== undefined) {
        var keywords = [];
        if (Array.isArray(req.body.keywords)) {
            keywords = req.body.keywords;
        } else {
            keywords.push(req.body.keywords);
        }
        if (req.body.keywordsList === 'black') {
            params.target = Object.assign(params.target, {keywords_black: keywords});
        } else {
            params.target = Object.assign(params.target, {keywords: keywords});
        }
    }

    if (req.body.subidList !== undefined && req.body.subid !== undefined) {
        var subid = [];
        if (Array.isArray(req.body.subid)) {
            subid = req.body.subid;
        } else {
            subid.push(req.body.subid);
        }
        if (req.body.subidList === 'black') {
            params.target = Object.assign(params.target, {subid_black: subid});
        } else {
            params.target = Object.assign(params.target, {subid: subid});
        }
    }

    callback(params);
}

function convertComplexStrings(complexStringArray) {
    var dbObject = {};
    for (var i = 0; i < complexStringArray.length; i++) {
        var stringSplit = complexStringArray[i].split('___');
        var pubIndex = stringSplit.length - 1;
        if (pubIndex > 0) {
            var pubId = stringSplit[pubIndex].trim();
            if (typeof dbObject[pubId] === 'undefined') {
                dbObject[pubId] = {};
            }
            var targetId = stringSplit[pubIndex - 1].trim();
            var targetName = pubIndex === 2 ? stringSplit[0].trim() : '';
            dbObject[pubId][targetId] = targetName;
        }
    }
    return dbObject;
}

function createCopiedRotation(rotationId, newRid, rotationName, callback) {
    var Rotations = schemas.rotations;
    Rotations.findById(rotationId, function (err, result) {
        if (err) {
            console.log(err);
            callback();
        } else {
            var rotation = result;
            rotation._id = newRid;
            rotation.name = rotationName;
            rotation.status = 'inactive';
            callback(result._id, rotation);
        }
    });
}

function createCopiedCampaign(cid, newCid, campaignName, callback) {
    var Advertisers = schemas.advertisers;
    Advertisers.aggregate([{$unwind: "$campaigns"}, {$match: {"campaigns._id": mongoose.Types.ObjectId(cid)}},
        {$project: {_id: 1, 'deals': 1, campaigns: 1}}], function (err, result) {
        if (err) {
            console.log(err);
            callback();
        } else {
            var dealId = '';
            for (var i = 0; i < result[0].deals.length; i++) {
                var campaigns = result[0].deals[i].campaigns.map(function (objectId) {
                    return objectId.toString();
                });

                if (campaigns.indexOf(cid) !== -1) {
                    ;
                    dealId = result[0].deals[i]._id;
                    break;
                }
            }
            var campaign = result[0].campaigns;
            campaign._id = newCid;
            campaign.name = campaignName;
            campaign.spent = 0;
            campaign.status = 'inactive';
            callback(result[0]._id, dealId, campaign);
        }
    });

}

function inArray(needle, array) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] == needle) {
            return true;
        }
    }
    return false;
}


function updateSupplies(Publishers, listType, supplySources, campaignName, res) {
    Publishers.aggregate({"$match": {"_id": {"$ne": null}}}, {$unwind: "$bids"}, {$project: {pubName: 1, status: 1, bids: 1}}, {$match: {"bids.status": {$ne: 'deleted'}}}, function (err, result) {
        if (err || result === null) {
            status = 400;
            sendResponse.sendHttpStatus(res, status);
        } else {
            status = 200;
            var supplyObj = {supply: {}};
            supplyObj.supply = result;
            var list = [];
            if (Array.isArray(supplySources)) {
                list = supplySources;
            } else {
                list.push(supplySources);
            }
            for (var i = 0; i < supplyObj.supply.length; i++) {
                var set;
                switch (listType) {
                    case 'black':
                        if (inArray(supplyObj.supply[i].bids._id, list)) {
                            set = {$pull: {"bids.$.campaign": campaignName}};
                        } else {
                            set = {$addToSet: {"bids.$.campaign": campaignName}};
                        }
                        break;

                    case 'white':
                        if (inArray(supplyObj.supply[i].bids._id, list)) {
                            set = {$addToSet: {"bids.$.campaign": campaignName}};
                        } else {
                            set = {$pull: {"bids.$.campaign": campaignName}};
                        }
                        break;

                    default:
                        break;
                }

                Publishers.update({"bids._id": supplyObj.supply[i].bids._id, $or: [{"bids.supplyType": "video-vast"}, {"bids.supplyType": "vast-vast"}]}, set, function (err, result) {
                    if (err) {
                        status = 400;
                    } else {
                        status = 201;
                    }
                });
            }
        }
    });
    sendResponse.sendHttpStatus(res, status);
}