var mongoose = require('mongoose');

exports.findDemandDeals = function(req, res) {
	var status;
	var Advertisers = schemas.advertisers;
        var matchAdv = {"$match": {"_id": {"$ne": null}}};
        if (typeof req.query.advId !== "undefined"){
            var adId = mongoose.Types.ObjectId(req.query.advId);
            matchAdv = {"$match": {"_id": adId}};
        }
	Advertisers.aggregate(matchAdv, {$unwind: "$deals"}, {$project: {name: 1, status: 1, deals: 1}}, {$match: {"deals.status": {$ne: 'deleted' } } }, function (err, result) {
		if (err || result === null) {
			status = 400;
			sendResponse.sendHttpStatus(res, status);
		} else {
			status = 200;
			var demanDelasObj = {demandDeals: {}};
			demanDelasObj.demandDeals = result;
			sendResponse.sendJson(res, status, demanDelasObj);
		}
	});
}


exports.findDemandDealsByAdvertiserId = function(req, res) {
	var status;
	var Advertisers = schemas.advertisers;
	Advertisers.aggregate([{$match: {_id: mongoose.Types.ObjectId(req.params.id)}}, {$unwind: "$deals"}, {$project: {deals: 1}},
									{ $match: {"deals.status": {$ne: 'deleted' } } }], function (err, result) {
		if (err || result === null) {
			status = 400;
			sendResponse.sendHttpStatus(res, status);
		} else {
			status = 200;
			var demanDelasObj = {demandDeals: {}};
			demanDelasObj.demandDeals = result;
			sendResponse.sendJson(res, status, demanDelasObj);
		}
	});
}


exports.findDemandDealsById = function(req, res) {
	var status;
	var Advertisers = schemas.advertisers;
	Advertisers.aggregate([{$unwind: "$deals"}, {$match: {"deals._id": mongoose.Types.ObjectId(req.params.id)}}, {$project: {deals: 1}},
									{ $match: {"deals.status": {$ne: 'deleted' } } }], function (err, result) {
		if (err) {
			status = 400;
			sendResponse.sendHttpStatus(res, status);
		} else {
			status = 200;
			var demanDelasObj = {demandDeals: {}};
			demanDelasObj.demandDeals = result;
			sendResponse.sendJson(res, status, demanDelasObj);
		}
	});
}


exports.addDemandDeal = function(req, res) {
	var status;
	var Advertisers = schemas.advertisers;
	var setParams;
	var falseInput = false;
	var _id = mongoose.Types.ObjectId();

	if (req.body.type === 'display') {
		if (req.body.advId !== undefined && req.body.name !== undefined) {
			setParams = {_id: _id, name:req.body.name, dealType:req.body.type, status: 'active', payout: req.body.payout}; //temporarily still saving payout
		}

	} else if (req.body.type === 'video-rtb' ||req.body.type === 'xml') {
		if (req.body.advId !== undefined && req.body.name !== undefined && req.body.endPoint !== undefined && req.body.forensiq !== undefined) {
			setParams = {_id: _id, name:req.body.name, dealType:req.body.type, endPoint:req.body.endPoint, forensiq:req.body.forensiq, status: 'active'};
		}

	} else if (req.body.type === 'video-vast' || req.body.type === 'video-dsp') {
		if (req.body.advId !== undefined && req.body.name !== undefined && req.body.endPoint !== undefined && req.body.forensiq !== undefined) {
			setParams = {_id: _id, name:req.body.name, dealType:req.body.type, endPoint:req.body.endPoint, payout: req.body.payout, forensiq:req.body.forensiq, status: 'active'};
		}
	}

	if (setParams === undefined) {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	} else {
		Advertisers.findByIdAndUpdate(req.body.advId, { $push: {deals: setParams } }, {new: true}, function (err, result) {
			if (err || result === null) {
				status = 400;
			} else {
				status = 201;
			}
			sendResponse.sendHttpStatus(res, status);
		});
	}
}


exports.editDemandDeal = function(req, res) {
	var status;
	var Advertisers = schemas.advertisers;
	var setParams;
	var falseInput = false;
	var set;

	if (req.body.type === 'video-rtb' || req.body.type === 'xml') {
		if ( req.body.dealId !== undefined && req.body.name !== undefined && req.body.endPoint !== undefined && req.body.status !== undefined && req.body.forensiq !== undefined ) {

			setParams = { '$set': { 'deals.$.id': req.body.dealId, 'deals.$.name': req.body.name, 'deals.$.dealType': req.body.type,
						'deals.$.endPoint': req.body.endPoint, 'deals.$.forensiq': req.body.forensiq, 'deals.$.status': req.body.status} };
		}

	} else if (req.body.type === 'video-vast' || req.body.type === 'video-dsp') {
		if ( req.body.dealId !== undefined && req.body.name !== undefined && req.body.endPoint !== undefined && req.body.status !== undefined && req.body.forensiq !== undefined  ) {

			setParams = { '$set': { 'deals.$.id': req.body.dealId, 'deals.$.name': req.body.name, 'deals.$.dealType': req.body.type,
						'deals.$.endPoint': req.body.endPoint, 'deals.$.payout': req.body.payout, 'deals.$.forensiq': req.body.forensiq, 'deals.$.status': req.body.status} };
		}

	} else if ( req.body.type === 'display' ) {
		if (req.body.dealId !== undefined && req.body.name !== undefined && req.body.status !== undefined) {

			setParams = { '$set': { 'deals.$.id': req.body.dealId,  'deals.$.name': req.body.name, 'deals.$.dealType': req.body.type,
							  'deals.$.payout': req.body.payout, 'deals.$.status': req.body.status} };  //temporarily still saving payout
	    }
    }

	if (setParams === undefined) {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	} else {
		Advertisers.update( { "deals._id": req.body.dealId }, setParams, {new: true}, function (err, result) {
			if (err || result.n === 0) {
				status = 400;
			} else {
				status = 201;
			}
			sendResponse.sendHttpStatus(res, status);
		});
	}
}

exports.deleteDemandDeal = function(req, res) {
	var status;
	var result;
	var Advertisers = schemas.advertisers;
	Advertisers.findOneAndUpdate( { "deals._id": req.body.dealId }, { '$set': { 'deals.$.status': 'deleted' } }, {new: true}, function (err, result) {
			if (err || result.n === 0) {
				status = 400;
			} else {
                                //change relevant campaigns status
                                if (typeof result.deals !== 'undefined' && Array.isArray(result.deals) && result.deals.length > 0){
                                    for (var i = 0; i < result.deals.length; i++){
                                        if (result.deals[i]._id.toString() === req.body.dealId  &&
                                                typeof result.deals[i].campaigns !== 'undefined' &&
                                                Array.isArray(result.deals[i].campaigns) &&
                                                result.deals[i].campaigns.length > 0){
                                            for (var j = 0; j < result.deals[i].campaigns.length; j++){
                                                if (typeof result.deals[i].campaigns[j] !== 'undefined'){
                                                    Advertisers.update({_id: result._id.toString(), "campaigns._id": result.deals[i].campaigns[j].toString()}, { $set: {"campaigns.$.status": 'deleted' } }, {new: true}, function (err, result) {
                                                        if (err){
                                                            console.log('Failed deleting campaign id ' + esult.deals[i].campaigns[j].toString() + ' when deleting demand deal id ' + req.body.dealId + '. Error message -');
                                                            console.log(err);
                                                        }
                                                    });
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
				status = 201;
			}
			sendResponse.sendHttpStatus(res, status);
		});
}
