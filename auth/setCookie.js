var Users = schemas.users;
var tokenDaysNum = 1;

exports.validateUser = function(req, res, callback) {
	userAuth.hashPassword(req.body.password).then((password) => {
		Users.find({"key": req.body.username, "password": password, "status": "active"}, function(err, obj) {
			if (err) throw err;

			if (Object.keys(obj).length === 0) {
				//user not exists.
				callback(false);
			} else {
				//init session user details.
				req.session.userId = obj[0].key;
				req.session.name = obj[0].name;
				req.session.type = obj[0].type;
				req.session.dsp = obj[0].dsp;
				req.session.ssp = obj[0].ssp;
				req.session.xml = obj[0].xml;
				callback(obj);
			}
		});
	});
}


exports.updateUserToken = function(userObj, token, callback) {
	var currentDate = new Date();
	var newDate = new Date(currentDate.setTime( currentDate.getTime() + tokenDaysNum * 86400000 ));
	Users.findByIdAndUpdate(userObj[0]._id, { $set: { token: token, expired: newDate}}, function (err, res) {
		if (err) {
			throw err;
			callback(false);
		} else {
			callback(true);
		}
	});
}


exports.generateToken = function(callback) {
 	require('crypto').randomBytes(48, function(err, buffer) {
	  	var token = buffer.toString('hex');
	  	callback(token);
  	});
}


exports.validateCookieToken = function(req, res, next) {

	console.log(req.cookies.token);

	if (req.session.userId) {
		var currentDate = new Date();

		Users.find({"token": req.cookies.token}, function(err, obj) {
			if (err) throw err;

			if (obj[0] !== undefined && Object.keys(obj[0]).length !== 0 && currentDate < obj[0].expired) {
				next();
			} else {
				res.set({
					'Access-Control-Allow-Headers': 'Content-Type,Authorization',
					'Access-Control-Allow-Origin': 'http://' + config.domain,
					'Access-Control-Allow-Credentials': true,
				});
				res.location('http://' + config.domain + '/login.html');
				res.status(401).json({"message": 'invalid token!'});
			}
		});

	} else {
		res.set({
			'Access-Control-Allow-Headers': 'Content-Type,Authorization',
			'Access-Control-Allow-Origin': 'http://' + config.domain,
			'Access-Control-Allow-Credentials': true,
		});
		res.location('http://' + config.domain + '/login.html');
		res.status(401).json({"message": 'invalid token!'});
	}
}
