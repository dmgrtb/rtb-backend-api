/**
 * @apiDescription This is the Description.
 * It is multiline capable.
 *
 * Last line of Description.
 *
 * @api {Post} /login User authentication
 * @apiName authentication
 * @apiGroup Login
 *
 * @apiPermission none
 *
 * @apiExample {curl} Example usage:
 *     curl -XPOST http://api.rtb2web.com/login -d '{"username": "demouser@dsnrmg.com", "password": "thisismypassword"}'
 *
 * @apiParam {String} username User name
 * @apiParam {String} password User password
 *
 * @apiParamExample {Json} Request template:
 *     {
 *       "username": "<user name::string>",
 *       "password": "<password::string>"
 *     }
 * @apiParamExample {Json} Request example:
 *     {
 *       "username": "demouser@dsnrmg.com",
 *       "password": "thisismypassword"
 *     }
 *
 * @apiSuccess (Success) {String} Success
 * @apiSuccessExample {String} Success Response:
 *     HTTP/1.1 200 OK
 *     success
 *
 * @apiError (Error) {String} Failed! The Username or Password incurrect.
 * @apiErrorExample {String} Error Response:
 *     HTTP/1.1 200 OK
 *     failed!
 *
 * @apiSampleRequest /login
 *
 */
exports.authentication = function(req, res) {

	//validate username & password.
	if (req.body.username !== undefined && req.body.password !== undefined) {
		validateUserAuth(req, res, function(result, token) {
			if (result) {
				sendResponse(res, req, true, token);
			} else {
				sendResponse(res, req, false);
			}
		});

	//validate cookie token.
	// } else if (req.cookies.token !== undefined) {
	// 	cookie.validateCookieToken(req, res, function(result) {
	// 		//valid token.
	// 		if (result) {
	// 			sendResponse(res, true);
	// 		} else {
	// 			sendResponse(res, false);
	// 		}
	// 	});

	//no cookie or username & password.
	} else {
		sendResponse(res, req, false);
	}
}

/**
 * @apiDescription This is the Description.
 * It is multiline capable.
 *
 * Last line of Description.
 *
 * @api {Get} /logout User logout
 * @apiName logout
 * @apiGroup Login
 *
 * @apiPermission none
 *
 * @apiExample {curl} Example usage:
 *     curl -XGET http://api.rtb2web.com/logout
 *
 * @apiSuccessExample {String} Success Response:
 *     HTTP/1.1 200 OK
 *
 * @apiSampleRequest /logout
 *
 */
exports.logout = function(req, res) {
	req.session.destroy();
	res.status(200).end();
}

exports.hashPassword = function(password) {
	var hmac = require('crypto').createHmac('md5', config.authSaltKey);
	hmac.update(password);
	password = hmac.digest('hex');
	return new Promise(function(resolve, reject) {
		resolve(password);
	});
}

function validateUserAuth(req, res, callback) {
	cookie.validateUser(req, res, function(userObj) {
		if (userObj) {
			cookie.generateToken(function(token) {
				cookie.updateUserToken(userObj, token, function(result) {
					if (result) {
						callback(true, token);
					} else {
						//update token in db failed.
						callback(false, null);
					}
				});
			});
		} else {
			//invalid usernamed
			callback(false, null);
		}
	});
}

function sendResponse(response, req, bool, token=null) {
	response.setHeader('Access-Control-Allow-Origin', 'http://' + config.domain);
	response.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    response.setHeader('Access-Control-Allow-Methods', 'GET,POST');
	response.setHeader('Access-Control-Allow-Credentials', true);

	if (bool && token === null) {

		response.write('success', function(err) {
			response.flushHeaders();
			response.end();
		});
	} else if (bool && token !== null) {
        var tokenUrl = config.domain.split('.').splice(1);
		response.cookie('token', token, { domain: tokenUrl.join('.'), maxAge: 60000000, httpOnly: true });
		response.write('success', function(err) {
			response.flushHeaders();
			response.end();
		});
	} else if(!bool) {
		response.location('http://' + config.domain + '/login.html');
		response.end('failed!');
	}
}
