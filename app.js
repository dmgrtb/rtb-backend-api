console.log("Start as", process.env.NODE_ENV);

/* common */
init = require('rtb-common')("init");

/* config */
config = init.config("env");

/* express */
app = init.app();

/* sentry */
if (process.env.NODE_ENV == 'production'){
        init.sentry();
}

/* body-parser */
app.use(init.bodyParser(false).urlencoded({ extended: true }));

/* utiles */
async = init.async();

/* elasticsearch */
esClient = init.es();
esResponseParser = init.esResponseParser();

/* postgres */
pool = init.postgres("rtb_aggregation");

/* mongodb */
mongoose = init.mongoose();
schemas = init.schemas();

/* timeout */
timeout = init.timeout();
app.use(timeout(120000));

/* cookieParser */
app.use(init.cookieParser()());

/* formidable */
formidable = init.formidable();

/* session */
session = init.session();
app.use(session({
	  secret: 'keyboard cat',
	  resave: false,
	  saveUninitialized: true
}));

/* lodash */
_ = init.lodash();

/* acl */
acl = init.acl();

/* send response */
sendResponse = require('./modules/sendResponse');

/* reports */
const querySearch = require('./reports/querySearch');
const queryAutocomplete = require('./reports/queryAutoComplete');

/* auth */
userAuth = require('./auth/userAuth');
cookie = require('./auth/setCookie');

/* advertisers */
const advertisers = require('./advertisers/advertisersCrud');
const demandDeals = require('./advertisers/demandDealsCrud');
const campaigns = require('./advertisers/campaignsCrud');
const creatives = require('./advertisers/creativesCrud');

/* publishers */
const publishers = require('./publishers/publishersCrud');
const supply = require('./publishers/supplyCrud');

/* users */
const accessControl = require('./users/usersCrud');

app.options('/advertisers/creatives/bulk/*', function(req, res) {
        res.setHeader('Access-Control-Allow-Headers', 'cache-control, x-requested-with');
        res.setHeader('Access-Control-Allow-Credentials', true);
        res.setHeader('Access-Control-Allow-Origin', 'http://' + config.domain);
        res.setHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
        res.setHeader('Access-Control-Max-Age', '1728000');
        res.status(200).json();
});


app.options('/*', function(req, res) {
	   var headers = {};
	   headers['Access-Control-Allow-Headers'] =  'X-PINGARUNER';
	   headers['Access-Control-Allow-Credentials'] = true;
	   headers['Access-Control-Allow-Origin'] = 'http://' + config.domain;
	   headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS, PUT, DELETE';
	   headers['Access-Control-Max-Age'] = '1728000';
	   headers["Content-Length"] = '0';
	   res.writeHead(200, headers);
	   res.end();
});

app.post('/login', function(req, res) {
	userAuth.authentication(req, res);
});

app.get('/logout', function(req, res) {
	userAuth.logout(req, res);
});

app.get('/reports', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
 	if (req.query.searchtype === 'search') {
      querySearch.parse(req, res);
    } else if(req.query.searchtype === 'autocomplete') {
      queryAutocomplete(req, res);
    }
});

app.get('/advertisers', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	advertisers.findAdvertiser(req, res);
});

app.post('/advertisers', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	advertisers.addAdvertiser(req, res);
});

app.put('/advertisers', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	advertisers.editAdvertiser(req, res);
});

app.delete('/advertisers', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	advertisers.deleteAdvertiser(req, res);
});


//Demand Deals
app.get('/advertisers/demanddeals/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	demandDeals.findDemandDeals(req, res);
});

app.get('/advertisers/demanddeals/:id', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	demandDeals.findDemandDealsById(req, res);
});

app.get('/advertisers/:id/demanddeals/', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	demandDeals.findDemandDealsByAdvertiserId(req, res);
});

app.post('/advertisers/demanddeals/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	demandDeals.addDemandDeal(req, res);
});

app.put('/advertisers/demanddeals/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	demandDeals.editDemandDeal(req, res);
});

app.delete('/advertisers/demanddeals/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	demandDeals.deleteDemandDeal(req, res);
});


//Campaigns
app.get('/advertisers/campaigns/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	campaigns.findCampaigns(req, res);
});

app.get('/advertisers/campaigns/rotations/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	campaigns.findRotations(req, res);
});

app.get('/advertisers/campaigns/countries/', [cookie.validateCookieToken, acl.middleware(3)], function(req, res) {
	campaigns.findCountries(req, res);
});

app.get('/advertisers/campaigns/categories/', [cookie.validateCookieToken, acl.middleware(3)], function(req, res) {
	campaigns.findCategories(req, res);
});

app.get('/advertisers/campaigns/:id', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	campaigns.findCampaignById(req, res);
});

app.get('/advertisers/campaigns/rotations/:id', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	campaigns.findRotationById(req, res);
});

app.post('/advertisers/campaigns/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	campaigns.addCampaign(req, res);
});

app.post('/advertisers/campaigns/rotations/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	campaigns.addRotation(req, res);
});

app.put('/advertisers/campaigns/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	campaigns.editCampaign(req, res);
});

app.put('/advertisers/campaigns/rotations/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	campaigns.editRotation(req, res);
});

app.delete('/advertisers/campaigns/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	campaigns.deleteCampaign(req, res);
});

app.delete('/advertisers/campaigns/rotations/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	campaigns.deleteRotation(req, res);
});


//Creatives
app.get('/advertisers/creatives/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	creatives.findCreatives(req, res);
});

app.get('/advertisers/creatives/:id', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	creatives.findCreativeById(req, res);
});

app.get('/advertisers/:id/creatives/', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	creatives.findCreativesByAdvertiserId(req, res);
});

app.post('/advertisers/creatives/validate/', function(req, res) {
	creatives.verifyCreativeNames(req, res);
});

app.post('/advertisers/creatives/bulk/', function(req, res) {
	creatives.uploadBulk(req, res);
});

app.post('/advertisers/creatives/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	creatives.addCreative(req, res);
});

app.put('/advertisers/creatives/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	creatives.editCreative(req, res);
});

app.delete('/advertisers/creatives/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	creatives.deleteCreative(req, res);
});


//Publishers
app.get('/publishers', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	publishers.findPublisher(req, res);
});

app.get('/publishers/', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	publishers.findPublisherById(req, res);
});

app.post('/publishers', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	publishers.addPublisher(req, res);
});

app.put('/publishers', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	publishers.editPublisher(req, res);
});

app.delete('/publishers', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	publishers.deletePublisher(req, res);
});


//Supply Source
app.get('/publishers/supply/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	supply.findSupply(req, res);
});

app.post('/publishers/supply/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	supply.addSupply(req, res);
});

app.get('/publishers/supply/byid/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	supply.findSupplyById(req, res);
});

app.put('/publishers/supply/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	supply.editSupply(req, res);
});

app.delete('/publishers/supply/', [cookie.validateCookieToken, acl.middleware(2)], function(req, res) {
	supply.deleteSupply(req, res);
});


//Users
app.get('/acl', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	accessControl.findUser(req, res);
});

app.get('/acl/users/:userId', function(req, res) {
	accessControl.findUserByUserId(req, res);
});

app.get('/acl/users/:userId/role', function(req, res) {
	accessControl.getRolesByUserId(req, res);
});

app.get('/acl/users/:usersId/:resourceId', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	accessControl.getUserPermissions(req, res);
});

app.post('/acl/users/', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	accessControl.addNewUser(req, res);
});

app.post('/acl/roles/roleChild', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	accessControl.addRole(req, res);
});

app.post('/acl/roles/roleParent', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	accessControl.addRoleParents(req, res);
});

app.post('/acl/users/role', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	accessControl.addUserRole(req, res);
});

app.delete('/acl/roles/', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	accessControl.removeRole(req, res);
});

app.delete('/acl/roles/permissions', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	accessControl.removeRolePermissions(req, res);
});

app.delete('/acl/users/role', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	accessControl.removeUserRole(req, res);
});

app.delete('/acl/users/roleParent', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	accessControl.removeRoleParents(req, res);
});

app.delete('/acl/resource', [cookie.validateCookieToken, acl.middleware(1)], function(req, res) {
	accessControl.removeResource(req, res);
});

init.startServer();
