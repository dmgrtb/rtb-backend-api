endpoints= {
    "lkqd": {
        "env": {
            "desktop": 'Desktop',
            "web": 'MobileWeb',
            "inapp": 'InApp'
        },
        "macro": {
            "desktopUrl": 'http://ssp.rtb2dmg.com/st/',
            "desktopRest": '&cb=$$cachebuster$$&domain=$$domain$$&pageurl=$$pageurl$$&w=$$width$$&h=$$height$$&ua=$$useragent$$&ip=$$ip$$&dnt=$$dnt$$devicetype=$$devicetype$$&geocountry=$$geocountry$$&iploclat=$$iploclat$$&iploclong=$$iploclong$$&contentid=$$contentid$$&categoryiab=$$categoryiab$$&cookieid=$$cookieid$$&siteid=$$siteid$$&contentlength=$$contentlength$$&contettitle=$$contettitle$$&aid=$$aid$$&rt=display',
            "webUrl": 'http://ssp.rtb2dmg.com/st/',
            "webRest": '&cb=$$cachebuster$$&domain=$$domain$$&pageurl=$$pageurl$$&w=$$width$$&h=$$height$$&ua=$$useragent$$&ip=$$ip$$&dnt=$$dnt$$devicetype=$$devicetype$$&geocountry=$$geocountry$$&iploclat=$$iploclat$$&iploclong=$$iploclong$$&contentid=$$contentid$$&categoryiab=$$categoryiab$$&cookieid=$$cookieid$$&siteid=$$siteid$$&contentlength=$$contentlength$$&contettitle=$$contettitle$$&aid=$$aid$$&idfasha1hex=$$idfasha1hex$$&deviceid=$$deviceid$$&osversion=$$osversion$$&carrier=$$ispcellular$$&ifa=$$idfa$$&rt=display',
            "inappUrl": 'http://ssp.rtb2dmg.com/st/',
            "inappRest": '&w=$$width$$&h=$$height$$&ip=$$ip$$&name=$$appname$$&bundle=$$bundleid$$&storeurl=$$appstoreurl$$&cb=$$cachebuster$$&iploclat=$$iploclat$$&iploclong=$$iploclong$$&categoryiab=$$categoryiab$$&geocountry=$$geocountry$$&ua=$$useragent$$&dnt=$$dnt$$&osversion=$$osversion$$&ifa=$$idfa$$&aid=$$aid$$&idfasha1hex=$$idfasha1hex$$&carrier=$$ispcellular$$&deviceid=$$deviceid$$&appver=$$appversion$$&rt=display'
        }
    },
    
    "verta": {
        "env": {
            "desktop": 'Desktop',
            "web": 'MobileWeb',
            "inapp": 'InApp'
        },
        "macro": {
            "desktopUrl": 'http://ssp.rtb2dmg.com/st/',
            "desktopRest": '&cb={cb}&domain={site_url}&pageurl={site_url}&w={width}&h={height}&ua={ua}&ip={uip}&dnt={dnt}&devicetype={device_connection}&geocountry={geo_country}&iploclat={geo_lat}&iploclong={geo_lon}&contentid={video_id}&categoryiab={site_category}&siteid={aid}&contentlength={video_duration}&contettitle={video_title}&aid={adid}&rt=display',
            "webUrl": 'http://ssp.rtb2dmg.com/st/',
            "webRest": '&cb={cb}&domain={site_url}&pageurl={site_url}&w={width}&h={height}&ua={ua}&ip={uip}&dnt={dnt}devicetype={device_connection}&geocountry={geo_country}&iploclat={geo_lat}&iploclong={geo_lon}&contentid={video_id}&categoryiab={site_category}&siteid={aid}&contentlength={video_duration}&contettitle={video_title}&aid={adid}&idfasha1hex={idfa_sha1}&carrier={device_isp}&ifa={idfa}&rt=display',
            "inappUrl": 'http://ssp.rtb2dmg.com/st/',
            "inappRest": '&w={width}&h={height}&ip={uip}&name={app_name}&bundle={app_bundle}&storeurl={app_store_url}&cb={cb}&iploclat={geo_lat}&iploclong={geo_lon}&categoryiab={site_category}&geocountry={geo_country}&ua={ua}&dnt={dnt}&siteid={aid}&ifa={idfa}&aid={adid}&idfasha1hex={idfa_sha1}&carrier={device_isp}&appver={app_version}&rt=display'
        }
    },
    
    "cedato": {
        "env": {
            "desktop": 'Desktop',
            "inapp": 'InApp'
        },
        "macro": {
            "desktopUrl": 'http://ssp.rtb2dmg.com/st/',
            "desktopRest": '&w=[CONTAINER_WIDTH]&h=[CONTAINER_HEIGHT]&ip=[IP]&name=[APP_NAME]&bundle=[APP_BUNDLE]&storeurl=[APP_STORE_URL]&cb=[CB]&iploclat=[LOCATION_LAT]&iploclong=[LOCATION_LONG]&categoryiab=[APP_CATEGORY]&geocountry=[LOCATION]&ua=[UA]&dnt=[DNT]&osversion=[DEVICEOSV]&ifa=[APP_IDFA]&aid=[APP_AID]&deviceid=[DEVICEID]&appver=[APP_VER]&rt=display',
            "inappUrl": 'http://ssp.rtb2dmg.com/st/',
            "inappRest": '&w=[CONTAINER_WIDTH]&h=[CONTAINER_HEIGHT]&ip=[IP]&name=[APP_NAME]&bundle=[APP_BUNDLE]&storeurl=[APP_STORE_URL]&cb=[CB]&iploclat=[LOCATION_LAT]&iploclong=[LOCATION_LONG]&categoryiab=[APP_CATEGORY]&geocountry=[LOCATION]&ua=[UA]&dnt=[DNT]&osversion=[DEVICEOSV]&ifa=[APP_IDFA]&aid=[APP_AID]&deviceid=[DEVICEID]&appver=[APP_VER]&rt=display'
        }
    },
    
    "streamrail": {
        "env": {
            "desktop": 'Desktop'
        },
        "macro": {
            "desktopUrl": 'http://ssp.rtb2dmg.com/st/',
            "desktopRest": '&cb=[CB]&domain=[PAGE_URL]&pageurl=[PAGE_URL]&w=[WIDTH]&h=[HEIGHT]&ua=[UA]&ip=[IP]&iploclat=[USER_LAT]&iploclong=[USER_LON]&contentid=[VIDEO_ID]&contentlength=[VIDEO_DURATION]&contettitle=[VIDEO_TITLE]&rt=display'
        }
    },
    
    "optimatic": {
        "env": {
            "desktop": 'Desktop'
        },
        "macro": {
            "desktopUrl": 'http://ssp.rtb2dmg.com/st/',
            "desktopRest": '&cb=[random_number]&domain=[page_url]&pageurl=[page_url]&w=[video_width]&h=[video_height]&ua=[opt_user_agent]&ip=[opt_ip_address]&dnt=[opt_dnt]&iploclat=[opt_latitude]&iploclong=[opt_longitude]&contentid=[video_id]&siteid=[placement_id]&contettitle=[video_title]&rt=display'
        }
    },
    
    "springserve": {
        "env": {
            "desktop": 'Desktop'
        },
        "macro": {
            "desktopUrl": 'http://ssp.rtb2dmg.com/st/',
            "desktopRest": '&cb={{CACHEBUSTER}}&domain={{DOMAIN}}&pageurl={{URL}}&w={{WIDTH}}&h={{HEIGHT}}&ua={{USER_AGENT}}&ip={{IP}}&dnt={{DNT}}&geocountry={{COUNTRY}}&iploclat={{LAT}}&iploclong={{LON}}&contentid={{CONTENT_ID}}&categoryiab={{IAB_CATEGORY}}&siteid={{SUB_ID}}&contentlength={{DURATION}}&contettitle={{ENCODED_VIDEO_TITLE}}&rt=display'
        }
    },
    
    "aniview": {
        "env": {
            "desktop": 'Desktop',
            "web": 'MobileWeb',
            "inapp": 'InApp'
        },
        "macro": {
            "desktopUrl": 'http://ssp.rtb2dmg.com/st/',
            "desktopRest": '&cb=[AV_RANDOM]&domain=[AV_DOMAIN]&pageurl=[AV_URL]&w=[AV_WIDTH]&h=[AV_HEIGHT]&ua=[AV_USERAGENT]&ip=[AV_IP]&dnt=[AV_DNT]&&iploclat=[AV_LATITUDE]&iploclong=[AV_LONGITUDE]&contentid=[AV_VIDEOID]&categoryiab=[AV_CATEGORY]&referer=[AV_URL]&contentlength=[AV_DURATION]&contettitle=[AV_TITLE]&aid=[AV_AID]&rt=display',
            "webUrl": 'http://ssp.rtb2dmg.com/st/',
            "webRest": '&cb=[AV_RANDOM]&domain=[AV_DOMAIN]&pageurl=[AV_URL]&w=[AV_WIDTH]&h=[AV_HEIGHT]&ua=[AV_USERAGENT]&ip=[AV_IP]&dnt=[AV_DNT]&&iploclat=[AV_LATITUDE]&iploclong=[AV_LONGITUDE]&contentid=[AV_VIDEOID]&categoryiab=[AV_CATEGORY]&referer=[AV_URL]&contentlength=[AV_DURATION]&contettitle=[AV_TITLE]&aid=[AV_AID]&idfasha1hex=[AV_IDFASHA1]&osversion=[AV_OSVERS]&carrier=[AV_CARRIER]&ifa=[AV_IDFA]&rt=display',
            "inappUrl": 'http://ssp.rtb2dmg.com/st/',
            "inappRest": '&[AV_WIDTH]&h=[AV_HEIGHT]&ip=[AV_IP]&name=[AV_APPNAME]&bundle=[AV_APPPKGNAME]&storeurl=[AV_APPSTOREURL]&cb=[AV_RANDOM]&iploclat=[AV_LATITUDE]&iploclong=[AV_LONGITUDE]&categoryiab=[AV_CATEGORY]&ua=[AV_USERAGENT]&dnt=[AV_DNT]&osversion=[AV_OSVERS]&ifa=[AV_IDFA]&aid=[AV_AID]&idfasha1hex=[AV_IDFASHA1]&carrier=[AV_CARRIER]&appver=[AV_APPVERS]&rt=display'
        }
    },
    
    "sourceknowladge": {
        "env": {
            "desktop": 'Desktop',
            "web": 'MobileWeb',
            "inapp": 'InApp'
        },
        "macro": {
            "desktopUrl": 'http://ssp.rtb2dmg.com/st/',
            "desktopRest": '&cb={random}&domain={site.domain}&pageurl={site.page}&w={imp.video.w}&h={imp.video.h}&ua={device.ua}&ip={device.ip}&dnt={device.dnt}&geocountry={device.geo.counrty}&iploclat={device.geo.ext.lat}&iploclong={device.geo.ext.lon}&contentid={sha1.site.page}&referer={site.ref}&siteid={site.id}&contentlength={imp.video.maxduration}&contettitle={media_title_enc}&aid={app.id}&rt=display',
            "webUrl": 'http://ssp.rtb2dmg.com/st/',
            "webRest": '&cb={random}&domain={site.domain}&pageurl={site.page}&w={imp.video.w}&h={imp.video.h}&ua={device.ua}&ip={device.ip}&dnt={device.dnt}&geocountry={device.geo.counrty}&iploclat={device.geo.ext.lat}&iploclong={device.geo.ext.lon}&contentid={sha1.site.page}&referer={site.ref}&siteid={site.id}&contentlength={imp.video.maxduration}&contettitle={media_title_enc}&aid={device.ifa}&={device.ifa}&idfasha1hex={device.dpidsha1}&osversion={device.osv}&carrier={device.carrier}&ifa={device.ifa}&rt=display',
            "inappUrl": 'http://ssp.rtb2dmg.com/st/',
            "inappRest": '&w={imp.video.w}&h={imp.video.h}&ip={device.ip}&name={app.name}&bundle={app.bundle}&storeurl={app.storeurl}&cb={random}&iploclat={device.geo.ext.lat}&iploclong={device.geo.ext.lon}&geocountry={device.geo.country}&ua={device.ua}&dnt={device.dnt}&osversion={device.osv}&ifa={device.ifa}&aid={device.ifa}&idfasha1hex={device.dpidsha1}&carrier={device.carrier}&appid={app.id}&appver={app.ver}&rt=display'
        }
    },
    
    "other": {
        "env": {
            "desktop": 'Desktop',
            "web": 'MobileWeb',
            "inapp": 'InApp'
        },
        "macro": {
            "desktopUrl": 'http://ssp.rtb2dmg.com/st/',
            "desktopRest": '&cb=[REPLACE_ME]&domain=[REPLACE_ME]&pageurl=[REPLACE_ME]&w=[REPLACE_ME]&h=[REPLACE_ME]&ua=[REPLACE_ME]&ip=[REPLACE_ME]&dnt=[REPLACE_ME]&devicetype=[REPLACE_ME]&geocountry=[REPLACE_ME]&iploclat=[REPLACE_ME]&iploclong=[REPLACE_ME]&contentid=[REPLACE_ME]&categoryiab=[REPLACE_ME]&referer=[REPLACE_ME]&cookieid=[REPLACE_ME]&siteid=[REPLACE_ME]&contentlength=[REPLACE_ME]&contettitle=[REPLACE_ME]&aid=[REPLACE_ME]&rt=display',
            "webUrl": 'http://ssp.rtb2dmg.com/st/',
            "webRest": '&cb=[REPLACE_ME]&domain=[REPLACE_ME]&pageurl=[REPLACE_ME]&w=[REPLACE_ME]&h=[REPLACE_ME]&ua=[REPLACE_ME]&ip=[REPLACE_ME]&dnt=[REPLACE_ME]&devicetype=[REPLACE_ME]&geocountry=[REPLACE_ME]&iploclat=[REPLACE_ME]&iploclong=[REPLACE_ME]&contentid=[REPLACE_ME]&categoryiab=[REPLACE_ME]&referer=[REPLACE_ME]&cookieid=[REPLACE_ME]&siteid=[REPLACE_ME]&contentlength=[REPLACE_ME]&contettitle=[REPLACE_ME]&aid=[REPLACE_ME]&idfasha1hex=[REPLACE_ME]&deviceid=[REPLACE_ME]&osversion=[REPLACE_ME]&carrier=[REPLACE_ME]&ifa=[REPLACE_ME]&rt=display',
            "inappUrl": 'http://ssp.rtb2dmg.com/st/',
            "inappRest": '&w=[REPLACE_ME]&h=[REPLACE_ME]&ip=[REPLACE_ME]&name=[REPLACE_ME]&bundle=[REPLACE_ME]&storeurl=[REPLACE_ME]&cb=[REPLACE_ME]&iploclat=[REPLACE_ME]&iploclong=[REPLACE_ME]&categoryiab=[REPLACE_ME]&geocountry=[REPLACE_ME]&ua=[REPLACE_ME]&dnt=[REPLACE_ME]&osversion=[REPLACE_ME]&ifa=[REPLACE_ME]&aid=[REPLACE_ME]&idfasha1hex=[REPLACE_ME]&carrier=[REPLACE_ME]&deviceid=[REPLACE_ME]&appid=[REPLACE_ME]&appver=[REPLACE_ME]&rt=display'
        }
    },
    
    "xml": {
        "Url": 'http://ssp.rtb2web.com/st/',
        "Rest": 'subid=[SUBID]&keywords=[KEYWORDS]&ua=[USER_AGENT]&ip=[IP]&pageurl=[PAGEURL]'
    }
};


module.exports = endpoints;