endpoints = {
    "MediaSmart": "?&sub2=%placement_id%&sub3=%placement_name%&sub4=%udid_raw%&sub5=%placement_name%",
    "PropellerAds": "?PublisherId={zoneid}&partner_var=${SUBID}",
    "ZeroPark": "?PublisherId={target}&partner_var={cid}",
    "xml": "?PublisherId={search_referrer_domain}&SubId1={pubfeed}.{subid}&partner_var={conversion}"
};

module.exports = endpoints;