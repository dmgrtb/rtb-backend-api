module.exports = {
  "development": {
      "port": 8081,
      "logsPath": appPath + "logs/",
      "mongoDb": "mongodb://localhost:27017/rtbui",
"es": {
        "host": "192.168.80.101:9200",
          "apiVersion": "2.4",
          "requestTimeout": 3600000, //1000 * 60 * 60,
        "keepAlive": false
      },
      "postgres": {
              "rtb_aggregation": {
                      "user": "postgres",
                      "password": "postgres",
                      "database": "rtb_aggregation",
                      "host": "82.166.136.134",
                      "port": 5432,
                      "max": 20, // max number of clients in the pool
                      "idleTimeoutMillis": 30000, // how long a client is allowed to remain idle before being closed
              }
      },
      "domain": "www.rtbdev.com",
      "dspUrl": "http://dev.dsp.rtb2web.com/dt/",
      "sspUrl": "http://dev.ssp.rtb2web.com/st/",
      "vast2VastUrl": "http://dev.ssp.rtb2web.com/vt/",
      "creativeUploadUrl": "/var/www/html/rtb-backend-api/uploads/",
      "creativeExternalUrl": "http://cdn.rtb2web.com/",
      "autoCompleteMaxResults": 100,
      "frequencyCap": 5,
      "authSaltKey": "saltDMGkey",
      "native": {
            "assets": [
                {
                    "type1": "title",
                    "required": 1
                },
                {
                    "type1": "img",
                    "type2": "3",
                    "required": 1
                },
                {
                    "type1": "img",
                    "type2": "1",
                    "required": 0
                },
                {
                    "type1": "data",
                    "type2": "2",
                    "required": 0
                },
                {
                    "type1": "data",
                    "type2": "3",
                    "required": 0
                },
                {
                    "type1": "data",
                    "type2": "12",
                    "required": 0
                }
            ],
            "clickTrackers": [
                   "http://dev.evt.rtb2web.com/click?auctionid=${AUCTION_ID}&bidid=${AUCTION_BID_ID}&impid=${AUCTION_IMP_ID}&seatid=${AUCTION_SEAT_ID}&adid=${AUCTION_AD_ID}"
            ],
            "impTrackers": [
                "http://dev.evt.rtb2web.com/imp?auctionid=${AUCTION_ID}&bidid=${AUCTION_BID_ID}&impid=${AUCTION_IMP_ID}&seatid=${AUCTION_SEAT_ID}&adid=${AUCTION_AD_ID}&price=${AUCTION_PRICE}&cur=${AUCTION_CURRENCY}"
            ]
    },
    "schemasPath": appPath + "modules/schemas",
    "sentry": {
            "url": "https://6a0a98ba380649da88a5799b5cf06dc1:ff77b4bd459e452bad89f8b94a96ceb7@sentry.io/196437",
            "erroCode": 500
    }
  },
  "qa": {
          "port": 8080,
          "logsPath": appPath + "logs/",
          "mongoDb": "mongodb://localhost:27017/rtbui",
          "es": {
            "host": "http://es_admin:92605beb4736d80b@localhost:9200",
            "apiVersion": "2.4",
            "requestTimeout": 3600000, //1000 * 60 * 60,
            "keepAlive": false
          },
          "postgres": {
                  "rtb_aggregation": {
                          "user": "postgres",
                          "password": "postgres",
                          "database": "rtb_aggregation",
                          "host": "localhost",
                          "port": 5432,
                          "max": 20, // max number of clients in the pool
                          "idleTimeoutMillis": 30000, // how long a client is allowed to remain idle before being closed
                  }
          },
          "domain": "www.rtbdev.com",
          "dspUrl": "http://qa.dsp.rtb2web.com/dt/",
          "sspUrl": "http://qa.ssp.rtb2web.com/st/",
          "vast2VastUrl": "http://qa.ssp.rtb2web.com/vt/",
          "creativeUploadUrl": "/var/www/html/rtb-backend-api/uploads/",
          "creativeExternalUrl": "http://cdn.rtb2web.com/",
          "autoCompleteMaxResults": 100,
          "frequencyCap": 5,
          "authSaltKey": "saltDMGkey",
          "native": {
                "assets": [
                    {
                        "type1": "title",
                        "required": 1
                    },
                    {
                        "type1": "img",
                        "type2": "3",
                        "required": 1
                    },
                    {
                        "type1": "img",
                        "type2": "1",
                        "required": 0
                    },
                    {
                        "type1": "data",
                        "type2": "2",
                        "required": 0
                    },
                    {
                        "type1": "data",
                        "type2": "3",
                        "required": 0
                    },
                    {
                        "type1": "data",
                        "type2": "12",
                        "required": 0
                    }
                ],
                "clickTrackers": [
                       "http://qa.evt.rtb2web.com/click?auctionid=${AUCTION_ID}&bidid=${AUCTION_BID_ID}&impid=${AUCTION_IMP_ID}&seatid=${AUCTION_SEAT_ID}&adid=${AUCTION_AD_ID}"
                ],
                "impTrackers": [
                    "http://qa.evt.rtb2web.com/imp?auctionid=${AUCTION_ID}&bidid=${AUCTION_BID_ID}&impid=${AUCTION_IMP_ID}&seatid=${AUCTION_SEAT_ID}&adid=${AUCTION_AD_ID}&price=${AUCTION_PRICE}&cur=${AUCTION_CURRENCY}"
                ]
        },
        "schemasPath": appPath + "modules/schemas",
        "sentry": {
                "url": "https://6a0a98ba380649da88a5799b5cf06dc1:ff77b4bd459e452bad89f8b94a96ceb7@sentry.io/196437",
                "erroCode": 500
        }
  },
  "ci": {
          "port": 8080,
          "logsPath": appPath + "logs/",
          "mongoDb": "mongodb://localhost:27017/rtb",
          "es": {
            "host": "http://es_admin:92605beb4736d80b@localhost:9200",
            "apiVersion": "2.4",
            "requestTimeout": 3600000, //1000 * 60 * 60,
            "keepAlive": false
          },
          "postgres": {
                  "rtb_aggregation": {
                          "user": "postgres",
                          "password": "postgres",
                          "database": "rtb_aggregation",
                          "host": "localhost",
                          "port": 5432,
                          "max": 20, // max number of clients in the pool
                          "idleTimeoutMillis": 30000, // how long a client is allowed to remain idle before being closed
                  }
          },
          "domain": "www.rtbdev.com",
          "dspUrl": "http://ci.dsp.rtb2web.com/dt/",
          "sspUrl": "http://ci.ssp.rtb2web.com/st/",
          "vast2VastUrl": "http://ci.ssp.rtb2web.com/vt/",
          "creativeUploadUrl": "/var/www/html/rtb-backend-api/uploads/",
          "creativeExternalUrl": "http://cdn.rtb2web.com/",
          "autoCompleteMaxResults": 100,
          "frequencyCap": 5,
          "authSaltKey": "saltDMGkey",
          "native": {
                "assets": [
                    {
                        "type1": "title",
                        "required": 1
                    },
                    {
                        "type1": "img",
                        "type2": "3",
                        "required": 1
                    },
                    {
                        "type1": "img",
                        "type2": "1",
                        "required": 0
                    },
                    {
                        "type1": "data",
                        "type2": "2",
                        "required": 0
                    },
                    {
                        "type1": "data",
                        "type2": "3",
                        "required": 0
                    },
                    {
                        "type1": "data",
                        "type2": "12",
                        "required": 0
                    }
                ],
                "clickTrackers": [
                       "http://ci.evt.rtb2web.com/click?auctionid=${AUCTION_ID}&bidid=${AUCTION_BID_ID}&impid=${AUCTION_IMP_ID}&seatid=${AUCTION_SEAT_ID}&adid=${AUCTION_AD_ID}"
                ],
                "impTrackers": [
                    "http://ci.evt.rtb2web.com/imp?auctionid=${AUCTION_ID}&bidid=${AUCTION_BID_ID}&impid=${AUCTION_IMP_ID}&seatid=${AUCTION_SEAT_ID}&adid=${AUCTION_AD_ID}&price=${AUCTION_PRICE}&cur=${AUCTION_CURRENCY}"
                ]
        },
        "schemasPath": appPath + "modules/schemas",
        "sentry": {
                "url": "https://6a0a98ba380649da88a5799b5cf06dc1:ff77b4bd459e452bad89f8b94a96ceb7@sentry.io/196437",
                "erroCode": 500
        }
  },
  "production": {
      "port": 8080,
      "mongoDb": "mongodb://dmgAdmin:b213412ed90ca0eca5@rtb-mongo1:27017/rtb",
      "es": {
        "host": "http://es_admin:92605beb4736d80b@rtb-elk-es-master-1:9200",
        "apiVersion": "2.4",
        "requestTimeout": 3600000, //1000 * 60 * 60,
        "keepAlive": false
      },
      "postgres": {
              "rtb_aggregation": {
                      "user": "postgres",
                      "password": "postgres",
                      "database": "rtb_aggregation",
                      "host": "rtb-postgres-node1",
                      "port": 5432,
                      "max": 20, // max number of clients in the pool
                      "idleTimeoutMillis": 30000, // how long a client is allowed to remain idle before being closed
              }
      },
      "domain": "console.rtb2web.com",
      "dspUrl": "http://dsp.rtb2web.com/dt/",
      "sspUrl": "http://ssp.rtb2web.com/st/",
      "vast2VastUrl": "http://ssp.rtb2web.com/vt/",
      "creativeUploadUrl": "/mnt/creatives/",
      "creativeExternalUrl": "http://cdn.rtb2web.com/",
      "autoCompleteMaxResults": 100,
      "frequencyCap": 5,
      "authSaltKey": "saltDMGkey",
      "native": {
            "assets": [
                {
                    "type1": "title",
                    "required": 1
                },
                {
                    "type1": "img",
                    "type2": "3",
                    "required": 1
                },
                {
                    "type1": "img",
                    "type2": "1",
                    "required": 0
                },
                {
                    "type1": "data",
                    "type2": "2",
                    "required": 0
                },
                {
                    "type1": "data",
                    "type2": "3",
                    "required": 0
                },
                {
                    "type1": "data",
                    "type2": "12",
                    "required": 0
                }
            ],
            "clickTrackers": [
                   "http://evt.rtb2web.com/click?auctionid=${AUCTION_ID}&bidid=${AUCTION_BID_ID}&impid=${AUCTION_IMP_ID}&seatid=${AUCTION_SEAT_ID}&adid=${AUCTION_AD_ID}"
            ],
            "impTrackers": [
                "http://evt.rtb2web.com/imp?auctionid=${AUCTION_ID}&bidid=${AUCTION_BID_ID}&impid=${AUCTION_IMP_ID}&seatid=${AUCTION_SEAT_ID}&adid=${AUCTION_AD_ID}&price=${AUCTION_PRICE}&cur=${AUCTION_CURRENCY}"
            ]
    },
    "schemasPath": appPath + "modules/schemas",
    "sentry": {
            "url": "https://6a0a98ba380649da88a5799b5cf06dc1:ff77b4bd459e452bad89f8b94a96ceb7@sentry.io/196437",
            "erroCode": 500
    }
  }
}
