exports.findUser = function(req, res) {
	var Users = schemas.users;
	Users.find({status: 'active' }, {name: 1}, function (err, result) {
		if (err || result === null) {
			var status = 201;
			sendResponse.sendHttpStatus(res, status);
		} else {
			var status = 200;
			var advObj = {users: {}};
			advObj.users = result;
			sendResponse.sendJson(res, status, advObj);
		}
	});
}


exports.findUserByUserId = function(req, res) {
	var Users = schemas.users;

	Users.find({key: req.params.userId}, function (err, result) {
		if (err || result === null) {
			var status = 201;
			sendResponse.sendHttpStatus(res, status);
		} else {
			var status = 200;
			sendResponse.sendJson(res, status, result);
		}
	});
}


function findUserByUsername(username) {
	var Users = schemas.users;
	return new Promise(function(resolve, reject) {

		Users.find({key: username}, function (err, result) {
			if (err || Object.keys(result).length === 0) {
				reject();
			} else {
				resolve(result);
			}
		});
	});
}


exports.getRolesByUserId = function(req, res) {

	var status = 201;

	if (req.params.userId) {

		acl.userRoles(req.params.userId, function(err, roles) {

			if (!err) {
				status = 200;

				//omit fields other than role field.
				roles = _.pull(roles, 'password', 'token', 'expired', 'status', 'ssp', 'dsp', 'xml', 'name', 'type');

				getResourcesByRoles(roles, function(resources) {
						sendResponse.sendJson(res, status, resources);
						return;
				});

			} else {
				status = 500;
				sendResponse.sendHttpStatus(res, status);
			}
		});
	} else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}


function getResourcesByRoles(roles, callback) {

	var itemsProcessed = 0;
	var resourcesArr = [];
	var i = 5;

	_.forEach(roles, function(value) {
		itemsProcessed++;
		acl.whatResources(value, function(err, result) {

				Object.keys(result).forEach(function(key) {

					var key = key.substr(1);

					//order categories by key.
					if (key == 'reports') {
						resourcesArr[0] = key;
					} else if (key == 'publishers') {
						resourcesArr[1] = key;
					} else if (key == 'advertisers') {
						resourcesArr[2] = key;
					} else if (key == 'advertisers/creatives') {
						resourcesArr[3] = key;
					} else if (key == 'advertisers/campaigns') {
						resourcesArr[4] = key;
					} else {
						resourcesArr[i] = key;
						i++;
					}
				});

				//remove empty elements.
				resourcesArr = resourcesArr.filter(function(n){ return n != undefined });

			if (itemsProcessed === roles.length) {
				console.log(resourcesArr);
				callback(resourcesArr);
			}
		});
	});
}


exports.getUserPermissions = function(req, res) {

	var status = 201;

	if (req.body.userId && req.body.resource) {
		acl.allowedPermissions(req.params.userId && req.params.resource).then( (res) => {
			status = 200;
			sendResponse.sendHttpStatus(res, status);
		}).catch((err) => {
			status = 500;
			sendResponse.sendHttpStatus(res, status);
		});
	} else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}


exports.addNewUser = function(req, res) {
	var Users = schemas.users;
	var status = 201;

	if (req.body.username && req.body.password && ( (req.body.type == 'internal') || req.body.type == 'external' && req.body.name) && req.body.dsp && req.body.ssp && req.body.xml) {
		findUserByUsername(req.body.username).then((result) => {
			status = 400;
			sendResponse.sendJson(res, status, { "message": "username alredy exists!" });
		}).catch((err) => {
			userAuth.hashPassword(req.body.password).then((result) => {
				var query = {key: req.body.username, password: result, type: req.body.type, dsp: req.body.dsp, ssp: req.body.ssp, xml: req.body.xml, status: 'active'};
				if (req.body.type == 'external') {
					query = Object.assign(query, { name: req.body.name });
				}

				var newUser = new Users(query);
				newUser.save(function (err, result) {
					console.log(err);
					if (err) status = 400;
					sendResponse.sendHttpStatus(res, status);
				});
			}).catch((err) => {
				console.log(err);
			});
		});
	 } else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}


exports.addRole = function(req, res) {

	var status = 201;

	if (req.body.role && req.body.resources && req.body.permissions) {
		acl.allow(req.body.role, req.body.resources, req.body.permissions, function(err) {

			if (!err) {
				status = 200;
			} else {
				status = 500;
			}
			sendResponse.sendHttpStatus(res, status);
		});
	} else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}


exports.removeRole = function(req, res) {

	var status = 201;

	if (req.body.role) {
		acl.removeRole(req.body.role, function(err) {
			if (!err) {
				status = 200;
			} else {
				status = 500;
			}
			sendResponse.sendHttpStatus(res, status);
		});
	} else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}


exports.removeRolePermissions = function(req, res) {

	var status = 201;

	if (req.body.role && req.body.resources && req.body.permissions) {
		acl.removeAllow(req.body.role, req.body.resources, req.body.permissions).then( (res) => {
			status = 200;
			sendResponse.sendHttpStatus(res, status);
		}).catch((err) => {
			status = 500;
			sendResponse.sendHttpStatus(res, status);

		});
	} else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}


exports.addUserRole = function(req, res) {

	var status = 201;

	if (req.body.userId && req.body.role) {
		acl.addUserRoles(req.body.userId, req.body.role, function(err) {

			if (!err) {
				status = 200;
			} else {
				status = 500;
			}
			sendResponse.sendHttpStatus(res, status);
		});
	} else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}


exports.removeUserRole = function(req, res) {

	var status = 201;

	if (req.body.userId && req.body.role) {
		acl.removeUserRoles(req.body.userId, req.body.role, function(err) {

			if (!err) {
				status = 200;
			} else {
				status = 500;
			}
			sendResponse.sendHttpStatus(res, status);
		});
	} else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}


exports.addRoleParents = function(req, res) {

	var status = 201;

	if (req.body.role && req.body.parents) {
		acl.addRoleParents(req.body.role, req.body.parents).then( (res) => {
			status = 200;
			sendResponse.sendHttpStatus(res, status);
		}).catch((err) => {
			status = 500;
			sendResponse.sendHttpStatus(res, status);
		});
	} else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}


exports.removeRoleParents = function(req, res) {

	var status = 201;

	if (req.body.role && req.body.parents) {
		acl.removeRoleParents(req.body.role, req.body.parents).then( (res) => {
			status = 200;
			sendResponse.sendHttpStatus(res, status);
		}).catch((err) => {
			status = 500;
			sendResponse.sendHttpStatus(res, status);
		});
	} else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}


exports.removeResource = function(req, res) {

	var status = 201;

	if (req.body.resource) {
		acl.removeResource(req.body.resource, function(err) {
			if (!err) {
				status = 200;
			} else {
				status = 500;
			}
			sendResponse.sendHttpStatus(res, status);
		});

	} else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}
