module.exports = function(from, to, callback) {
	var oneDay = 24*60*60*1000;	// hours*minutes*seconds*milliseconds
	var firstDate = new Date(from);
	var secondDate = new Date(to);
	var diffDays = Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay));

	callback(diffDays);
}


