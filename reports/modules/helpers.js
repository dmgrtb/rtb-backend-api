var counter = 0;
var awaitingProcess = 1;
var response;
var table = {};


module.exports.convertTimezone = function(timezone) {
	var timezone = timezone;
	if (timezone = 'utc') {
		timezone = '+00:00';
	} else if (timezone = 'etc') {
		timezone = '-04:00';
	}

	return timezone;
}

module.exports.optimizeIndex = function(type, from, to) {
  var index;
  from = new Date(from);
  var fromDay   = ("0" + from.getDate()).slice(-2);
  var fromMonth = ("0" + (from.getMonth() + 1)).slice(-2);
  var fromYear  = from.getFullYear();

  to = new Date(to);
  var toDay   = ("0" + to.getDate()).slice(-2);
  var toMonth = ("0" + (to.getMonth() + 1)).slice(-2);
  var toYear  = to.getFullYear();

  if (fromYear === toYear && fromMonth === toMonth && fromDay === toDay) {
        index = type + '-' + toYear + '.' + toMonth + '.' + toDay;
  } else if (fromYear === toYear && fromMonth === toMonth) {
        index = type + '-' + toYear + '.' + toMonth + '.' + '*';
  } else if (fromYear === toYear) {
        index = type + '-' + toYear + '.' + '*';
  } else {
        index = type + '-*';
  }

  return index;
}

module.exports.initResponse = function(res) {
	response = res;
}

module.exports.matricsCalc = function matricsCalc(parsedResponse, callback) {

    for (var i=queryArr.length; i--;) {
        if (queryArr[i].type === "matric") {

            if (queryArr[i].apiName === 'efficiency' || queryArr[i].apiName === 'winper' || queryArr[i].apiName === 'resper' ||
                queryArr[i].apiName === 'ctr' || queryArr[i].apiName === 'cvr' || queryArr[i].apiName === 'sresponseper' ||
                queryArr[i].apiName === 'swinsper' || queryArr[i].apiName ==='sfillrate' || queryArr[i].apiName === 'sefficiency') {

                for (var j=parsedResponse.length; j--;) {
            				if (parsedResponse[j][queryArr[i].apiName + 2] !== 0) {
            					parsedResponse[j][queryArr[i].apiName] = parsedResponse[j][queryArr[i].apiName + 1] / parsedResponse[j][queryArr[i].apiName + 2] * 100;
            				} else {
            					parsedResponse[j][queryArr[i].apiName] = 0;
            				}

            				delete parsedResponse[j][queryArr[i].apiName + 1];
            				delete parsedResponse[j][queryArr[i].apiName + 2];
			          }

            } else if (queryArr[i].apiName === 'revenue') {
                for (var j=parsedResponse.length; j--;) {
                    parsedResponse[j][queryArr[i].apiName] = typeof parsedResponse[j][queryArr[i].apiName] === 'undefined' || parsedResponse[j][queryArr[i].apiName] === null ? 0 : parsedResponse[j][queryArr[i].apiName];
                }

            } else if (queryArr[i].apiName === 'profit') {
                for (var j=parsedResponse.length; j--;) {
                    parsedResponse[j][queryArr[i].apiName + 1] = typeof parsedResponse[j][queryArr[i].apiName + 1] === 'undefined' || parsedResponse[j][queryArr[i].apiName + 1] === null ? 0 : parsedResponse[j][queryArr[i].apiName +1];
                    parsedResponse[j][queryArr[i].apiName] = parsedResponse[j][queryArr[i].apiName + 1] - parsedResponse[j][queryArr[i].apiName + 2];
                    delete parsedResponse[j][queryArr[i].apiName + 1];
                    delete parsedResponse[j][queryArr[i].apiName + 2];
                }

            } else if (queryArr[i].apiName === 'srpm' || queryArr[i].apiName === 'scpm') {
                for (var j=parsedResponse.length; j--;) {
                    if (parsedResponse[j][queryArr[i].apiName + 2] !== 0) {
                      parsedResponse[j][queryArr[i].apiName] = parsedResponse[j][queryArr[i].apiName + 1] / parsedResponse[j][queryArr[i].apiName + 2] * 1000;
                    } else {
                      parsedResponse[j][queryArr[i].apiName] = 0;
                    }
                    delete parsedResponse[j][queryArr[i].apiName + 1];
                    delete parsedResponse[j][queryArr[i].apiName + 2];
              }
            }



        }
    }
    callback(parsedResponse);
}

module.exports.increaseAwaitingProcess = function() {
	awaitingProcess++;
}

module.exports.getAwaitingProcess = function() {
	return awaitingProcess;
}


module.exports.prepareResponse = function prepareResponse(parsedResponse, type) {
		counter++;
		console.log(counter + '|' + awaitingProcess);
		if (type === 'rows') {
			table.rows = parsedResponse;

		} else if (type === 'totals') {
			table.totals = parsedResponse;

		} else if (type === 'charts') {
			table.charts = parsedResponse;
		}

		if (counter === awaitingProcess) {
                    
			counter = 0;
			awaitingProcess = 1;
			sendResponse.sendJson(response, 200, table);
			table = {};
		}
}
