var helper = require('./helpers');

exports.createQueryBody = function(index, type) {
	 var queryBody = {};
	 queryBody.index = index;
	 queryBody.type = type;
	 queryBody.body = {};
	 queryBody.body.size = 0;
	 var query = {query:{}};
	 var newBody = Object.assign(queryBody.body, query);
	 queryBody.body = newBody;

    return queryBody;
};


exports.insertTimestamp = function(from, to) {
	 var timestamp = { 
	 					bool: {
					      must: [{
					        range: {
					          "@timestamp": {
					            gte: from,
					            lte:  to
					          }
					        }
					      }]
					    }
				    }

    return timestamp;
};


exports.insertDateHistogram = function(interval, format, timezone) {
	 var convetedTimezone = helper.convertTimezone(timezone);
	 var dateHistogram = { aggs : {
							    date : {
							        date_histogram : {
							           field : "@timestamp",
							           interval : interval,
							           format : format,
							           time_zone: convetedTimezone
							        }
							     }
							   }
						    }
    return dateHistogram;
}


exports.insertMustFilters = function(dimensionsArr) {
	var mustArr = [];

	for (var i=0, length=dimensionsArr.length; i<length; i++) {
		if (dimensionsArr[i].value !== undefined && dimensionsArr[i].value !== '') {

			var termObj  = {};
			var fieldObj = {};

			termObj.terms = {};
			fieldObj[dimensionsArr[i].name] = [];
                        
                        //TEMPORARY FIX SINCE COUNTRY WAS SAVED AS LOWER CASE UNTIL MAY 8TH, 2017 AND UPPERCASE LATER
                        if (dimensionsArr[i].apiName === 'country'){
                            if (Array.isArray(dimensionsArr[i].value)) {
                                fieldObj[dimensionsArr[i].name] = [];
                                for (var j = 0; j < dimensionsArr[i].value.length; j++){
                                    fieldObj[dimensionsArr[i].name].push(dimensionsArr[i].value[j].toLowerCase());
                                    fieldObj[dimensionsArr[i].name].push(dimensionsArr[i].value[j].toUpperCase());
                                }				
                            } else {
                                fieldObj[dimensionsArr[i].name] = [dimensionsArr[i].value.toLowerCase(), dimensionsArr[i].value.toUpperCase()];
                            }
                        //TEMPORARY FIX END
                        } else if (Array.isArray(dimensionsArr[i].value)) {
				fieldObj[dimensionsArr[i].name] = dimensionsArr[i].value;
			} else {
				fieldObj[dimensionsArr[i].name].push(dimensionsArr[i].value);
			}

			termObj.terms = fieldObj;
			mustArr.push(termObj);
		}
	}

	return mustArr;
}
