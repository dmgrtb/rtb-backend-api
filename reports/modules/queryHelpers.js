var calcs = require('../calcs');
var helper = require('./helpers');
var queryBuilder = require('./queryBuilder');


module.exports.buildInitialQuery = function buildInitialQuery(req, dimensionsArr=null, interval) {

		var index = helper.optimizeIndex(req.query.type, req.query.from, req.query.to);

		//create query body.
		var query = queryBuilder.createQueryBody(index, req.query.type);

		//insert @timestamp.
		if (req.query.from !== undefined && req.query.to !== undefined) {
			query.body.query = queryBuilder.insertTimestamp(req.query.from, req.query.to);
		}

		//insert date histogram.
		if (interval !== 'none' && req.query.timezone !== undefined) {
			if (interval == 'hour') {
				var format = "yyyy/MM/dd ha";
			} else {
				var format = "yyyy/MM/dd";
			}
			var dateHistogram = queryBuilder.insertDateHistogram(interval, format, req.query.timezone);
			var newBody = Object.assign(query.body, dateHistogram);
			query.body = newBody;
		}

		//add dimensions filters.
		if (dimensionsArr !== null) {
			var mustFilters = queryBuilder.insertMustFilters(dimensionsArr);

			for (var i=0, length=mustFilters.length; i<length; i++) {
				query.body.query.bool.must.push(mustFilters[i]);
			}
		}

		//add filter by user acl permissions.
		//if (req.session.ssp === true && req.session.type == 'external' && req.query.type == 'ssp') {
		if (req.session.type == 'external'){
			var typeFilter = [{name: "publisher", value: req.session.name}];
			var mustFilters = queryBuilder.insertMustFilters(typeFilter);
			query.body.query.bool.must.push(mustFilters[0]);
		}

		return query;
}


module.exports.buildQuery = function buildQuery(queryArr, callback) {
		var queryArr = queryArr;
		var chainObj = {};
		var flag = false;

		for (var i=length=queryArr.length; i--;) {
			var obj = {aggs:{}};
			var tempAggObj = {};

			if (queryArr[i].type === "dimension") {
				tempAggObj[queryArr[i].apiName] = {terms:{}};
			 	tempAggObj[queryArr[i].apiName].terms = {field: queryArr[i].name, collect_mode: "breadth_first", size: 0};

			 	obj.aggs = tempAggObj;

		 		if (chainObj.aggs !== undefined) {
		 			obj.aggs[queryArr[i].apiName].aggs = chainObj.aggs;
		 		}

		 		chainObj = obj;

			 } else if (queryArr[i].type === "matric" || queryArr[i].type === "totals") {

			 	//if (queryArr[i].apiName == 'sefficiency' || queryArr[i].apiName ==)

			 	funName = 'get' + request.query.type + queryArr[i].apiName;

			 	tempAggObj = calcs[funName](queryArr[i]);

			 	if (flag === false) {
			 		obj.aggs = tempAggObj;
			 		chainObj = obj;
			 		firstMatricName = queryArr[i].apiName;
			 		flag = true;

			 	} else {
			 		Object.assign(chainObj.aggs, tempAggObj);
			 	}
			 }
		 }
	     callback(chainObj);
}


module.exports.executeQuery = function(query, callback) {
		esClient.search(query, function (err, res) {

			  if (err) {
			    //handle error
			    throw err;
			  }

			  callback(res);
		  	});
}
