var queryHelper = require('./modules/queryHelpers');
var convTable = require('./convTable');
var matricArr = { cost:0 };
var calcDaysBetweenDates = require('./modules/daysBetweenDatesCalc');
var esResponseParser = require("es-response-parser");
var request;
var queryArr = [];
var interval;
var helper = require('./modules/helpers');
var queryHelper = require('./modules/queryHelpers');

var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];


module.exports = function(req) {

	request = req;

	calcDaysBetweenDates(req.query.from, req.query.to, function(days) {

		if (days <= 30) {
			interval = 'day';
		} else if (days >= 30 && days <= 90) {
			interval = 'week';
		} else {
			interval = 'month';
		}

		buildQuery(interval);
	});
}


function buildQuery(interval) {

	for (var key in matricArr) {
		if (typeof convTable[key] !== 'undefined') {
			queryArr.push(convTable[key]);
		}
	}

	queryHelper.buildQuery(queryArr, function(aggsParams) {

		var query = queryHelper.buildInitialQuery(request, null, interval);

		var searchBody = Object.assign(query.body.aggs.date, aggsParams);

		query.body.aggs.date = searchBody;

		queryHelper.executeQuery(query, function(result) {

			var parsedResponse = esResponseParser.parse(result);

			for (var i=length=queryArr.length; i--;) {
						if (interval === 'month') {
							var d = new Date(parsedResponse[j].date);
							parsedResponse[j].monthName = monthNames[d.getMonth()];
						}
			}

			helper.prepareResponse(parsedResponse, 'charts');
		});
	});
}
