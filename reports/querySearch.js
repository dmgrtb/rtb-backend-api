var util = require('util');
var dimensionArr = new Array();
var matricArr = new Array();
var response;
var convTable = require('./convTable');
var calcs = require('./calcs');
var table = {};
var charts = require('./charts');
var helper = require('./modules/helpers');
var queryHelper = require('./modules/queryHelpers');
var queryBuild = require('./queryBuild');

/**
 * @apiDescription This is the Description.
 * It is multiline capable.
 *
 * Last line of Description.
 *
 * @api {Get} /reports DSP report
 * @apiName dspReport
 * @apiGroup Reporting
 *
 * @apiPermission none
 *
 * @apiExample {curl} Example usage:
 *     curl -H <> -i http://api.rtb2web.com/reports?searchtype=search&type=dsp&timezone=utc&from=2017-08-03T00:00:00&to=2017-08-03T23:59:59&interval=none&publisher=&supplysource=&request=&imp=&clicks=&ctr=&conversion=&cvr=&bidres=&resper=&win=&winper=&efficiency=&cost=&revenue=&profit=&avgwin=&avgbid=&totals=yes&charts=no
 *
 * @apiHeader {Cookie} authorization Authorization value.
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Accept-Encoding": "Accept-Encoding: gzip, deflate"
 *     }
 *
 * @apiParam {String="search", "autocomplete"} searchtype Report Type
 * @apiParam {DateTime="YYYY-MM-DDTHH:MM:SS"} from From date
 * @apiParam {DateTime="YYYY-MM-DDTHH:MM:SS"} to To date
 * @apiParam {String="none", "hour", "day", "month"} interval Report interval
 * @apiParam {String="utc"} timezone Report timezone
 * @apiParam {String="dsp"} type DSP Report
 * @apiParam {String="Yes", "No"} totals Return total values
 * @apiParam {String="yes", "No"} charts Return charts values
 * @apiParam (Metrics) [request] Return number of requests
 * @apiParam (Metrics) [bidres] Return number of bid response
 * @apiParam (Metrics) [resper] Return % of response
 * @apiParam (Metrics) [win] Return number of wins
 * @apiParam (Metrics) [winper] Return % of wins
 * @apiParam (Metrics) [imp] Return number of impressions
 * @apiParam (Metrics) [efficiency] Return efficiency value
 * @apiParam (Metrics) [clicks] Return number of clicks
 * @apiParam (Metrics) [conversion] Return number of converiosns
 * @apiParam (Metrics) [avgbid] Return avarage bid
 * @apiParam (Metrics) [avgwin] Return avarage win price
 * @apiParam (Metrics) [cost] Return total cost
 * @apiParam (Metrics) [revenue] Return total revenue
 * @apiParam (Metrics) [profit] Return total profit
 * @apiParam (Metrics) [ctr] Return % CTR
 * @apiParam (Metrics) [cvr] Return % CVR
 * @apiParam (Dimensions) [publisher] Group by publisher name
 * @apiParam (Dimensions) [supplysource] Group by supply source name
 * @apiParam (Dimensions) [domain] Group by domain name
 * @apiParam (Dimensions) [app] Group by app name
 * @apiParam (Dimensions) [appid] Group by app ID
 * @apiParam (Dimensions) [device] Group by device
 * @apiParam (Dimensions) [useragent] Group by user agent
 * @apiParam (Dimensions) [connectiontype] Group by connection type
 * @apiParam (Dimensions) [carrier] Group by carrier name
 * @apiParam (Dimensions) [os] Group by OS
 * @apiParam (Dimensions) [ip] Group by IP
 * @apiParam (Dimensions) [iabcat] Group by IAB category
 * @apiParam (Dimensions) [adtype] Group by AD type
 * @apiParam (Dimensions) [department] Group by department name
 * @apiParam (Dimensions) [algorithm] Group by algorithm
 * @apiParam (Dimensions) [advertiser] Group by advertiser name
 * @apiParam (Dimensions) [deal] Group by deal name
 * @apiParam (Dimensions) [campaign] Group by campaign name
 * @apiParam (Dimensions) [creative] Group by creative name
 * @apiParam (Dimensions) [creativeid] Group by creative ID
 * @apiParam (Dimensions) [country] Group by country
 * @apiParam (Dimensions) [city] Group by city
 *
 * @apiParamExample {String} Request template:
 *     ?searchtype=search&type=dsp&timezone=<time zone::string>&from=<from date::date(YYYY-MM-DDTHH:MM:SS)>&to=<to date::date(YYYY-MM-DD HH:MM:SS)>&interval=<interval::string>&publisher=&supplysource=&request=&imp=&clicks=&ctr=&conversion=&cvr=&bidres=&resper=&win=&winper=&efficiency=&cost=&revenue=&profit=&avgwin=&avgbid=&totals=yes&charts=<charts::string>
 *
 * @apiParamExample {String} Request example:
 *     ?searchtype=search&type=dsp&timezone=utc&from=2017-08-03T00:00:00&to=2017-08-03T23:59:59&interval=none&publisher=&supplysource=&request=&imp=&clicks=&ctr=&conversion=&cvr=&bidres=&resper=&win=&winper=&efficiency=&cost=&revenue=&profit=&avgwin=&avgbid=&totals=yes&charts=no
 *
 * @apiSuccess (Success) {Json} rows
 * @apiSuccess (Success) {String} [rows.publisher] Publisher name
 * @apiSuccess (Success) {Json} [totals]
 * @apiSuccess (Success) {Integer} [totals.totalboximps] Total impresions
 * @apiSuccess (Success) {Integer} [totals.totalboxclicks] Total clicks
 * @apiSuccess (Success) {Integer} [totals.totalboxconversions] Total conversions
 * @apiSuccess (Success) {Number} [totals.totalboxcost] Total cost
 * @apiSuccess (Success) {Number} [totals.totalboxrevenue] Total revenue
 * @apiSuccess (Success) {Json} [charts]
 * @apiSuccessExample {Json} Success Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "rows": {},
 *        "totals": {}
 *     }
 *
 * @apiError (Error) {String} Failed! The Username or Password incurrect.
 * @apiErrorExample {String} Error Response:
 *     HTTP/1.1 200 OK
 *     failed!
 *
 * @apiSampleRequest /reports
 *
 */

 /**
  * @apiDescription This is the Description.
  * It is multiline capable.
  *
  * Last line of Description.
  *
  * @api {Get} /reports SSP report
  * @apiName sspReport
  * @apiGroup Reporting
  *
  * @apiPermission none
  *
  * @apiExample {curl} Example usage:
  *     curl -H <> -i http://api.rtb2web.com/reports?searchtype=search&type=ssp&timezone=utc&from=2017-08-03T00:00:00&to=2017-08-03T23:59:59&interval=none&publisher=&supplysource=&request=&imp=&clicks=&ctr=&conversion=&cvr=&bidres=&resper=&win=&winper=&efficiency=&cost=&revenue=&profit=&avgwin=&avgbid=&totals=yes&charts=no
  *
  * @apiHeader {Cookie} authorization Authorization value.
  *
  * @apiHeaderExample {json} Header-Example:
  *     {
  *       "Accept-Encoding": "Accept-Encoding: gzip, deflate"
  *     }
  *
  * @apiParam {String="search", "autocomplete"} searchtype Report Type
  * @apiParam {DateTime="YYYY-MM-DDTHH:MM:SS"} from From date
  * @apiParam {DateTime="YYYY-MM-DDTHH:MM:SS"} to To date
  * @apiParam {String="none", "hour", "day", "month"} interval Report interval
  * @apiParam {String="utc"} timezone Report timezone
  * @apiParam {String="ssp"} type SSP Report
  * @apiParam {String="Yes", "No"} totals Return total values
  * @apiParam {String="yes", "No"} charts Return charts values
  * @apiParam (Metrics) [sopportunities] Return number of opportunities
  * @apiParam (Metrics) [sadresponse] Return number of ad response
  * @apiParam (Metrics) [sresponseper] Return % of response
  * @apiParam (Metrics) [simpressions] Return number of impressions
  * @apiParam (Metrics) [swins] Return number of wins
  * @apiParam (Metrics) [swinsper] Return % of wins
  * @apiParam (Metrics) [sefficiency] Return efficiency
  * @apiParam (Metrics) [savgbidfloor] Return avarage bidfloor
  * @apiParam (Metrics) [savgbid] Return avarage bid
  * @apiParam (Metrics) [savgwin] Return avarage win bid
  * @apiParam (Metrics) [scost] Return total cost
  * @apiParam (Metrics) [srevenue] Return total revenue
  * @apiParam (Metrics) [sprofit] Return total profit
  * @apiParam (Metrics) [scpm] Return CPM
  * @apiParam (Metrics) [srpm] Return RPM
  * @apiParam (Metrics) [smargin] Return % margin
  * @apiParam (Metrics) [sfillrate] Return % sfill rate
  * @apiParam (Metrics) [sforensiqblock] Return total forensiq block
  * @apiParam (Dimensions) [ssupplypartner] Group by publisher name
  * @apiParam (Dimensions) [ssupplysource] Group by supply source name
  * @apiParam (Dimensions) [sdemandpartner] Group by demand partner name
  * @apiParam (Dimensions) [sdemand deal] Group by sdemand deal name
  * @apiParam (Dimensions) [sdomain] Group by domain name
  * @apiParam (Dimensions) [scountry] Group by scountry
  * @apiParam (Dimensions) [sdevicetype] Group by device
  * @apiParam (Dimensions) [sos] Group by connection OS
  * @apiParam (Dimensions) [suseragent] Group by user agent
  * @apiParam (Dimensions) [sdemandtag] Group by demand tag
  * @apiParam (Dimensions) [sdemanddealtype] Group by demand deal type
  * @apiParam (Dimensions) [iabcat] Group by IAB category
  * @apiParam (Dimensions) [playersize] Group by player size
  *
  * @apiParamExample {String} Request template:
  *     ?searchtype=search&type=ssp&timezone=<time zone::string>&from=<from date::date(YYYY-MM-DDTHH:MM:SS)>&to=<to date::date(YYYY-MM-DD HH:MM:SS)>&interval=<interval::string>&publisher=&supplysource=&request=&imp=&clicks=&ctr=&conversion=&cvr=&bidres=&resper=&win=&winper=&efficiency=&cost=&revenue=&profit=&avgwin=&avgbid=&totals=yes&charts=<charts::string>
  *
  * @apiParamExample {String} Request example:
  *     ?searchtype=search&type=ssp&timezone=utc&from=2017-08-03T00:00:00&to=2017-08-03T23:59:59&interval=none&publisher=&supplysource=&request=&imp=&clicks=&ctr=&conversion=&cvr=&bidres=&resper=&win=&winper=&efficiency=&cost=&revenue=&profit=&avgwin=&avgbid=&totals=yes&charts=no
  *
  * @apiSuccess (Success) {Json} rows
  * @apiSuccess (Success) {String} [rows.publisher] Publisher name
  * @apiSuccess (Success) {Json} [totals]
  * @apiSuccess (Success) {Integer} [totals.totalboximps] Total impresions
  * @apiSuccess (Success) {Integer} [totals.totalboxclicks] Total clicks
  * @apiSuccess (Success) {Integer} [totals.totalboxconversions] Total conversions
  * @apiSuccess (Success) {Number} [totals.totalboxcost] Total cost
  * @apiSuccess (Success) {Number} [totals.totalboxrevenue] Total revenue
  * @apiSuccess (Success) {Json} [charts]
  * @apiSuccessExample {Json} Success Response:
  *     HTTP/1.1 200 OK
  *     {
  *        "rows": {},
  *        "totals": {}
  *     }
  *
  * @apiError (Error) {String} Failed! The Username or Password incurrect.
  * @apiErrorExample {String} Error Response:
  *     HTTP/1.1 200 OK
  *     failed!
  *
  * @apiSampleRequest /reports
  *
  */
module.exports.parse = function queryStringParse(req, res) {

	//do not run if another request still processing.
	if (helper.getAwaitingProcess() < 2) {

		queryArr = [];
		matricArr = [];
		response = res;
		helper.initResponse(res);
		request = req;

		//build dimension and matric array base `convTabel`.
		for (var key in req.query) {
			if (typeof convTable[key] !== 'undefined') {
				if (convTable[key].type === 'dimension' || convTable[key].type === 'matric') {
					convTable[key].value = req.query[key];
					queryArr.push(convTable[key]);
				} else if (convTable[key].type === 'totals') {
					convTable[key].value = req.query[key];
					matricArr.push(convTable[key]);
				}
			}
		}

		queryBuild.queryAll(queryArr, req, res);
	}
}


function getTableRows(dimensionObj) {

	var searchParams = queryHelper.buildInitialQuery(request, queryArr, request.query.interval);
	if (request.query.interval !== 'none') {
		var newBodySearch = Object.assign(searchParams.body.aggs.date, dimensionObj);
		searchParams.body.aggs.date = newBodySearch;
	} else {
		var newBodySearch = Object.assign(searchParams.body, dimensionObj);
		searchParams.body = newBodySearch;
	}

	console.log(JSON.stringify(searchParams));
	console.log('+++++++++++++++++++++++++++++++++++++++++++++');

 	esClient.search(searchParams, function (err, res) {

	  if (err) {
	    //handle error
	    throw err;
	  }

		helper.matricsCalc(esResponseParser.parse(res), function(parsedMatricResponse) {
			helper.prepareResponse(parsedMatricResponse, 'rows');
		});
	});
 }


function matricsTotalQuery(chainTotalArr) {

   var searchParams = queryHelper.buildInitialQuery(request, null, 'none');
   var newBodySearch = Object.assign(searchParams.body, chainTotalArr);
   searchParams.body = newBodySearch;

   console.log(JSON.stringify(searchParams));

   esClient.search(searchParams, function (err, res) {

	  if (err) {
	    //handle error
	    throw err;
	  }

	  if(request.query.type == 'ssp') { // ***irrelevant***. if it is again, should add "xml". (sagit 11.3.18)

	    	var parsedResponse = [{"totalBoxSspOpp": res.aggregations.totalBoxSspOpp.value, "totalBoxSspImps": res.aggregations.totalBoxSspImps.value,
	  						  "totalBoxSspRev":  res.aggregations.totalBoxSspRev.value, "totalBoxSspCost": res.aggregations.totalBoxSspCost.value,
	  						  "totoalforensiqblock": res.aggregations.totoalforensiqblock.value
	  						 }];
	  	helper.prepareResponse(parsedResponse, 'totals');
  	  } else {
	  	helper.prepareResponse(esResponseParser.parse(res), 'totals');
  	  }
	});
}
