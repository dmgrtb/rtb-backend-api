var esResponseParser = require("es-response-parser");
var convTable = require('./convTable');
var helper = require('./modules/helpers');
var queryBuilder = require('./modules/queryBuilder');

module.exports = function(req, res) {

	if (convTable[req.query.dimension] !== undefined) {
		queryAutoComplete(req, res);
	} else {
		helper.sendResponse(JSON.stringify([]), res);
	}
}

function queryAutoComplete(req, res) {
	var searchQuery = queryBuilder.createQueryBody(req.query.type + '-*', req.query.type);
	var searchInput = ".*" + decodeURIComponent(req.query.value) + ".*";
	var dimension = convTable[req.query.dimension].name;

	var aggs = { aggs: { dimension: { terms: { field: {}, size: {}, include:{} } } } };
	aggs.aggs.dimension.terms.field = dimension;
	aggs.aggs.dimension.terms.size = config.autoCompleteMaxResults;   		
        
        var additionalDimensions = {set: false, baseDim: req.query.dimension, preSetPub: false};
        if ((req.query.dimension === 'app' || req.query.dimension === 'domain') && 
                typeof req.query.adddimensions !== 'undefined' &&
                req.query.adddimensions === 'true'){

            additionalDimensions.set = true;   
            if (req.query.dimension === 'app'){
                additionalDimensions.fields = [
                    'request.app.name',
                    'request.app.id'
                ];
                additionalDimensions.idField = 'request.app.id';
                additionalDimensions.bucketName = 'appId';
            } else if (req.query.dimension === 'domain' && req.query.type == 'dsp'){
                additionalDimensions.fields = [        
                    'request.site.name',
                    'request.site.domain',
                    'request.site.page',
                    'request.site.id'
                ];
                additionalDimensions.idField = 'request.site.id';
                additionalDimensions.bucketName = 'siteId';

            } 
            
            else if (req.query.dimension === 'domain' && (req.query.type == 'ssp' || req.query.type == 'xml')) {
                additionalDimensions.fields = [        
                    'auction.request.site.domain'
                ];
            }
            
            searchQuery.body.size = config.autoCompleteMaxResults;
            searchQuery.body.query = {
                bool: {
                    should: [],
                    minimum_should_match: 1                                                
                }
            };
            
            for (var i = 0; i < additionalDimensions.fields.length; i++){
                obj = {regexp: {}};
                obj.regexp[additionalDimensions.fields[i]] = searchInput;
                searchQuery.body.query.bool.should.push(obj);
            }            

            //HARDCODED date limit just for the domain field starting when the field (request.site.page) wasn't analyzed
            if (req.query.dimension === 'domain'){
                searchQuery.body.query.bool.must = {
                                                    range: {
                                                      "@timestamp": {
                                                        gte: "2017-02-08T00:00:00"
                                                      }
                                                    }
                                                  };
            }
           

            aggs.aggs.dimension.aggs = {};    
            aggs.aggs.dimension.aggs[additionalDimensions.bucketName] = {
                                                                            terms: {
                                                                                field: additionalDimensions.idField,
                                                                                size: config.autoCompleteMaxResults
                                                                            }
                                                                        };
            
            if (typeof req.query.pubid !== 'undefined'){
                additionalDimensions.preSetPub = req.query.pubid;
                searchQuery.body.query.bool.must = {
                    term: {
                        "request.sspId": req.query.pubid
                    }
                };
            } else {         
                aggs.aggs.dimension.aggs[additionalDimensions.bucketName].aggs = {
                    publisher: {
                        terms: {
                            field: "request.sspId",
                            size: config.autoCompleteMaxResults                                
                        }
                    }                    
                };   
            }
        } else if (req.query.dimension === 'carrier') {
            if (typeof req.query.countryList !== 'undefined' && typeof req.query.countries !== 'undefined' && Array.isArray(req.query.countries)){
                searchQuery.body.size = config.autoCompleteMaxResults;
                switch (req.query.countryList){
                    case 'white':
                        searchQuery.body.query = {
                            constant_score: {
                                filter: {
                                    bool: {
                                        should: [],
                                        minimum_should_match: 1,                                                  
                                    }
                                }
                            }
                        };              
                        for (var i = 0; i < req.query.countries.length; i++){
                            var country = req.query.countries[i];/*.toLowerCase();                        */
                            var queryObj = {term: {"request.device.geo.country": country}};
                            searchQuery.body.query.constant_score.filter.bool.should.push(queryObj);
                        }
                        break;
                    case 'black':
                        searchQuery.body.query = {
                            constant_score: {
                                filter: {                            
                                    bool: {
                                        must_not: []                                                
                                    }
                                }
                            }
                        };    
                        for (var i = 0; i < req.query.countries.length; i++){
                            var country = req.query.countries[i].toLowerCase();
                        
                            var queryObj = {term: {"request.device.geo.country": country}};
                            searchQuery.body.query.constant_score.filter.bool.must_not.push(queryObj);
                        }
                        break;                    
                }

            
            } else {
                searchQuery.body.query = {                            
                            constant_score: {
                                filter: {
                                    bool: {}
                                    }
                                }
                        };
            }
            searchQuery.body.query.constant_score.filter.bool.must = { regexp: {} };
            searchQuery.body.query.constant_score.filter.bool.must.regexp[dimension] = searchInput;
            searchQuery.body.size = config.autoCompleteMaxResults;
            aggs.aggs.dimension.terms.include = searchInput;
        } else if (req.query.dimension === 'device') {
            var searchInput = decodeURIComponent(req.query.value);
            var caseSearch = '.*';
            for (var i = 0; i < searchInput.length; i++){
                if (searchInput.charAt(i).toLowerCase().match(/[a-z]/i)){
                    caseSearch += '[' + searchInput.charAt(i).toUpperCase() + searchInput.charAt(i).toLowerCase() + ']';
                } else {
                    caseSearch += searchInput.charAt(i);
                }                
            }
            caseSearch += '.*';
            additionalDimensions.set = true; 
            searchQuery.body.size = config.autoCompleteMaxResults;
            additionalDimensions.fields = [        
                'request.device.make',
                'request.device.model'
            ];
            additionalDimensions.idField = 'request.device.model';
            additionalDimensions.bucketName = 'deviceModel';

            searchQuery.body.query = {
                bool: {
                    should: [],
                    minimum_should_match: 1,                                                  
                }
            };
            
            for (var i = 0; i < additionalDimensions.fields.length; i++){
                obj = {regexp: {}};
                obj.regexp[additionalDimensions.fields[i]] = caseSearch;
                searchQuery.body.query.bool.should.push(obj);
            }          
            aggs.aggs.dimension.aggs = {};  
            aggs.aggs.dimension.aggs[additionalDimensions.bucketName] = {
                                                                            terms: {
                                                                                field: additionalDimensions.idField,
                                                                                size: config.autoCompleteMaxResults
                                                                            }
                                                                        };      
                                                                        
            var dt = new Date();
            //HARDCODED check if we're using the query at least a month since the device fields were set as "not analyzed"
            if (dt.getFullYear() === 2017 && (dt.getMonth() < 3 || (dt.getMonth() === 3 && dt.getDate() <= 7))){
                var yyyy = '2017';
                var mm = '03';
                var dd = '07';
            } else {
                var yyyy = dt.getFullYear().toString();
                var mm = dt.getMonth().toString();
                if (mm.length === 1) mm = '0' + mm;
                var dd = dt.getDate().toString();
                if (dd.length === 1) dd = '0' + dd;                
            }
            searchQuery.body.query.bool.must = {
                                                range: {
                                                  "@timestamp": {
                                                    gte: yyyy + "-" + mm + "-" + dd + "T00:00:00"
                                                  }
                                                }
                                              };            
        
        } else {
            if(req.query.dimension === 'ssupplysource' && req.session.type == 'external'){
                searchQuery.body.query = {
                    bool: {
                        must: [
                            {
                                regexp: {supplySource : searchInput}
                            },
                            {term: {
                                    publisher: req.session.name
                            }}
                        ]
                    }
                };
                searchQuery.body.size = config.autoCompleteMaxResults;
                aggs.aggs.dimension.terms.include = searchInput;
            }
            else {searchQuery.body.query = { regexp: {} };
            searchQuery.body.query.regexp[dimension] = searchInput;
            searchQuery.body.size = config.autoCompleteMaxResults;
            aggs.aggs.dimension.terms.include = searchInput;
            }
        }

	var body = Object.assign(searchQuery.body, aggs);
	searchQuery.body = body;        

	console.log('query:', JSON.stringify(searchQuery));

	esClient.search(searchQuery, function (err, result) {

	  if (err) {
	    console.log('err', err);
	    throw err;
	  }

	  parseAutoCompleteResult(result, additionalDimensions, res);

    });
}


function parseAutoCompleteResult(res, additionalDimensions, response) {
 	queryArr = res.aggregations.dimension.buckets;

    if (additionalDimensions.set){
            if (additionalDimensions.baseDim === 'device'){                
                var resultsArr = [];
                for (var makeIndex = 0; makeIndex < queryArr.length; makeIndex++){
                    for (var modelIndex = 0; modelIndex < queryArr[makeIndex][additionalDimensions.bucketName].buckets.length; modelIndex++){
                        resultsArr.push({'make': queryArr[makeIndex].key, 'model': queryArr[makeIndex][additionalDimensions.bucketName].buckets[modelIndex].key});
                    }            
                }                                  
            } else if (additionalDimensions.baseDim === 'app' || additionalDimensions.baseDim === 'domain'){
                var resultsArr = [];
                for (var nameIndex = 0; nameIndex < queryArr.length; nameIndex++){
                    for (var idIndex = 0; idIndex < queryArr[nameIndex][additionalDimensions.bucketName].buckets.length; idIndex++){
                        if (additionalDimensions.preSetPub !== false){
                            var dimensionString = queryArr[nameIndex].key + '___' + 
                                            queryArr[nameIndex][additionalDimensions.bucketName].buckets[idIndex].key + '___' +
                                            additionalDimensions.preSetPub;
                            resultsArr.push(dimensionString);
                        } else {
                            for (var pubIndex = 0; pubIndex < queryArr[nameIndex][additionalDimensions.bucketName].buckets[idIndex].publisher.buckets.length; pubIndex++){
                                var dimensionString = queryArr[nameIndex].key + '___' + 
                                                queryArr[nameIndex][additionalDimensions.bucketName].buckets[idIndex].key + '___' +
                                                queryArr[nameIndex][additionalDimensions.bucketName].buckets[idIndex].publisher.buckets[pubIndex].key;
                                resultsArr.push(dimensionString);
                            }
                        }
                    }            
                }  
            }
        } else {
            var resultsArr = [];
            for (var i=length=queryArr.length; i--;) {
                resultsArr.push(queryArr[i].key);
            }            
	sendResponse.sendJson(response, 200, resultsArr);
}}
