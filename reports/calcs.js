var util = require('util');
var obj = {};

/* DSP */

exports.getdspavgwin = function(field) {

  	var obj = {};
	obj[field.apiName] = {avg:{}};
 	obj[field.apiName].avg = {field: field.name};

  	return obj;
}


exports.getdspavgwin = function(field) {

  	var obj = {};
	obj[field.apiName] = {avg:{}};
 	obj[field.apiName].avg = {field: field.name};

  	return obj;
}

exports.getdspwin = function(field) {

  	var obj = {};
	obj[field.apiName] = {value_count:{}};
 	obj[field.apiName].value_count = {field: field.name};

  	return obj;
}

exports.getdspavgbid = function(field) {

  	var obj = {};
	obj[field.apiName] = {avg:{}};
 	obj[field.apiName].avg = {field: field.name};

  	return obj;
}

exports.getdsprequest = function(field) {

  	var obj = {};
	obj[field.apiName] = {value_count:{}};
 	obj[field.apiName].value_count = {field: field.name};

  	return obj;
}

exports.getdspimp = function(field) {

  	var obj = {};
	obj[field.apiName] = {value_count:{}};
 	obj[field.apiName].value_count = {field: field.name};

  	return obj;
}

exports.getdspbidres = function(field) {

  	var obj = {};
	obj[field.apiName] = {value_count:{}};
 	obj[field.apiName].value_count = {field: field.name};

  	return obj;
}

exports.getdspconversion = function(field) {

  var obj = {};
  obj[field.apiName] = {value_count:{}};
  obj[field.apiName].value_count = {field: field.name};

    return obj;
}

exports.getdspresper = function(field) {

  	var obj = {};
	obj[field.apiName + '1'] = {value_count:{}};
 	obj[field.apiName + '1'].value_count = {field: field.name1};

 	obj[field.apiName + '2'] = {value_count:{}};
 	obj[field.apiName + '2'].value_count = {field: field.name2};

  	return obj;
}

exports.getdspwinper = function(field) {

 	var obj = {};
	obj[field.apiName + '1'] = {value_count:{}};
 	obj[field.apiName + '1'].value_count = {field: field.name1};

 	obj[field.apiName + '2'] = {value_count:{}};
 	obj[field.apiName + '2'].value_count = {field: field.name2};


  	return obj;
}

exports.getdspefficiency = function(field) {

    var obj = {};
    obj[field.apiName + '1'] = {value_count:{}};
    obj[field.apiName + '1'].value_count = {field: field.name1};

    obj[field.apiName + '2'] = {value_count:{}};
    obj[field.apiName + '2'].value_count = {field: field.name2};

    return obj;
}

exports.getdspctr = function(field) {

    var obj = {};
    obj[field.apiName + '1'] = {value_count:{}};
    obj[field.apiName + '1'].value_count = {field: field.name1};

    obj[field.apiName + '2'] = {value_count:{}};
    obj[field.apiName + '2'].value_count = {field: field.name2};

    return obj;
}

exports.getdspcvr = function(field) {

  	var obj = {};
  	obj[field.apiName + '1'] = {value_count:{}};
   	obj[field.apiName + '1'].value_count = {field: field.name1};

   	obj[field.apiName + '2'] = {value_count:{}};
   	obj[field.apiName + '2'].value_count = {field: field.name2};

  	return obj;
}

exports.getdspcost = function(field) {

	var obj = {};

 	obj[field.apiName] = {sum:{}};
 	obj[field.apiName].sum = {field: field.name};

	return obj;
}

exports.getdsptotals = function(fieldName) {

  var obj = {};
  obj['totalBoxImps'] = {value_count:{}};
  obj['totalBoxImps'].value_count = {field: 'imp.price'};

  obj['totalBoxCost'] = {sum:{}};
  obj['totalBoxCost'].sum = {field: 'imp.realPrice'};

  obj['totalBoxClicks'] = {value_count:{}};
  obj['totalBoxClicks'].value_count = {field: 'click.dateTime'};

  obj['totalBoxConversions'] = {value_count:{}};
  obj['totalBoxConversions'].value_count = {field: 'conversion.id'};

  obj['totalBoxRevenue'] = {sum:{}};
  obj['totalBoxRevenue'].sum = {field: 'conversion.payout'};


  return obj;
}

exports.getdspclicks = function(field) {

  var obj = {};
	obj[field.apiName] = {value_count:{}};
 	obj[field.apiName].value_count = {field: field.name};

  return obj;
}

exports.getdsprevenue = function(field){
    var obj = {};
    obj[field.apiName] = {sum:{}};
    obj[field.apiName].sum = {field: field.name};
    return obj;
}

exports.getdspprofit = function(field){
    var obj = {};
    obj[field.apiName + '1'] = {sum:{}};
    obj[field.apiName + '1'].sum = {field: field.name1};
    
    obj[field.apiName + '2'] = {sum:{}};
    obj[field.apiName + '2'].sum = {field: field.name2};    
    return obj;
}


/* SSP */

exports.getsspsopportunities = function(field){
	var obj = {};
	obj[field.apiName] = {value_count:{}};
 	obj[field.apiName].value_count = {field: field.name};

  	return obj;
}

exports.getsspsimpressions = function(field){
  var obj = {};
  obj[field.apiName] = {value_count:{}};
  obj[field.apiName].value_count = {field: field.name};

    return obj;
}

exports.getsspsfillrate = function(field){
    var obj = {};
    obj[field.apiName + '1'] = {value_count:{}};
    obj[field.apiName + '1'].value_count = {field: field.name1};

    obj[field.apiName + '2'] = {value_count:{}};
    obj[field.apiName + '2'].value_count = {field: field.name2};

    return obj;
}

exports.getsspsadresponse = function(field){
  	var obj = {};
  	obj[field.apiName] = {value_count:{}};
   	obj[field.apiName].value_count = {field: field.name};

  	return obj;
}

exports.getsspsresponseper = function(field){

  console.log('resfield', field);
  	var obj = {};
  	obj[field.apiName + '1'] = {value_count:{}};
   	obj[field.apiName + '1'].value_count = {field: field.name1};

   	obj[field.apiName + '2'] = {value_count:{}};
   	obj[field.apiName + '2'].value_count = {field: field.name2};

  	return obj;
}

exports.getsspswins = function(field){
  	var obj = {};
  	obj[field.apiName] = {value_count:{}};
   	obj[field.apiName].value_count = {field: field.name};

  	return obj;
}

exports.getsspswinsper = function(field){
  	var obj = {};
  	obj[field.apiName + '1'] = {value_count:{}};
   	obj[field.apiName + '1'].value_count = {field: field.name1};

   	obj[field.apiName + '2'] = {value_count:{}};
   	obj[field.apiName + '2'].value_count = {field: field.name2};

  	return obj;
}

exports.getsspsavgwin = function(field){
    var obj = {};
  	obj[field.apiName] = {avg:{}};
   	obj[field.apiName].avg = {field: field.name};

  	return obj;
}

exports.getsspsavgbid = function(field){
    var obj = {};
    obj[field.apiName] = {avg:{}};
    obj[field.apiName].avg = {field: field.name};

    return obj;
}

exports.getsspsefficiency = function(field) {
    var obj = {};
    obj[field.apiName + '1'] = {value_count:{}};
    obj[field.apiName + '1'].value_count = {field: field.name1};

    obj[field.apiName + '2'] = {value_count:{}};
    obj[field.apiName + '2'].value_count = {field: field.name2};

    return obj;
}

exports.getsspsrevenue = function(field) {
    var obj = {};
    obj[field.apiName] = {sum:{}};
    obj[field.apiName].sum =  {script : {inline : "doc['imp.auctionid'].empty ?  0 : doc['auction.win2Bid'].value / 1000", lang : "groovy"}}; 

    return obj;
}

exports.getsspscost = function(field) {
    var obj = {};
    obj[field.apiName] = {sum:{}};
    obj[field.apiName].sum =  {script : {inline : "doc['imp.auctionid'].empty ?  0 : doc['auction.request.imp.bidfloor'].value / 1000", lang : "groovy"}}; 

    return obj;
}

exports.getsspsprofit = function(field) {
    var obj = {};
    obj[field.apiName] = {sum:{}};
    obj[field.apiName].sum =  {script : {inline : "doc['imp.auctionid'].empty ? 0 : (doc['auction.win2Bid'].value / 1000) - (doc['auction.request.imp.bidfloor'].value / 1000)", lang : "groovy"}}; 

    return obj;
}

exports.getsspsmargin = function(field) {
    var obj = {};
    obj[field.apiName] = {sum:{}};
    obj[field.apiName].sum =  {script : {inline : "doc['imp.auctionid'].empty ? 0 : ((doc['auction.win2Bid'].value / 1000 - doc['auction.request.imp.bidfloor'].value / 1000) / doc['auction.win2Bid'].value / 1000) * 100", lang : "groovy"}}; 

    return obj;
}

exports.getsspscpm = function(field) {
    var obj = {};
    obj[field.apiName + '1'] = {sum:{}};
    obj[field.apiName + '1'].sum =  {script : {inline : "doc['imp.auctionid'].empty ? 0 : (doc['auction.request.imp.bidfloor'].value / 1000)", lang : "groovy"}}; 

    obj[field.apiName + '2'] = {value_count:{}};
    obj[field.apiName + '2'].value_count = {field: field.name1};

    return obj;
}

exports.getsspsrpm = function(field) {
    var obj = {};
    obj[field.apiName + '1'] = {sum:{}};
    obj[field.apiName + '1'].sum =  {script : {inline : "doc['imp.auctionid'].empty ? 0 : (doc['auction.win2Bid'].value / 1000)", lang : "groovy"}}; 

    obj[field.apiName + '2'] = {value_count:{}};
    obj[field.apiName + '2'].value_count = {field: field.name1};

    return obj;
}

exports.getsspsforensiqblock = function(field) {
    var obj = {};
    obj[field.apiName] = {sum:{}};
    obj[field.apiName].sum =  {script : {inline : "doc['auction.dsp.forensiqScore'].value > 64 ? 1 : 0", lang : "groovy"}}; 

    return obj;
}

exports.getssptotals = function(fieldName) {
    var obj = {};
    obj['totalBoxSspOpp'] = {value_count:{}};
    obj['totalBoxSspOpp'].value_count = {field: "auction.dsp._id"};

    obj['totalBoxSspImps'] = {value_count:{}};
    obj['totalBoxSspImps'].value_count = {field: "imp.auctionid" };

    obj['totalBoxSspCost'] = {sum:{}};
    obj['totalBoxSspCost'].sum =  {script : {inline : "doc['imp.auctionid'].empty ?  0 : doc['auction.request.imp.bidfloor'].value / 1000", lang : "groovy"}}; 

    obj['totalBoxSspRev'] = {sum:{}};
    obj['totalBoxSspRev'].sum =  {script : {inline : "doc['imp.auctionid'].empty ?  0 : doc['auction.win2Bid'].value / 1000", lang : "groovy"}}; 

    obj['totoalforensiqblock'] = {sum:{}};
    obj['totoalforensiqblock'].sum =  {script : {inline : "doc['auction.dsp.forensiqScore'].value > 64 ? 1 : 0", lang : "groovy"}}; 

    return obj;
}
