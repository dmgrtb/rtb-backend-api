var fieldsArr = [];

     /* DSP */

	//Dimensions Supply
	fieldsArr['publisher'] 		= {"apiName": "publisher", "name": "request.sspName", "type": "dimension"};
    fieldsArr['supplysource']   = {"apiName": "supplysource", "name": "response.general.supplySource", "type": "dimension"};
	fieldsArr['domain']    		= {"apiName": "domain", "name": "request.site.domain", "type": "dimension"};
	fieldsArr['app']       		= {"apiName": "app", "name": "request.app.name", "type": "dimension"};
	fieldsArr['appid']      	= {"apiName": "appid", "name": "request.app.id", "type": "dimension"};

	//Dimensions Type
	fieldsArr['device']    		= {"apiName": "device", "name": "request.device.make", "type": "dimension"};
	fieldsArr['useragent'] 		= {"apiName": "useragent", "name": "request.device.ua", "type": "dimension"};
	fieldsArr['connectiontype'] = {"apiName": "connectiontype", "name": "request.device.connectiontype", "type": "dimension"};
	fieldsArr['carrier'] 		= {"apiName": "carrier", "name": "request.device.isp", "type": "dimension"};
	fieldsArr['os']    	  	    = {"apiName": "os", "name": "request.device.os", "type": "dimension"};
	fieldsArr['ip']             = {"apiName": "ip", "name": "request.device.ip", "type": "dimension"};
	fieldsArr['iabcat']    		= {"apiName": "iabcat", "name": "request.app.cat", "type": "dimension"};
    fieldsArr['adtype']    		= {"apiName": "adtype", "name": "request.adType", "type": "dimension"};
    fieldsArr['department']     = {"apiName": "department", "name": "response.general.department", "type": "dimension"};
    fieldsArr['algorithm']     = {"apiName": "algorithm", "name": "request.algorithm", "type": "dimension"};

	//Dimensions Demand
	fieldsArr['advertiser'] 	= {"apiName": "advertiser", "name": "response.general.advertiserName", "type": "dimension"};
	fieldsArr['deal']    	   	= {"apiName": "deal", "name": "response.general.dealName", "type": "dimension"};
	fieldsArr['campaign']    	= {"apiName": "campaign", "name": "response.general.campaignName", "type": "dimension"};
	fieldsArr['creative']    	= {"apiName": "creative", "name": "response.general.creativeName", "type": "dimension"};
    fieldsArr['creativeid']    	= {"apiName": "creativeid", "name": "response.general.creativeId", "type": "dimension"};

	//Dimensions Geo
	fieldsArr['country']   		= {"apiName": "country", "name": "request.device.geo.country", "type": "dimension"};
	fieldsArr['city']   		= {"apiName": "city", "name": "request.device.geo.city", "type": "dimension"};

	//Metrics
    fieldsArr['request']		= {"apiName": "sum(requests) as request", "name": "request.id", "type": "matric"};
	fieldsArr['avgwin']    		= {"apiName": "case when sum(wins) = 0 then 0 else sum(impprice)/sum(wins)::numeric end as avgwin", "name": "imp.price", "type": "matric"};
	fieldsArr['avgbid']    		= {"apiName": "case when sum(responses) = 0 then 0 else sum(bidprice)/sum(responses) end as avgbid", "name": "response.seatbid.bid.price", "type": "matric"};
	fieldsArr['imp']       		= {"apiName": "sum(impressions) as imp", "name": "imp.price", "type": "matric"};
	fieldsArr['bidres']    		= {"apiName": "sum(responses) as bidres", "name": "response.seatbid.bid.price", "type": "matric"};
	fieldsArr['win']       		= {"apiName": "sum(wins) as win", "name": "win.dateTime", "type": "matric"};
	fieldsArr['resper']    		= {"apiName": "case when sum(requests) = 0 then 0 else sum(responses)/sum(requests)::numeric*100 end as resper", "name1": "response.seatbid.bid.price", "name2": "request.id", "type": "matric"};
	fieldsArr['winper']    		= {"apiName": "case when sum(responses) = 0 then 0 else sum(wins)/sum(responses)::numeric*100 end as winper", "name1": "win.dateTime", "name2": "response.seatbid.bid.price", "type": "matric"};
	fieldsArr['efficiency']		= {"apiName": "case when sum(wins) = 0 then 0 else sum(impressions)/sum(wins)::numeric*100 end as efficiency", "name1": "imp.price", "name2": "win.realPrice", "type": "matric"};
	fieldsArr['cost']      		= {"apiName": "sum(imprealprice) as cost", "name": "imp.realPrice", "type": "matric"};
	fieldsArr['clicks'] 		= {"apiName": "sum(clicks) as clicks", "name": "click.dateTime", "type": "matric"};
	fieldsArr['conversion'] 	= {"apiName": "sum(conversions) as conversion", "name": "conversion.id", "type": "matric"};
	fieldsArr['ctr'] 	        = {"apiName": "case when sum(impressions) = 0 then 0 else sum(clicks)/sum(impressions)::numeric*100 end as ctr", "name1": "click.dateTime" , "name2": "imp.price", "type": "matric"};
	fieldsArr['cvr'] 	        = {"apiName": "COALESCE(case when sum(clicks) = 0 then 0 else sum(conversions)/sum(clicks)::numeric*100 end, 0) as cvr", "name1": "conversion.id" , "name2": "click.dateTime", "type": "matric"};
	fieldsArr['revenue']		= {"apiName": "COALESCE(sum(conversionspayout), 0) as revenue", "name": "conversion.payout", "type": "matric"};
    fieldsArr['profit']		    = {"apiName": "COALESCE(sum(conversionspayout) - sum(imprealprice), 0) as profit", "name1": "conversion.payout" , "name2": "imp.realPrice", "type": "matric"};


    /* SSP */
    fieldsArr['ssupplypartner']  = {"apiName": "ssupplypartner", "name": "publisher", "type": "dimension"};
    fieldsArr['ssupplysource']   = {"apiName": "ssupplysource", "name": "supplySource", "type": "dimension"};
    fieldsArr['sdemandpartner']  = {"apiName": "sdemandpartner", "name": "auction.dsp.demandPartner", "type": "dimension"};
    fieldsArr['sdemanddeal']     = {"apiName": "sdemanddeal", "name": "auction.dsp.demandDeal", "type": "dimension"};
    fieldsArr['sdomain']         = {"apiName": "sdomain", "name": "auction.request.site.domain", "type": "dimension"};
    fieldsArr['sappname']        = {"apiName": "sappname", "name": "auction.request.app.name", "type": "dimension"};
    fieldsArr['sbundle']         = {"apiName": "sbundle", "name": "auction.request.app.bundle", "type": "dimension"};
    fieldsArr['scountry']        = {"apiName": "scountry", "name": "auction.request.device.geo.country", "type": "dimension"};
    fieldsArr['sdevicetype']     = {"apiName": "sdevicetype", "name": "auction.request.device.model", "type": "dimension"};
    fieldsArr['sos']             = {"apiName": "sos", "name": "auction.request.device.os", "type": "dimension"};
    fieldsArr['suseragent']      = {"apiName": "suseragent", "name": "auction.request.device.ua", "type": "dimension"};
    fieldsArr['sdemandtag']      = {"apiName": "sdemandtag", "name": "auction.dsp.endPoint", "type": "dimension"};
    fieldsArr['sdemanddealtype'] = {"apiName": "sdemanddealtype", "name": "auction.dsp.demandDealType", "type": "dimension"};
	fieldsArr['iabcat'] 		 = {"apiName": "iabcat", "name": "auction.dsp.demandDealType", "type": "dimension"};

    //Dimensions Player
    fieldsArr['playersize']   	 = {"apiName": "playersize", "name1": "publisher.w", "name2": "publisher.h", "type": "dimension"};

    //Metrics
	fieldsArr['sopportunities']  = {"apiName": "sum(sopportunities) as sopportunities", "name": "auction.dsp._id", "type": "matric"};
	fieldsArr['simpressions']    = {"apiName": "sum(simpressions) as simpressions", "name": "imp.auctionid", "type": "matric"};
	fieldsArr['sadresponse'] 	 = {"apiName": "COALESCE(sum(responses), 0) as sadresponse", "name": "auction.dsp.response.id", "type": "matric"};
	fieldsArr['sresponseper']    = {"apiName": "case when sum(sopportunities) = 0 then 0 else COALESCE(sum(responses)/sum(sopportunities)::numeric*100, 0) end as sresponseper", "name1": "auction.dsp.response.id", "name2": "auction.dsp._id", "type": "matric"};
	fieldsArr['swins']    	     = {"apiName": "sum(swins) as swins", "name": "auction.winDspId", "type": "matric"};
	fieldsArr['swinsper']    	 = {"apiName": "case when sum(sopportunities) = 0 then 0 else sum(swins)/sum(sopportunities)::numeric*100 end as swinsper", "name1": "auction.winDspId", "name2": "auction.dsp._id", "type": "matric"};
	fieldsArr['sefficiency']	 = {"apiName": "case when sum(swins) = 0 then 0 else sum(simpressions)/sum(swins)::numeric*100 end as sefficiency", "name1": "imp.auctionid", "name2": "auction.winDspId", "type": "matric"};
	fieldsArr['savgwin']    	 = {"apiName": "case when sum (swins) = 0 then 0 else sum(swin2bidall)/sum(swins)::numeric end as savgwin", "name": "auction.win2Bid", "type": "matric"};
	fieldsArr['savgbid'] 		 = {"apiName": "case when sum (swins) = 0 then 0 else sum(swinbidall)/sum(swins)::numeric end as savgbid", "name": "auction.winBid", "type": "matric"};
	fieldsArr['srevenue'] 		 = {"apiName": "COALESCE(sum(win2bid)/1000::numeric, 0) as srevenue", "name1": "imp.auctionid", "name2": "auction.win2Bid", "type": "matric"};
	fieldsArr['scost'] 		 	 = {"apiName": "COALESCE(sum(scost)/1000::numeric, 0) as scost", "name1": "imp.auctionid", "name2": "auction.request.imp.bidfloor", "type": "matric"};
	fieldsArr['sprofit'] 		 = {"apiName": "COALESCE((sum(win2bid)/1000 - sum(scost)/1000)::numeric, 0) as sprofit", "name1": "imp.auctionid", "name2": "auction.win2Bid", "name3": "auction.request.imp.bidfloor", "type": "matric"};
	fieldsArr['scpm'] 		 	 = {"apiName": "case when sum (simpressions) = 0 then 0 else COALESCE(sum(scost)/1000 / sum(simpressions)*1000, 0) end as scpm", "name1": "imp.auctionid", "name2": "auction.request.imp.bidfloor", "type": "matric"};
	fieldsArr['srpm']      		 = {"apiName": "case when sum (simpressions) = 0 then 0 else COALESCE(sum(win2bid)/1000 / sum(simpressions)*1000, 0) end as srpm", "name1": "imp.auctionid", "name2": "auction.win2Bid", "type": "matric"};
	fieldsArr['smargin']             = {"apiName": "case when sum(win2bid) = 0 then 0 else COALESCE((sum(win2bid) - sum(scost)) / sum(win2bid) * 100, 0) end as smargin", "name1": "imp.auctionid", "name2": "auction.win2Bid", "name3": "auction.request.imp.bidfloor", "type": "matric"};
	fieldsArr['sfillrate'] 	     = {"apiName": "case when sum (sopportunities) = 0 then 0 else sum(simpressions)/sum(sopportunities)::numeric*100 end as sfillrate", "name1": "imp.auctionid" , "name2": "auction.dsp._id", "type": "matric"};
	fieldsArr['sforensiqblock']  = {"apiName": "sum(riskscore) as sforensiqblock", "type": "matric"};
	fieldsArr['savgbidfloor']    = {"apiName": "case when sum(sopportunities) = 0 then 0 else COALESCE(sum(allbidfloor)/sum(sopportunities)::numeric, 0) end as savgbidfloor", "type": "matric"};


        /* XML */
    fieldsArr['xsupplypartner']  = {"apiName": "xsupplypartner", "name": "publisher", "type": "dimension"};
    fieldsArr['xsupplysource']   = {"apiName": "xsupplysource", "name": "supplySource", "type": "dimension"};
    fieldsArr['xdemandpartner']  = {"apiName": "xdemandpartner", "name": "auction.dsp.demandPartner", "type": "dimension"};
    fieldsArr['xdemanddeal']     = {"apiName": "xdemanddeal","name": "auction.dsp.demandDeal", "type": "dimension"};
    fieldsArr['xdomain']         = {"apiName": "xdomain", "name": "auction.request.site.domain", "type": "dimension"};
    fieldsArr['xcountry']        = {"apiName": "xcountry", "name": "auction.request.device.geo.country", "type": "dimension"};
    fieldsArr['xos']             = {"apiName": "xos", "name": "auction.request.device.os", "type": "dimension"};
    fieldsArr['xbrowser']        = {"apiName": "xbrowser", "name": "auction.request.device.os", "type": "dimension"};
    fieldsArr['xcampaign']       = {"apiName": "xcampaign", "name": "auction.dsp.campaign", "type": "dimension"};
    fieldsArr['xblockreason']    = {"apiName": "xblockreason", "name": "auction.dsp.error", "type": "dimension"};
    fieldsArr['xsubid']          = {"apiName": "xsubid", "name": "auction.request.subid", "type": "dimension"};
    fieldsArr['xkeywords']       = {"apiName": "xkeywords", "name": "auction.request.keywords", "type": "dimension"};


    //Metrics
	fieldsArr['xrequests']  = {"apiName": "sum(xrequests) as xrequests",/* "name": "auction.dsp._id",*/ "type": "matric"};
	fieldsArr['ximpressions'] = {"apiName": "sum(ximpressions) as ximpressions",/* "name": "imp.auctionid",*/ "type": "matric"};
    fieldsArr['ximpressionsper'] = {"apiName": "case when sum(xrequests) = 0 then 0 else COALESCE(sum(ximpressions)/sum(xrequests)::numeric*100, 0) end as ximpressionsper",/* "name": "imp.auctionid",*/ "type": "matric"};
	fieldsArr['xresponse'] = {"apiName": "COALESCE(sum(xresponse), 0) as xresponse", /*"name": "auction.dsp.response.id",*/ "type": "matric"};
	fieldsArr['xresponseper'] = {"apiName": "case when sum(xrequests) = 0 then 0 else COALESCE(sum(xresponse)/sum(xrequests)::numeric*100, 0) end as xresponseper",/* "name1": "auction.dsp.response.id", "name2": "auction.dsp._id", */"type": "matric"};
	fieldsArr['xwin'] = {"apiName": "sum(xwin) as xwin", /*"name": "auction.winDspId", */"type": "matric"};
	fieldsArr['xwinper'] = {"apiName": "case when sum(xresponse) = 0 then 0 else sum(xwin)/sum(xresponse)::numeric*100 end as xwinper",/* "name1": "auction.winDspId", "name2": "auction.dsp._id",*/ "type": "matric"};
	fieldsArr['xavgwinprice'] = {"apiName": "case when sum (xwin) = 0 then 0 else sum(xwinbidall)/sum(xwin)::numeric end as xavgwinprice", /*"name": "auction.win2Bid",*/ "type": "matric"};
	fieldsArr['xavgbidprice'] = {"apiName": "case when sum (xwin) = 0 then 0 else sum(xwinbidall)/sum(xwin)::numeric end as xavgbidprice",/* "name": "auction.winBid",*/ "type": "matric"};
	fieldsArr['xrev'] = {"apiName": "COALESCE(sum(xwinbid)::numeric, 0) as xrev",/* "name1": "imp.auctionid", "name2": "auction.win2Bid",*/ "type": "matric"};
	fieldsArr['xcost'] = {"apiName": "COALESCE(sum(xcost)::numeric, 0) as xcost",/* "name1": "imp.auctionid", "name2": "auction.request.imp.bidfloor",*/ "type": "matric"};
	fieldsArr['xprofit'] = {"apiName": "COALESCE((sum(xwinbid) - sum(xcost))::numeric, 0) as xprofit", /*"name1": "imp.auctionid", "name2": "auction.win2Bid", "name3": "auction.request.imp.bidfloor",*/ "type": "matric"};
	fieldsArr['xcpm'] = {"apiName": "case when sum(ximpressions) = 0 then 0 else COALESCE(sum(xcost) / sum(ximpressions), 0) end as xcpm", /*"name1": "imp.auctionid", "name2": "auction.request.imp.bidfloor",*/ "type": "matric"};
	fieldsArr['xmargin'] = {"apiName": "case when sum(xwinbid) = 0 then 0 else COALESCE((sum(xwinbid) - sum(xcost)) / sum(xwinbid) * 100, 0) end as xmargin",/* "name1": "imp.auctionid", "name2": "auction.win2Bid", "name3": "auction.request.imp.bidfloor",*/ "type": "matric"};
	fieldsArr['xblock'] = {"apiName": "sum(xblock) as xblock", "type": "matric"};
	fieldsArr['xblockper'] = {"apiName": "case when sum(xrequests) = 0 then 0 else COALESCE(sum(xblock)/sum(xrequests)::numeric, 0) end as xblockper", "type": "matric"};
        
        /*Rotations*/
        //dimentions
        fieldsArr['rname']           = {"apiName": "rname", "name": "rotation_name", "type": "dimension"};
        fieldsArr['rcampaign']       = {"apiName": "rcampaign", "name": "campaign_name", "type": "dimension"};
        fieldsArr['rpublisher']      = {"apiName": "rpublisher", "name": "publisher", "type": "dimension"};
        fieldsArr['isp']             = {"apiName": "isp","name": "isp", "type": "dimension"};
        fieldsArr['rdevice']         = {"apiName": "rdevice", "name": "device", "type": "dimension"};
        fieldsArr['rcountry']        = {"apiName": "rcountry", "name": "country", "type": "dimension"};
        fieldsArr['ros']             = {"apiName": "ros", "name": "os", "type": "dimension"};
        fieldsArr['rosv']            = {"apiName": "rosv", "name": "osv", "type": "dimension"};
        fieldsArr['rplacementid']    = {"apiName": "rplacementid", "name": "placement_id", "type": "dimension"};
        fieldsArr['rplacementname']  = {"apiName": "rplacementname", "name": "placement_name", "type": "dimension"};
        fieldsArr['udidraw']         = {"apiName": "udidraw", "name": "udid", "type": "dimension"};
        
        //matrics
        fieldsArr['cin']  = {"apiName": "sum(cin) as cin", "type": "matric"};
        fieldsArr['cout'] = {"apiName": "sum(cout) as cout", "type": "matric"};
        fieldsArr['rconversions'] = {"apiName": "sum(rconversions) as rconversions", "type": "matric"};
        
        

module.exports = fieldsArr;
