var moment = init.moment();

exports.queryAll = function(paramsArr, req, res) {
    
    var dimensions = [];
    var values = [];
    var matrics = [];
    var fields = '';
    var groupBy = '';
    var mFields = '';
    var date = '';
    var where = 'and ';


    for (param in paramsArr) {

        if (paramsArr[param].type == 'dimension') {
            fields += paramsArr[param].apiName + ',';
            groupBy += paramsArr[param].apiName + ',';

            if (paramsArr[param].value !== '') {
                where += paramsArr[param].apiName + '=' + '\'' + paramsArr[param].value + '\'' + ' and ';
            }
            if (paramsArr[param].apiName == 'xblockreason'){
                where += 'xblockreason != \'NA\'';
            }
        }
        else if (paramsArr[param].type == 'matric') {
            mFields += paramsArr[param].apiName + ',';
            matrics.push(paramsArr[param].apiName);
        }
    }

    if (req.query.interval == 'hour') {
        fields += 'to_char(date, \'yyyy-mm-dd hh12 AM\') as date';
        groupBy += 'date';

    }
    else if(req.query.interval == 'day' ) {
        fields += 'to_char(date, \'yyyy-mm-dd\') as date';
        groupBy += 'to_char(date, \'yyyy-mm-dd\')';

    }
    else if(req.query.interval == 'month') {
        fields += 'to_char(date, \'yyyy-mm\') as date';
        groupBy += 'to_char(date, \'yyyy-mm\')';

    }
    else {
        fields = fields.slice(0, -1);
        groupBy = groupBy.slice(0, -1);
    }

    mFields = mFields.slice(0, -1);
    where = where.replace(/and $/, '');

    var from = moment(req.query.from).format("YYYY-MM-DD 00:00:00");
    var to = moment(req.query.to).format("YYYY-MM-DD 23:59:59");

    var date = 'where date between ' + "'" + from + "'" + ' and ' + "'" + to + "' " + where;

    if (req.session.ssp === true && req.session.type == 'external' && req.query.type == 'ssp') {
        date += 'and ssupplypartner = ' + "'" + req.session.name + "'";
    }
    else if (req.session.xml === true && req.session.type == 'external' && req.query.type == 'xml') {
        date += 'and xsupplypartner = ' + "'" + req.session.name + "'";
    } 

    var type = req.query.type;
    console.log('select ' + fields + ',' + mFields + ' from ' + type + ' ' + date + ' group by ' + groupBy);

    Promise.all([searchQuery(type, fields, mFields, date, groupBy), totalQuery(type, date)]).then(([rows, totals]) => {


        sendResponse.sendJson(res, 200, {rows, totals});
    }).catch((err) => {

    });
}

function searchQuery(type, fields, mFields, date, groupBy) {

    return promiseQuery('select ' + fields + ',' + mFields + ' from ' + type + ' ' + date + ' group by ' + groupBy, []);
}

function totalQuery(type, date) {

    if (type == 'xml') {
        return promiseQuery('select  COALESCE(sum(xrequests), 0) as totalBoxXmlReq,  COALESCE(sum(ximpressions), 0) as totalBoxXmlImps, COALESCE(sum(xwinbid), 0) as totalBoxXmlRev, COALESCE(sum(xcost), 0) as totalBoxXmlCost,  COALESCE(sum(xblock), 0) as totalXmlBlock from xml ' + date + ';', []);
    }
    else if (type == 'ssp') {
        return promiseQuery('select  COALESCE(sum(sopportunities), 0) as totalBoxSspOpp,  COALESCE(sum(simpressions), 0) as totalBoxSspImps, COALESCE(sum(win2bid)/1000, 0) as totalBoxSspRev, COALESCE(sum(scost)/1000, 0) as totalBoxSspCost,  COALESCE(sum(riskscore), 0) as totalforensiqblock from ssp ' + date + ';', []);
    }
    else if (type == 'rot') {
        return promiseQuery('select  COALESCE(sum(cin), 0) as totalClicksIn,  COALESCE(sum(cout), 0) as totalClicksOut from rot ' + date + ';', []);
    }
    else {
        return promiseQuery('select COALESCE(sum(impressions), 0) as totalBoxImps, COALESCE(sum(imprealprice), 0) as totalBoxCost, COALESCE(sum(clicks), 0) as totalBoxClicks, COALESCE(sum(conversions), 0) as totalBoxConversions, COALESCE(sum(conversionspayout), 0) as totalBoxRevenue from dsp ' + date + ';', []);
    }

}

function promiseQuery(sql, values) {

    return new Promise(function(resolve, reject) {
        pool.query(sql, values, function(err, result) {
            if (err) {console.log(err); reject(err)}
            else resolve(result.rows);
        })
    });
}
