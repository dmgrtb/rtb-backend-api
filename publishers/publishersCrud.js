var mongoose = require('mongoose');

exports.findPublisher = function(req, res) {
	var Publishers = schemas.publishers;
	Publishers.aggregate({$match: {status: { $ne: 'deleted' }}}, {$project: {_id: 1, pubName: 1, frequencyCap: 1, endpoint: 1, cap: {$divide: ['$cap', 1000]}}}, function (err, result) {
		if (err || result === null) {
			status = 400;
			sendResponse.sendHttpStatus(res, status);
		} else {
			status = 200;
			var pubObj = {publishers: {}};
			pubObj.publishers = result;
			sendResponse.sendJson(res, status, pubObj);
		}
	});
}


exports.findPublisherById = function(req, res) {
	var status;
	var Publishers = schemas.publishers;
	Publishers.aggregate([{$match: {_id: mongoose.Types.ObjectId(req.params.id)}},
									{ $match: {"status": {$ne: 'deleted' } } }, {$project: {pubName: 1, pubId: 1, status: 1, seatId: 1, trafficSourceId: 1}}], function (err, result) {
		if (err || result === null) {
			status = 400;
			sendResponse.sendHttpStatus(res, status);
		} else {
			status = 200;
			var pubObj = {publishers: {}};
			pubObj.publishers = result;
			sendResponse.sendJson(res, status, pubObj);
		}
	});
}


exports.addPublisher = function(req, res) {
	var status;
	var Publishers = schemas.publishers;
	var id = mongoose.Types.ObjectId();

	if ( req.body.pubName !== undefined &&  !isNaN(req.body.trafficSourceId) &&  req.body.trafficSourceId !== undefined && req.body.cap !== undefined) {
		var newPublisher = new Publishers({_id: id, pubId: id, pubName: req.body.pubName, seatId: req.body.pubName,  cap: req.body.cap*1000, trafficSourceId: req.body.trafficSourceId,
			endpoint: config.dspUrl + id, macros: {"partner_var": "${AUCTION_ID}_${AUCTION_IMP_ID}"}, status: 'active'});

		if (req.body.frequencyCap && !isNaN(req.body.frequencyCap) && req.body.frequencyCap >= -1 && req.body.frequencyCap <= 9) {
                    newPublisher = Object.assign(newPublisher, {frequencyCap: req.body.frequencyCap});
		}
			newPublisher.save(function (err, result) {
				console.log(err);
			if (err) status = 400;
			status = 201;
			sendResponse.sendHttpStatus(res, status);
		});

	} else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}


exports.editPublisher = function(req, res) {
	var status;
	var Publishers = schemas.publishers;
	var setParams;
	if ( req.body.pubName !== undefined && req.body.pubId !== undefined && req.body.status !== undefined && req.body.trafficSourceId !== undefined && req.body.cap !== undefined) {
		 setParams = {_id: req.body.pubId, pubId: req.body.pubId, pubName:req.body.pubName, cap: req.body.cap*1000, trafficSourceId: req.body.trafficSourceId,
		 	           status: req.body.status, macros: {"partner_var": "${AUCTION_ID}_${AUCTION_IMP_ID}"}, endpoint: config.dspUrl + req.body.pubId};
	}

	if (req.body.frequencyCap && !isNaN(req.body.frequencyCap) && req.body.frequencyCap >= -1 && req.body.frequencyCap <= 9) {
                setParams = Object.assign(setParams, {frequencyCap: req.body.frequencyCap});
	} else {
		setParams = Object.assign(setParams, {frequencyCap: config.frequencyCap});
	}

	if (setParams === undefined) {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	} else {
		Publishers.findByIdAndUpdate(req.body.pubId, { $set: setParams }, {new: true}, function (err, result) {
			if (err || result.n === 0) {
				status = 400;
			} else {
				status = 200;
			}
			sendResponse.sendHttpStatus(res, status);
		});
	}
}


exports.deletePublisher = function(req, res) {
	var status;
	var Publishers = schemas.publishers;

	if (req.body.pubId !== undefined) {
		Publishers.findByIdAndUpdate(req.body.pubId, { $set: { status: 'deleted' } },function (err, result) {
                    console.log('initial result');
                    console.log(result);
			if (err || result.n === 0) {
				status = 400;
			} else {
                                if (typeof result.bids !== 'undefined' && Array.isArray(result.bids) && result.bids.length > 0){
                                    for (var i = 0; i < result.bids.length; i++){
                                        var setParams = { '$set': {} };
                                        var dealsStatus = 'bids.' + i + '.status';
                                        setParams[dealsStatus] = 'deleted';
                                        Publishers.findByIdAndUpdate(req.body.pubId, setParams, function (err, result) {
                                            if (err){
                                                console.log('Failed deleting supply source id ' + result.deals[i]._id + ' when deleting publisher id ' + req.body.advId + '. Error message -');
                                                console.log(err);
                                            }
                                        });
                                    }
                                }
				status = 204;
			}
			sendResponse.sendHttpStatus(res, status);
		});
	} else {
		status = 400;
		sendResponse.sendHttpStatus(res, status);
	}
}
