var mongoose = require('mongoose');
var assert = require('assert');
var supplyConfig = require('../config/supplyConfig.js');

exports.findSupply = function (req, res) {
    var Publishers = schemas.publishers;
    var matchPub = {"$match": {"_id": {"$ne": null}}};
    if (typeof req.query.pubId !== "undefined") {
        var pubId = mongoose.Types.ObjectId(req.query.pubId);
        matchPub = {"$match": {"_id": pubId}};
    }
    Publishers.aggregate(matchPub, {$unwind: "$bids"}, {$project: {pubName: 1, status: 1, bids: 1}}, {$match: {"bids.status": {$ne: 'deleted'}}}, function (err, result) {
        if (err || result === null) {
            status = 400;
            sendResponse.sendHttpStatus(res, status);
        } else {
            status = 200;
            var supplyObj = {supply: {}};
            supplyObj.supply = result;
            sendResponse.sendJson(res, status, supplyObj);
        }
    });
}


exports.findSupplyById = function (req, res) {
    var Publishers = schemas.publishers;
    var params = {};

    if (Array.isArray(req.query.id)) {
        var supplyIdsArr = [];

        for (supplyId in req.query.id) {
            supplyIdsArr.push(mongoose.Types.ObjectId(req.query.id[supplyId]));
        }

        params = {"bids._id": {$in: supplyIdsArr}};
    } else {
        params = {"bids._id": mongoose.Types.ObjectId(req.query.id)};
    }

    Publishers.aggregate({$unwind: "$bids"}, {$match: params},
            {$match: {"bids.status": {$ne: 'deleted'}}}, function (err, result) {

        if (err) {
            status = 400;
            sendResponse.sendHttpStatus(res, status);
        } else {
            status = 200;
            var supplyObj = {supply: {}};
            supplyObj.supply = result;
            sendResponse.sendJson(res, status, supplyObj);
        }
    });
}

exports.addSupply = function (req, res) {
    var Publishers = schemas.publishers;
    var Advertisers = schemas.advertisers;
    var _id = mongoose.Types.ObjectId();

    supplyValidate(req, _id, req.body.status, function (params) {

        if (params === undefined) {
            status = 400;
            sendResponse.sendHttpStatus(res, status);
        } else {
            Publishers.findByIdAndUpdate(req.body.pubId, {$push: {"bids": params}}, {new : true}, function (err, result) {
                if (err || result === null) {
                    status = 400;
                } else {
                    status = 201;
                }

                if (params.campaign !== undefined) {
                    if (Array.isArray(params.campaign)) {
                        updateCampaigns(params.campaign, params, req.body.supplyId, res);
                    }
                } else {
                    updateCampaigns([], params, req.body.supplyId, res);

                }
                sendResponse.sendHttpStatus(res, status);
            });
        }
    });
}


exports.editSupply = function (req, res) {
    var Advertisers = schemas.advertisers;
    var Publishers = schemas.publishers;

    supplyValidate(req, req.body.supplyId, req.body.status, function (params) {
        if (params === undefined) {
            status = 400;
            sendResponse.sendHttpStatus(res, status);
        } else {
            Publishers.update({_id: req.body.pubId, "bids._id": req.body.supplyId}, {$set: {"bids.$": params}}, {new : true}, function (err, result) {
                if (err || result.n === 0) {
                    status = 400;
                } else {
                    status = 201;
                }


                if (params.campaign !== undefined) {
                    if (Array.isArray(params.campaign)) {
                        updateCampaigns(params.campaign, params, req.body.supplyId, res);
                    }
                } else {
                    updateCampaigns([], params, req.body.supplyId, res);

                }
                sendResponse.sendHttpStatus(res, status);
            });
        }
    });
}

exports.deleteSupply = function (req, res) {
    var Publishers = schemas.publishers;

    Publishers.update({"bids._id": req.body.supplyId}, {'$set': {'bids.$.status': 'deleted'}}, {new : true}, function (err, result) {
        if (err || result.n === 0) {
            status = 400;
        } else {
            status = 201;
        }
        sendResponse.sendHttpStatus(res, status);
    });
}


function supplyValidate(req, id, status, callback) {
    var params;

    if ((((req.body.supplyType === 'display-rtb' || req.body.supplyType === 'video-vast' || req.body.supplyType === 'vast-vast' || req.body.supplyType === 'native') && !isNaN(req.body.maxBid)) || req.body.supplyType === 'xml')
            && id !== undefined && req.body.pubId !== undefined && req.body.supplyName !== undefined  && status !== undefined) {

        params = {_id: id, supplyName: req.body.supplyName, minBid: req.body.maxBid, status: status, supplyType: req.body.supplyType, target: {}};
        if (req.body.frequencyCap && !isNaN(req.body.frequencyCap) && req.body.frequencyCap >= -1 && req.body.frequencyCap <= 9) {
            params.frequencyCap = req.body.frequencyCap;
        } else {
            params.frequencyCap = -1;
        }

        if (req.body.supplyType === 'video-vast' || req.body.supplyType === 'vast-vast') {
            if (req.body.environment) {
                params.environment = req.body.environment;
                params.target = Object.assign(params.target, {endpoint: supplyConfig[req.body.platform].macro[req.body.environment + 'Url'] + id + '?pid=' + req.body.pubId + supplyConfig[req.body.platform].macro[req.body.environment + 'Rest']});
            } else {
                callback();
                return;
            }
        }
        
        if(req.body.supplyType === 'xml'){
            params.target = Object.assign(params.target, {endpoint: supplyConfig['xml'].Url + id + '?' + supplyConfig['xml'].Rest});
        }

        if (req.body.platform !== undefined) {
            params.platform = req.body.platform;
        }
        if (req.body.margin !== undefined) {
            params.margin = req.body.margin;
        }
        if (req.body.fallback !== undefined) {
            params.fallback = req.body.fallback;
        }
        if (req.body.calcBidFloor !== undefined) {
            params.calcBidFloor = req.body.calcBidFloor;
        }

        if (req.body.tagType !== undefined) {
            params.tagType = req.body.tagType;
        }

        if (req.body.campaign !== undefined) {
            var campaigns;
            if (Array.isArray(req.body.campaign)) {
                campaigns = req.body.campaign;
            } else {
                campaigns = [req.body.campaign];
            }
            params.campaign = campaigns;
        }

        if (req.body.country !== undefined) {
            if (Array.isArray(req.body.country)) {
                params.target = Object.assign(params.target, {country: req.body.country});
            } else {
                params.target = Object.assign(params.target, {country: [req.body.country]});
            }
        }
        if (req.body.mobileOs !== undefined) {
            if (Array.isArray(req.body.mobileOs)) {
                params.target = Object.assign(params.target, {mobileOs: req.body.mobileOs});
            } else {
                params.target = Object.assign(params.target, {mobileOs: [req.body.mobileOs]});
            }
        }
        if (req.body.desktopOs !== undefined) {
            if (Array.isArray(req.body.desktopOs)) {
                params.target = Object.assign(params.target, {desktopOs: req.body.desktopOs});
            } else {
                params.target = Object.assign(params.target, {desktopOs: [req.body.desktopOs]});
            }
        }
        if (req.body.domain !== undefined) {
            var domains;
            if (Array.isArray(req.body.domain)) {
                domains = convertComplexStrings(req.body.domain);
            } else {
                domains = [req.body.domain];
                domains = convertComplexStrings(domains);
            }
            params.target = Object.assign(params.target, {domain: domains});
        }
        if (req.body.connectionType !== undefined) {
            if (Array.isArray(req.body.connectionType)) {
                var connectionType = req.body.connectionType.map(function (cType) {
                    return parseInt(cType, 10);
                });
                params.target = Object.assign(params.target, {connectionType: connectionType});
            } else {
                var connectionType = parseInt(req.body.connectionType, 10);
                params.target = Object.assign(params.target, {connectionType: [connectionType]});
            }
        }
        if (req.body.app !== undefined && req.body.appList !== undefined) {
            var apps;
            if (Array.isArray(req.body.app)) {
                apps = convertComplexStrings(req.body.app);
            } else {
                apps = [req.body.app];
                apps = convertComplexStrings(apps);
            }
            if (req.body.appList === 'black') {
                params.target = Object.assign(params.target, {app_black: apps});
            } else {
                params.target = Object.assign(params.target, {app: apps});
            }
        }
        if (req.body.keywords !== undefined && req.body.keywordsList !== undefined) {
            var keywords;
            if (Array.isArray(req.body.keywords)) {
                keywords = req.body.keywords;
            } else {
                keywords = [req.body.keywords];
            }
            if (req.body.keywordsList === 'black') {
                params.target = Object.assign(params.target, {keywords_black: keywords});
            } else {
                params.target = Object.assign(params.target, {keywords: keywords});
            }
        }
        if (req.body.subid !== undefined && req.body.subidList !== undefined) {
            var subid;
            if (Array.isArray(req.body.subid)) {
                subid = req.body.subid;
            } else {
                subid = [req.body.subid];
            }
            if (req.body.subidList === 'black') {
                params.target = Object.assign(params.target, {subid_black: subid});
            } else {
                params.target = Object.assign(params.target, {subid: subid});
            }
        }
        if (req.body.widthXheight !== undefined) {
            if (Array.isArray(req.body.widthXheight)) {
                params.target = Object.assign(params.target, {widthXheight: req.body.widthXheight});
            } else {
                params.target = Object.assign(params.target, {widthXheight: [req.body.widthXheight]});
            }
        }
        callback(params);
    } else if (req.body.supplyType === 'video-rtb' && id !== undefined && req.body.pubId !== undefined && req.body.supplyName !== undefined && status !== undefined) {
        params = {_id: id, supplyName: req.body.supplyName, status: status, supplyType: req.body.supplyType, target: {endpoint: config.dspUrl + id}};
        callback(params);
    } else {
        callback();
    }
}

function convertComplexStrings(complexStringArray) {
    var dbObject = {};
    for (var i = 0; i < complexStringArray.length; i++) {
        var stringSplit = complexStringArray[i].split('___');
        var pubIndex = stringSplit.length - 1;
        if (pubIndex > 0) {
            var pubId = stringSplit[pubIndex].trim();
            if (typeof dbObject[pubId] === 'undefined') {
                dbObject[pubId] = {};
            }
            var targetId = stringSplit[pubIndex - 1].trim();
            
            var targetName = pubIndex === 2 ? stringSplit[0].trim() : '';
            dbObject[pubId][targetId] = targetName;
        }
    }
    return dbObject;
}


function updateCampaigns(campaigns, params, _id, res) {
    var Publishers = schemas.publishers;
    var Advertisers = schemas.advertisers;
    var i;
    Advertisers.find({}, {"campaigns.target": 1, "campaigns.name": 1}, function (err, result) {

        if (err || result === null) {
            console.log(err);
            status = 400;
            sendResponse.sendHttpStatus(res, status);
        } else {
            for (j = 0; j < result.length; j++) {
                for (i in result[j].campaigns) {
                    if (result[j].campaigns[i] !== undefined && result[j].campaigns[i].target !== undefined) {
                        if (result[j].campaigns[i].target.supplySource_black !== undefined && result[j].campaigns[i].target.supplySource_black !== null) {
                            blackList = result[j].campaigns[i].target.supplySource_black;
                            for (var k = 0; k < blackList.length; k++) {
                                if (blackList[k] === _id) {
                                    blackList.splice(k, 1);
                                }
                            }
                            if (inArray(result[j].campaigns[i].name, campaigns)) {
                                set = {$pull: {"campaigns.$.target.supplySource_black": _id}};
                                        
                                Advertisers.update({"campaigns.name": result[j].campaigns[i].name}, set, {new : true}, function (err, result) {

                                    if (err || result === null) {
                                        console.log(err);
                                        status = 400;
                                        sendResponse.sendHttpStatus(res, status);
                                    } else {
                                        status = 201;
                                    }

                                });
                            }
                        } else {
                            if (inArray(result[j].campaigns[i].name, campaigns)) {
                                set = {$addToSet: {"campaigns.$.target.supplySource": _id}};
                            } else {
                                set = {$pull: {"campaigns.$.target.supplySource": _id}};
                            }
                            Advertisers.update({"campaigns.name": result[j].campaigns[i].name}, set, {new : true}, function (err, result) {
                                if (err || result === null) {
                                    console.log(err);
                                    status = 400;
                                    sendResponse.sendHttpStatus(res, status);
                                } else {
                                    status = 201;
                                }

                            });

                        }

                        status = 200;
                        var supplyObj = {supply: {}};
                        supplyObj.supply = result;

                    }
                }
            }
        }
    });
}

function inArray(needle, array) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] == needle) {
            return true;
        }
    }
    return false;
}
