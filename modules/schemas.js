var Schema = mongoose.Schema;
var ObjectIdSchema = Schema.ObjectId;
var ObjectId = mongoose.Types.ObjectId;


var UserSchema = new Schema({
            _id:      { type: ObjectIdSchema, auto: true },
            username: { type: String },
            password: { type: String },
            type:     { type: String },
            token:    { type: String },
            key:      { type: String },
            ssp:      { type: Boolean },
            dsp:      { type: Boolean },
            xml:      { type: Boolean },
            name:     { type: String },
            status:   { type: String },
            expired:  { type: Date }
}, { versionKey: false });

var TargetSchema = new Schema({
             country: { type: Array },
      connectionType: { type: Array },
                  os: { type: Array },
                 pos: { type: Array },
              domain: { type: Array },
                 app: { type: Array },
        supplySource: { type: ObjectIdSchema, "default":[] },
          categories: { type: Array },
            carriers: { type: Array },
}, { _id : false });

var CampaignsSchema = new Schema({
                 _id: { type: ObjectIdSchema },
              status: { type: String },
                name: { type: String },
        campaignType: { type: String },
        pricingModel: { type: String },
              maxBid: { type: Number },
         payoutModel: { type: String },
              payout: { type: Number },
                 cap: { type: Number },
               spent: { type: Number, "default": 0 },
       learningSpent: { type: Number, "default": 0 },
   optimizationSpent: { type: Number, "default": 0 },
      maxImpressions: { type: Number },
           creatives: [{ type: ObjectIdSchema, "default":[] }],
        frequencyCap: { type: Number, "default": -1},
            clickUrl: { type: String },
          department: { type: String },
              factor: { type: Number },
              target: { type: Schema.Types.Mixed }
}, { minimize: false });

var CountriesSchema = new Schema({
                _id: { type: ObjectIdSchema },
               name: { type: String },
          "alpha-3": { type: String },
});

var CategoriesSchema = new Schema({
                _id: { type: String },
        description: { type: String }
});

var CreativesSchema = new Schema({
                 _id: { type: ObjectIdSchema },
              status: { type: String },
                name: { type: String },
        creativeType: { type: String },
                   w: { type: Number },
                   h: { type: Number },
              secure: { type: String },
         landingPage: { type: String },
              iframe: { type: String },
            jsScript: { type: String },
                vast: { type: String },
                iUrl: { type: String },
      pixelGenerator: { type: String },
           skippable: { type: String },
                 adm: { type: String },
		xAdm: { type: String },
             aDomain: { type: Array  },
                attr: { type: Array  },
                type: { type: Array  },
          categories: { type: Array  },
   videoExtraDetails: { type: Array  },
              assets: { type: Schema.Types.Mixed },
 requiredAssetsCount: { type: Number },
              layout: { type: Array  },
              adUnit: { type: Array  },
             context: { type: Array  },
      contextSubType: { type: Array  },
            native10: { type: String },
            native11: { type: String },
        frequencyCap: { type: Number, "default": -1}
});

var DealsSchema = new Schema({
                 _id: { type: ObjectIdSchema },
              status: { type: String },
                name: { type: String },
            dealType: { type: String },
            endPoint: { type: String },
            forensiq: { type: String },
              payout: { type: Number },
           campaigns: [{ type: ObjectIdSchema }],
});

var AdvertisersSchema = new Schema({
              _id:   { type: ObjectIdSchema, auto: true},
             name:   { type: String },
           status:   { type: String },
            deals:   [ DealsSchema ],
        campaigns:   [ CampaignsSchema ],
        creatives:   [ CreativesSchema ]
}, { versionKey: false }, { minimize: false });

var supplySourceSchema = new Schema({
              _id:   { type: ObjectIdSchema },
           status:   { type: String },
       supplyType:   { type: String },
      environment:   { type: String },
           minBid:   { type: Number },
           margin:   { type: Number },
         fallback:   { type: String },
         platform:   { type: String },
     calcBidFloor:   { type: Number },
         endpoint:   { type: String },
         campaign:   { type: Array },
          tagType:   { type: String },
       supplyName:   { type: String },
     frequencyCap:   { type: Number, "default": -1},
           target:   { type: Schema.Types.Mixed }
}, { minimize: false });

var PublisherSchema = new Schema({
              _id:   { type: ObjectIdSchema },
           status:   { type: String },
            pubId:   { type: String },
          pubName:   { type: String },
              cap:   { type: Number },
     frequencyCap:   { type: Number, "default": config.frequencyCap},
           seatId:   { type: String },
         endpoint:   { type: String },
  trafficSourceId:   { type: Number },
            spent:   { type: Number, "default": 0 },
           macros:   { type: Schema.Types.Mixed },
             bids:   [ supplySourceSchema ]
}, { versionKey: false }, { minimize: false });

var RotationSchema = new Schema({
              _id:   { type: ObjectIdSchema },
           status:   { type: String },
             name:   { type: String },
        publisher:   { type: String },
         endpoint:   { type: String },
        campaigns:   { type: Object },
              sum:   { type: Number}
}, { versionKey: false }, { minimize: false });

module.exports.users       = mongoose.model("Users", UserSchema, "acl_users");
module.exports.countries   = mongoose.model("Countries", CountriesSchema, "countries");
module.exports.categories  = mongoose.model("Categories", CategoriesSchema, "ContentCategories");
module.exports.advertisers = mongoose.model("Adv", AdvertisersSchema, "advertisers");
module.exports.publishers  = mongoose.model("Publishers", PublisherSchema, "SSPList");
module.exports.rotations   = mongoose.model("Rotations", RotationSchema, "rotations");
