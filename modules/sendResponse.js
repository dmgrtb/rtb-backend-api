exports.sendHttpStatus = function(res, status) {
	res.set({
		'Access-Control-Allow-Origin': 'http://' + config.domain,
		'Access-Control-Allow-Credentials': true
	});
    res.status(status).end();
}


exports.sendJson = function(res, status, data) {
	res.set({
		'Access-Control-Allow-Origin': 'http://' + config.domain,
		'Access-Control-Allow-Credentials': true
	});
    res.status(status).json(data);
}


exports.sendPlainText = function(res, status, data) {
	res.set({
		'Access-Control-Allow-Origin': 'http://' + config.domain,
		'Access-Control-Allow-Credentials': true,
		'Content-Type': 'text/html; charset=utf-8'
	});
    res.status(status).send(data);
}


